<?php $title = "<i class='fa fa-cogs'></i>&nbsp;Setup Content"; ?>
<div id="idImgLoader" style="margin: 0 auto; text-align: center;">
	<img src='<?php echo base_url();?>assets/img/loader-dark.gif' />
</div>
<div id="data" style="display:none;">
<section class="content">
<div class="page-header">
	<h1>
		<?php echo $title;?>
	</h1>
</div>
<div class="tabbable">
	<ul class="nav nav-tabs" id="formAksi">
		<li class="active">
			<a data-toggle="tab" href="#home">
			<i class="green ace-icon fa fa-file-image-o bigger-120"></i>
				Header
			</a>
		</li>

	    <li>
			<a data-toggle="tab" href="#logo">
			<i class="green ace-icon fa fa-file-image-o bigger-120"></i>
				Logo
			</a>
		</li>

											
	</ul>

	<div class="tab-content">



	
	<div id="home" class="tab-pane fade in active">
	<form id="formAksi" class="form-horizontal" role="form" action="#" method="POST" >
		<div class="form-body">
			<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> FaceBook </label>
				<div class="col-sm-10">
					<input type="text" id="facebook" name="facebook" class="col-xs-10 col-sm-5" />
				</div>
			</div>
			<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Twitter </label>
				<div class="col-sm-10">
					<input type="text" id="twitter" name="twitter" class="col-xs-10 col-sm-5" />
				</div>
			</div>
			<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Istagram </label>
				<div class="col-sm-10">
					<input type="text" id="instagram" name="instagram" class="col-xs-10 col-sm-5" />
				</div>
			</div>
			<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Telp </label>
				<div class="col-sm-10">
					<input type="text" id="telp" name="telp" class="col-xs-10 col-sm-5" />
				</div>
			</div>
			<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Email </label>
				<div class="col-sm-10">
					<input type="text" id="email" name="email" class="col-xs-10 col-sm-5" />
				</div>
			</div>
			<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Sekilas </label>
				<div class="col-sm-10">
					<input type="text" id="sekilas" name="sekilas" class="col-xs-10 col-sm-5" />
				</div>
			</div>
			<div class="form-group">
			
			<div class="col-md-offset-2 col-md-9">
				<button class="btn btn-info" type="submit" id="btn_save" onclick="save()">
					<i class="ace-icon fa fa-check bigger-110"></i>
					Update
				</button>
			</div>	
			</div>
		</div>
	</form>	
	</div>
	
    <div id="logo" class="tab-pane fade">
	
		
		<div class="form-body">
			<form id="form-upload" class="form-horizontal" role="form" action="<?= site_url('Setup/upload')?>" method="POST" enctype="multipart/form-data">
			<div class="form-group" >
			<label class="control-label col-md-3">Pilih File</label>
			<div class="input-group col-md-6">
				<input type="file" name="file-upload" id="file-upload">
				<span class="help-block"></span>
				<div class="input-group-btn">
					<button type="submit" class="btn btn-primary">Upload</button>
				</div>
			</div>
			</div>
			<div class="form-group" >
				<label class="control-label col-md-3">Logo Header</label>
					<div class="input-group col-md-9">
						<img id="preview-upload" src="#" style="height: 100px;border: 1px solid #DDC; " />
					</div>
			</div>
			</form>	
				
		</div>	
		
		
	
	</div>

	</div>
</div>

</div>
</section>

<script type="text/javascript">
	var zonk='';
	var link = "<?php echo site_url('Setup')?>";
	$(document).ready(function(){
      ubah();
    });

	function data(){
		$('#data').fadeIn();
	}
	
	
	
	$(document).ready(function(){
      //$('#idImgLoader').show(2000);
	  $('#idImgLoader').fadeOut(2000);
	  setTimeout(function(){
            data();
      }, 2000);
	   setTimeout(function(){
            ckeditor();
      }, 2000);
    });
	
	function ckeditor(){
		tinymce.init({
			selector: "textarea"
		});
	}
	
	$(document).ready(function(){
		$('#form-upload').submit(function(e) {
			tinyMCE.triggerSave();
			e.preventDefault(); var formData = new FormData($(this)[0]);
			$.ajax({
				url: $(this).attr("action"), type: 'POST', dataType: 'json', data: formData, async: true,
				beforeSend: function() { $('#btnSave').text('saving...'); $('#btnSave').attr('disabled',true); },
				success: function(response) {
					if(response.status) { swal_berhasil();
							ubah();
							
					} else { swal_berhasil();
							ubah(); }
				},
				complete: function() { $('#btnSave').text('save'); $('#btnSave').attr('disabled',false); },
				cache: false, contentType: false, processData: false
			});
		});

		function readURL(input) {
			if (input.files && input.files[0]) {
				var rd = new FileReader(); 
				rd.onload = function (e) { $('#preview-upload').attr('src', e.target.result); }; rd.readAsDataURL(input.files[0]);
			}
		}
		$("#file-upload").change(function(){ readURL(this); });
	});

    function ubah() {

		
		link_edit = "ajax_edit";
		
        $.ajax({
            url : link+"/"+link_edit+"/",
            type: "GET",
            dataType: "JSON",
            success: function(result) { 
			   var img = '<?= base_url(); ?>../assets/images/'+result.set_data7;	   
			   $("input[name='facebook']").val(result.set_data1); 
			   $("input[name='twitter']").val(result.set_data2);
			   $("input[name='instagram']").val(result.set_data3); 
			   $("input[name='telp']").val(result.set_data4);
			   $("input[name='email']").val(result.set_data5);
			   $("input[name='sekilas']").val(result.set_data6);
			   $('#preview-upload').attr('src', img);
            }, error: function (jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }
	
    $(document).on('submit', '#formAksi', function(e) { 
    tinyMCE.triggerSave(); 
      e.preventDefault();
			
				link_edit = "update_link";
		
        $.ajax({
            url : link+"/"+link_edit+"/",
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            success: function(data){                 
                swal_berhasil(); 
                ubah();
                //$('#a7').val();  
                setTimeout(function(){
                    $('#btn_save').text('Ubah');
                    $('#btn_save').attr('disabled', false);
                    //document.getElementById('formAksi').reset();
                }, 1000);

            }           
        });
        return false;
    });
</script>