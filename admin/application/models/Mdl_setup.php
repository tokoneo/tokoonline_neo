<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_setup extends CI_Model {
    
    var $table = 't_setup';
	
	public function get_by_id() {
		$this->db->select('a.fc_isi as set_data1, b.fc_isi as set_data2, c.fc_isi as set_data3, d.fc_isi as set_data4,e.fc_isi as set_data5, f.fc_isi as set_data6, g.fc_isi as set_data7');
		$this->db->from('t_setup a');
		$this->db->join('t_setup b ', 'b.fc_param="TWITTER"', 'left outer');
		$this->db->join('t_setup c ', 'c.fc_param="INSTAGRAM"', 'left outer');
		$this->db->join('t_setup d ', 'd.fc_param="TELP"', 'left outer');
		$this->db->join('t_setup e ', 'e.fc_param="EMAIL"', 'left outer');
		$this->db->join('t_setup f', 'f.fc_param="SEKILAS"','left outer');
		$this->db->join('t_setup g', 'g.fc_param="LOGO"','left outer');
		$this->db->where('a.fc_param','FACEBOOK');
		$query = $this->db->get();
		return $query->row();
	}
	
	public function update_link($data1,$data2,$data3,$data4, $data5, $data6) {
		$this->db->update($this->table, $data1, $data2, $data3, $data4, $data5, $data6);
		return $this->db->affected_rows();
	}
	
	public function update_data($data, $data2) {
		$this->db->update($this->table, $data, $data2);
		return $this->db->affected_rows();
	}
}