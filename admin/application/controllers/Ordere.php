<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ordere extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('Mdl_ordere');
        $this->auth->restrict();
        date_default_timezone_set("Asia/Jakarta");
        $this->load->library("session");
    }
    
    
    public function ajax_list() {
        $kode = $this->uri->segment(3);
        $list = $this->Mdl_ordere->get_datatables($kode);
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $produk) {
            $no++;
            $row = array();
            $row[] = '
            <input type="hidden" name="fc_kdbarang" value="'.$produk->fc_kdbarang.'">
            <input type="hidden" name="fc_kdgudang" value="'.$produk->fc_kdgudang.'">
             <input type="hidden" name="fc_kdorder" value="'.$produk->fc_kdorder.'">
            <input type="checkbox" name="cb_data[]" id="cb_data[]" value="'.$produk->fc_kdorder.'" checked="" >';
            $row[] = $no;
            $row[] = $produk->fv_nama_barang;
            $row[] = $produk->f_jumlah_produk;
            $row[] = $produk->fm_harga;
            $row[] = $produk->fm_subtotal;
            $row[] = 'Belum Melakukan Pembayaran';
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_REQUEST['draw'],
                        "recordsTotal" => $this->Mdl_ordere->count_all($kode),
                        "recordsFiltered" => $this->Mdl_ordere->count_filtered($kode),
                        "data" => $data,
                );
        echo json_encode($output);
    }

    public function generate_act(){
         for($i=0; $i<sizeof($this->input->post('cb_data', TRUE)); $i++){
            $kode_gudang = $this->input->post('fc_kdgudang');
            $kode_barang = $this->input->post('fc_kdbarang');
            $id_ordere = $this->input->post('fc_kdorder');
            $query = $this->db->query('SELECT * FROM td_order WHERE fc_kdorder = "'.$_POST['cb_data'][$i].'"  ');
           // print_r($kode_gudang);
           //  echo $kode_gudang;
            foreach ($query->result() as $val) {
                    $query2 = $this->db->query(
                        '
                        SELECT *
                        FROM td_stok_barang_gudang
                        WHERE fc_kdgudang ="'.$kode_gudang.'" and fc_kdbarang ="'.$kode_barang.'"  
                        '
                    );

                    foreach ($query2->result() as $value) {
                        $quantity_update = $value->fc_qty_barang + $val->f_jumlah_produk;
                        $data_qty = array(
                         'fc_qty_barang' => $quantity_update,
                        );
                        $update_qty = $this->Mdl_ordere->update_table('td_stok_barang_gudang',$data_qty, array('fc_kdbarang' => $value->fc_kdbarang , 'fc_kdgudang' => $value->fc_kdgudang));
                       //print_r($this->db->last_query());
                    }

            }

            $this->load->library('email');

               $config['charset'] = 'utf-8';
               $config['useragent'] = 'Produk pada pesanan Anda #'.$id_ordere.' telah dibatalkan';
               $config['protocol'] = 'smtp';
               $config['mailtype'] = 'html';
               $config['smtp_host'] = 'ssl://smtp.gmail.com';
               $config['smtp_port'] = '465';
               $config['smtp_timeout'] = '5';
               $config['smtp_user'] = 'edwinlaksono12@gmail.com'; //isi dengan email gmail
               $config['smtp_pass'] = 'Aero1996'; //isi dengan password
               $config['crlf'] = "\r\n";
               $config['newline'] = "\r\n";
               $config['wordwrap'] = TRUE;
               $config['charset'] = 'iso-8859-1';


               $this->email->initialize($config);

               $order_email = $this->Mdl_ordere->get_table_where('tm_order', array('fc_kdorder' => $id_ordere));

               $email = $order_email[0]['fv_email_order'];
               $this->email->from('edwinlaksono12@gmail.com', "Produk pada pesanan Anda #".$id_ordere." telah dibatalkan");
               $this->email->to($email);
               $this->email->subject('Produk pada pesanan Anda #'.$id_ordere.' telah dibatalkan');

               $data['id_order'] = $id_ordere;

               $data['order']=$this->Mdl_ordere->get_table_where('tm_order', array('fc_kdorder' => $id_ordere));
               $data['detail_order']=$this->Mdl_ordere->get_table_join_where('td_order','td_barang','td_order.fc_kdbarang=td_barang.fc_kdbarang', array('fc_kdorder' => $id_ordere));

               //print_r($this->db->last_query());
               $body = $this->load->view('moduls/email',$data,TRUE);
               $this->email->message($body);
               $this->email->send();

            $delete_keranjang = $this->db->query('delete from td_order where fc_kdorder="'.$_POST['cb_data'][$i].'"');  
            $delete_keranjang2 = $this->db->query('delete from tm_order where fc_kdorder="'.$_POST['cb_data'][$i].'"');    
        }
    }      
}    