<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Mdl_administrator');
		$this->load->model('Mdl_produk');
		$this->load->model('Mdl_gallery');
		$this->auth->restrict();
		date_default_timezone_set("Asia/Jakarta");
		$this->load->library("session");
	}

// Load View
    //public function index() { $this->load->view('admin_view'); }

// Pilih Modul Menu
	public function modul($modul) {
        if(!$modul){$this->session->set_userdata('err_msg', 'Anda Harus pilih salah satu Menu.'); redirect('admin');}
		$data['level']        = $this->Mdl_user->get_level();
		$data['produk']       = $this->Mdl_produk->get_produk();
		$data['album']        = $this->Mdl_gallery->get_album();
		$this->load->view('moduls/'.$modul, $data);	
	}
	
	
	function user_setup(){
		$data['view_file']    = "moduls/setup_user";
		$this->load->view('admin_view',$data);
	}

	function update_set(){
		$nim = $this->session->userdata('id_user');
		$nama = $this->session->userdata('nama');
		$password = $this->session->userdata('password');
		$username = $this->input->post('a1');
		$pwlama = $this->input->post('a2');
		$pwbaru= $this->input->post('a3');
		$pwulang = $this->input->post('a4');
		$md5lama=md5($pwlama);
		$passbaru= md5($pwbaru);
		$passe= $pwbaru;
		$enc=md5($passbaru);
		if ($pwbaru==$pwulang){
			if($password==$md5lama){
				if($nim!=''&&$password!=''&&$md5lama!=''&&$pwbaru!=''){
					$data = array(
								'id_user'   => $username,
				        'password'   => $passbaru,
				        'view_password' => $pwbaru
				      );
				    $this->Mdl_administrator->update(array('id_user' => $this->session->userdata('id_user')), $data);
				    echo "<script language='javascript' >alert('Sukses!!'); document.location=''</script>";
				}else{
					echo "<script language='javascript' >alert('Error Coba Cek !!'); document.location=''</script>";
				}
			}else{
				echo "<script language='javascript' >alert('Password Lama Tidak Sesuai!!'); document.location=''</script>";
			}
		}else{
			echo "<script language='javascript' >alert('Kedua Password Baru Tidak Sama!!'); document.location=''</script>";
		}
		if($data==true){
		$this->session->set_flashdata("pesan","<h3 class='content-box-header bg-primary' id='pesan'> <i class='glyph-icon icon-envelope'></i>&nbsp;&nbsp;&nbsp;Password Baru Anda Adalah : $pwbaru<div class='header-buttons-separator'><a href='#' onclick='Batal()' class='icon-separator'><i class='glyph-icon icon-times'></i></a></div></h3>");
		}else{
		$this->session->set_flashdata("pesan","<h3 class='content-box-header bg-danger' id='pesan'> <i class='glyph-icon icon-envelope'></i>&nbsp;&nbsp;&nbsp;Gagal Ganti Password<div class='header-buttons-separator'><a href='#' onclick='Batal()' class='icon-separator'><i class='glyph-icon icon-times'></i></a></div></h3>");
		}
	}


// Load Data Tabel List
	public function data($modul,$deleted) {
        if(!$modul){$this->session->set_userdata('err_msg', 'Anda Harus pilih salah satu Modul.'); redirect('admin');}
		if ($modul=='view_home'){
			$this->load->view('moduls/view_home');
		}else if($modul=='user'){
			$this->load->view('moduls/user');
		}else if($modul=='slider'){
			$this->load->view('moduls/slider');
		}else if($modul=='kontak'){
			$this->load->view('moduls/kontak');
		}else if($modul=='produk'){
			$this->load->view('moduls/produk');
		}else if($modul=='gallery'){
			$this->load->view('moduls/gallery');
		}else if($modul=='produk_det')	{
			$this->load->view('moduls/produk_det');
		}else if($modul=='foto'){
			$this->load->view('moduls/foto');
		}		
	}

	function cek_stok(){
		echo json_encode($this->Mdl_administrator->get_cek_stok());
	}

	function show_cart(){
		$cart = $this->Mdl_administrator->get_cek_stok2();
		
		$output = '';

			foreach ($cart as $items) {
				$link = base_url('Order/detail_order/'.$items->fc_kdorder);
				$output .="
					<li>
											<a href='#'>
												<div class='clearfix'>
													<span class='pull-left'>
														<i class='btn btn-xs no-hover btn-success fa fa-shopping-cart'></i>
																<a href=".$link.">".$items->fc_kdorder."</a>
													</span>
												</div>
											</a>
										</li>
				";
			}	
			return $output;
	}

	function load_cart(){ //load data cart
		echo $this->show_cart();
	}

}
