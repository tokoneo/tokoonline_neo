-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 07, 2018 at 03:55 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_shopneo_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_bank`
--

CREATE TABLE IF NOT EXISTS `data_bank` (
  `id_data` int(11) NOT NULL,
  `jenis_bank` varchar(255) NOT NULL,
  `atas_nama_bank` varchar(255) NOT NULL,
  `no_rekening` varchar(255) NOT NULL,
  `id_user` varchar(100) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_bank`
--

INSERT INTO `data_bank` (`id_data`, `jenis_bank`, `atas_nama_bank`, `no_rekening`, `id_user`) VALUES
(1, 'Mandiri', 'Nama', '32523523', ''),
(2, 'BRI', 'Nama', '52352', '');

-- --------------------------------------------------------

--
-- Table structure for table `konfirmasi_bayar`
--

CREATE TABLE IF NOT EXISTS `konfirmasi_bayar` (
  `id_konfirmasi` int(11) NOT NULL,
  `tgl_konfirmasi` date NOT NULL,
  `id_order` varchar(255) NOT NULL,
  `jumlah_bayar` int(11) NOT NULL,
  `bank_bayar` varchar(20) NOT NULL,
  `rekening_bayar` varchar(30) NOT NULL,
  `nama_bayar` varchar(30) NOT NULL,
  `foto` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konfirmasi_bayar`
--

INSERT INTO `konfirmasi_bayar` (`id_konfirmasi`, `tgl_konfirmasi`, `id_order`, `jumlah_bayar`, `bank_bayar`, `rekening_bayar`, `nama_bayar`, `foto`) VALUES
(1, '2018-10-25', 'T181025001', 10000000, 'bca', '111', 'edwin', '111.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `mainmenu`
--

CREATE TABLE IF NOT EXISTS `mainmenu` (
  `seq` int(11) NOT NULL,
  `idmenu` int(11) NOT NULL,
  `nama_menu` varchar(50) NOT NULL,
  `active_menu` varchar(50) NOT NULL,
  `icon_class` varchar(50) NOT NULL,
  `link_menu` varchar(50) NOT NULL,
  `menu_akses` varchar(12) NOT NULL,
  `entry_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `entry_user` varchar(50) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mainmenu`
--

INSERT INTO `mainmenu` (`seq`, `idmenu`, `nama_menu`, `active_menu`, `icon_class`, `link_menu`, `menu_akses`, `entry_date`, `entry_user`) VALUES
(1, 1, 'Dashboard', '', 'menu-icon fa fa-tachometer', 'Dashboard', '', '2018-11-02 03:54:49', NULL),
(8, 8, 'Administrator', '', 'menu-icon fa fa-user', '#', '', '2017-10-13 17:57:17', NULL),
(2, 2, 'Slider', '', 'menu-icon fa fa-file-image-o', 'Slider', '', '2017-10-17 17:28:56', NULL),
(3, 3, 'Pencairan Poin', '', 'menu-icon fa fa-money', 'Pencairanpoin', '', '2018-10-24 01:49:50', NULL),
(4, 4, 'Gudang', '', 'menu-icon fa fa-building', 'Gudang', '', '2018-10-25 02:51:59', NULL),
(19, 5, 'Setting Penjualan', '', 'menu-icon fa fa-building', 'Setting', '', '2018-11-02 02:03:51', NULL),
(20, 6, 'Order', '', 'menu-icon fa fa-money', 'Order', '', '2018-11-02 03:47:12', NULL),
(21, 7, 'Barang Masuk', '', 'menu-icon fa fa-money', 'Bpb', '', '2018-11-02 16:15:47', NULL),
(22, 9, 'Laporan', '', 'menu-icon fa fa-money', '#', '', '2018-11-06 03:27:15', NULL),
(23, 10, 'Voucher', '', 'menu-icon fa fa-money', 'Voucher', '', '2018-11-06 08:20:23', NULL),
(24, 11, 'Master', '', 'menu-icon fa fa-money', '#', '', '2018-11-06 16:23:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ongkir_pembeli`
--

CREATE TABLE IF NOT EXISTS `ongkir_pembeli` (
  `id_ongkir` int(11) NOT NULL,
  `ongkir` double NOT NULL,
  `id_order` varchar(50) NOT NULL,
  `id_penjual` varchar(50) NOT NULL,
  `tagihan_admin` int(11) NOT NULL DEFAULT '0',
  `pembayaran` int(11) NOT NULL DEFAULT '0',
  `jasa_pengiriman` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ongkir_pembeli`
--

INSERT INTO `ongkir_pembeli` (`id_ongkir`, `ongkir`, `id_order`, `id_penjual`, `tagihan_admin`, `pembayaran`, `jasa_pengiriman`) VALUES
(1, 14000, 'T181101001', 'TOKONEO', 0, 0, 'JNE - OKE'),
(2, 10000, 'T181101001', 'TOKONEO', 0, 0, 'JNE - OKE'),
(3, 14000, 'T181102001', 'TOKONEO', 0, 0, 'JNE - OKE'),
(4, 20000, 'T181102001', 'TOKONEO', 0, 0, 'JNE - OKE'),
(5, 0, 'T181107001', 'TOKONEO', 0, 0, '0');

-- --------------------------------------------------------

--
-- Table structure for table `pencairan_poin`
--

CREATE TABLE IF NOT EXISTS `pencairan_poin` (
  `kode_cairpoin` int(11) NOT NULL,
  `id_user_dapat` varchar(20) NOT NULL,
  `fc_id_order_detail` varchar(30) NOT NULL,
  `fc_kdpoin` int(11) NOT NULL,
  `selisih_harga` int(11) NOT NULL,
  `keuntungan_harga` int(11) NOT NULL,
  `persentasi` int(11) NOT NULL,
  `total_poin` int(11) NOT NULL,
  `status_ambil` int(11) NOT NULL,
  `tgl_ambil` datetime NOT NULL,
  `id_user_pencair` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pencairan_poin`
--

INSERT INTO `pencairan_poin` (`kode_cairpoin`, `id_user_dapat`, `fc_id_order_detail`, `fc_kdpoin`, `selisih_harga`, `keuntungan_harga`, `persentasi`, `total_poin`, `status_ambil`, `tgl_ambil`, `id_user_pencair`) VALUES
(2, 'admin', '1', 4, 500000, 498000, 1, 8, 0, '2018-11-01 00:00:00', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `submenu`
--

CREATE TABLE IF NOT EXISTS `submenu` (
  `id_sub` int(11) NOT NULL,
  `nama_sub` varchar(50) NOT NULL,
  `mainmenu_idmenu` int(11) NOT NULL,
  `active_sub` varchar(20) NOT NULL,
  `icon_class` varchar(100) NOT NULL,
  `link_sub` varchar(50) NOT NULL,
  `sub_akses` varchar(12) NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `entry_user` varchar(20) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `submenu`
--

INSERT INTO `submenu` (`id_sub`, `nama_sub`, `mainmenu_idmenu`, `active_sub`, `icon_class`, `link_sub`, `sub_akses`, `entry_date`, `entry_user`) VALUES
(1, 'Barang Masuk', 9, '', '', 'Barang_masuk', '', '2018-11-06 17:16:45', NULL),
(6, 'Order', 9, '', '', 'Laporan_order', '', '2018-11-06 17:16:38', NULL),
(7, 'Stock', 9, '', '', 'Laporan_stock', '', '2018-11-06 17:16:27', NULL),
(8, 'Poin', 9, '', '', 'Point', '', '2018-11-06 17:16:18', NULL),
(9, 'Bank', 11, '', '', 'Bank', '', '2018-11-06 17:21:49', NULL),
(10, 'About', 11, '', '', 'About', '', '2018-11-06 17:21:53', NULL),
(11, 'Kontak', 11, '', '', 'Kontak', '', '2018-11-06 17:21:56', NULL),
(12, 'Barang', 11, '', '', 'Barang', '', '2018-11-06 17:53:43', NULL),
(13, 'Kategori Produk', 11, '', '', 'Kategori_produk', '', '2018-11-07 01:28:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tab_akses_mainmenu`
--

CREATE TABLE IF NOT EXISTS `tab_akses_mainmenu` (
  `id` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `id_level` int(11) NOT NULL,
  `c` int(11) DEFAULT '0',
  `r` int(11) DEFAULT '0',
  `u` int(11) DEFAULT '0',
  `d` int(11) DEFAULT '0',
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `entry_user` varchar(50) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tab_akses_mainmenu`
--

INSERT INTO `tab_akses_mainmenu` (`id`, `id_menu`, `id_level`, `c`, `r`, `u`, `d`, `entry_date`, `entry_user`) VALUES
(1, 1, 1, NULL, 1, NULL, NULL, '2017-09-25 16:49:01', 'direktur'),
(2, 8, 1, 0, 0, 0, 0, '2018-11-06 03:27:38', ''),
(3, 2, 1, 0, 1, 0, 0, '2017-10-13 19:29:46', ''),
(4, 3, 1, 0, 1, 0, 0, '2017-10-13 19:29:46', ''),
(5, 4, 1, 0, 1, 0, 0, '2017-10-13 19:29:46', ''),
(23, 5, 1, 0, 1, 0, 0, '2018-11-02 02:04:02', ''),
(24, 6, 1, 0, 1, 0, 0, '2018-11-02 03:53:48', ''),
(25, 7, 1, 0, 1, 0, 0, '2018-11-02 16:15:56', ''),
(26, 9, 1, 0, 1, 0, 0, '2018-11-06 03:28:28', ''),
(27, 10, 1, 0, 1, 0, 0, '2018-11-06 08:03:04', ''),
(28, 11, 1, 0, 1, 0, 0, '2018-11-06 16:24:19', ''),
(29, 12, 1, 0, 1, 0, 0, '2018-11-06 17:55:08', '');

-- --------------------------------------------------------

--
-- Table structure for table `tab_akses_submenu`
--

CREATE TABLE IF NOT EXISTS `tab_akses_submenu` (
  `id` int(11) NOT NULL,
  `id_sub_menu` int(11) NOT NULL,
  `id_level` int(11) NOT NULL,
  `c` int(11) DEFAULT '0',
  `r` int(11) DEFAULT '0',
  `u` int(11) DEFAULT '0',
  `d` int(11) DEFAULT '0',
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `entry_user` varchar(30) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tab_akses_submenu`
--

INSERT INTO `tab_akses_submenu` (`id`, `id_sub_menu`, `id_level`, `c`, `r`, `u`, `d`, `entry_date`, `entry_user`) VALUES
(1, 1, 1, 0, 1, 0, 0, '2017-10-13 17:45:40', ''),
(6, 6, 1, 0, 1, 0, 0, '2018-11-06 03:34:29', ''),
(7, 7, 1, 0, 1, 0, 0, '2018-11-06 03:34:29', ''),
(8, 8, 1, 0, 1, 0, 0, '2018-11-06 03:34:39', ''),
(9, 9, 1, 0, 1, 0, 0, '2018-11-06 17:08:23', ''),
(10, 10, 1, 0, 1, 0, 0, '2018-11-06 17:22:35', ''),
(11, 11, 1, 0, 1, 0, 0, '2018-11-06 17:22:35', ''),
(12, 12, 1, 0, 1, 0, 0, '2018-11-07 01:23:27', ''),
(13, 13, 1, 0, 1, 0, 0, '2018-11-07 01:29:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_about`
--

CREATE TABLE IF NOT EXISTS `tb_about` (
  `id_about` int(11) NOT NULL,
  `about_logo` varchar(30) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `about_deskripsi` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `id_admin` int(11) DEFAULT NULL,
  `about_title_meta` varchar(200) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `about_deskripsi_meta` varchar(200) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `about_keyword_meta` varchar(200) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_about`
--

INSERT INTO `tb_about` (`id_about`, `about_logo`, `about_deskripsi`, `id_admin`, `about_title_meta`, `about_deskripsi_meta`, `about_keyword_meta`) VALUES
(1, 'neo_wood_art.png', 'Lorem ipsum dolor sit amet, consectetur adipisicing elPellentesque vehicula augue eget nisl ullamcorper, molestie blandit ipsum auctor. Mauris volutpat augue dolor..\r\n\r\nConsectetur adipisicing elit, sed do eiusmod tempor incididunt ut lab ore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco. labore et dolore magna aliqua.', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_kontak`
--

CREATE TABLE IF NOT EXISTS `tb_kontak` (
  `id_kontak` int(11) NOT NULL,
  `kontak_lat` varchar(100) DEFAULT NULL,
  `kontak_long` varchar(100) DEFAULT NULL,
  `kontak_deskripsi` text,
  `kontak_judul` varchar(30) DEFAULT NULL,
  `kontak_title_meta` varchar(200) DEFAULT NULL,
  `kontak_deskripsi_meta` text,
  `kontak_keyword_meta` varchar(200) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kontak`
--

INSERT INTO `tb_kontak` (`id_kontak`, `kontak_lat`, `kontak_long`, `kontak_deskripsi`, `kontak_judul`, `kontak_title_meta`, `kontak_deskripsi_meta`, `kontak_keyword_meta`) VALUES
(1, '-7.952659', '112.604725', 'Nulla ac convallis lorem, eget euismod nisl. Donec in libero sit amet mi vulputate consectetur. Donec auctor interdum purus, ac finibus massa bibendum nec.', 'Kontak Kami', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_slider`
--

CREATE TABLE IF NOT EXISTS `tb_slider` (
  `id_slider` int(11) NOT NULL,
  `fv_slider_judul` varchar(200) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `fv_slider_deskripsi` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `fc_slider_gambar` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `id_user` varchar(200) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_slider`
--

INSERT INTO `tb_slider` (`id_slider`, `fv_slider_judul`, `fv_slider_deskripsi`, `fc_slider_gambar`, `id_user`) VALUES
(1, 'Furniture', 'Kursi Rotan', '1.jpg', NULL),
(2, 'Furniture', 'Meja Artistik', '2.jpg', NULL),
(3, 'Furniture', 'Meja Kayu Jati', '3.jpg', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `td_barang`
--

CREATE TABLE IF NOT EXISTS `td_barang` (
  `fc_id` int(11) NOT NULL,
  `fc_kdbarang` char(20) DEFAULT NULL,
  `fc_kdkategori` int(11) DEFAULT NULL,
  `fv_nama_barang` varchar(100) DEFAULT NULL,
  `fv_deskripsi` text,
  `fc_img_1` text,
  `fc_img_2` text,
  `fc_img_3` text,
  `fc_img_4` text,
  `fd_harga_barang_publish` double(15,0) DEFAULT NULL,
  `fd_harga_barang_min` double(15,0) DEFAULT NULL,
  `fv_jenis_poin` varchar(20) DEFAULT NULL,
  `fc_kdgudang` int(11) DEFAULT NULL,
  `fv_berat` char(10) DEFAULT NULL,
  `fv_dimensi` char(30) DEFAULT NULL,
  `fc_user` char(20) DEFAULT NULL,
  `fc_status_stok` char(30) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `td_barang`
--

INSERT INTO `td_barang` (`fc_id`, `fc_kdbarang`, `fc_kdkategori`, `fv_nama_barang`, `fv_deskripsi`, `fc_img_1`, `fc_img_2`, `fc_img_3`, `fc_img_4`, `fd_harga_barang_publish`, `fd_harga_barang_min`, `fv_jenis_poin`, `fc_kdgudang`, `fv_berat`, `fv_dimensi`, `fc_user`, `fc_status_stok`) VALUES
(1, 'BI00001', 5, 'Meja Kayu Jati', 'Meja Kayu Jati Adalah', 'kotak_1.jpg', 'kotak_2.jpg', 'kotak_3.jpg', 'kotak_4.jpg', 1000000, 500000, 'satu poin', 1, '2000', '110x38x80cm', 'TOKONEO', 'in stok'),
(2, 'BI00002', 5, 'Meja Kayu Jati Merah', 'Meja Kayu Jati Adalah', 'kotak_2.jpg', 'kotak_2.jpg', 'kotak_3.jpg', 'kotak_4.jpg', 1000000, 500000, 'satu poin', 1, '2000', '110x38x80cm', 'TOKONEO', 'pre order');

-- --------------------------------------------------------

--
-- Table structure for table `td_chatlive`
--

CREATE TABLE IF NOT EXISTS `td_chatlive` (
  `fc_id` int(11) NOT NULL,
  `fc_idroom` char(5) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `fv_msg` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `fd_waktu` datetime DEFAULT NULL COMMENT 'Relasi Dengan Tabel tb_user field id_nik',
  `fc_pengirim` enum('P','A') CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT 'P' COMMENT 'P = Pengunjung, A = Admin',
  `fc_baca` enum('Y','N','') CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT 'N'
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `td_chatlive`
--

INSERT INTO `td_chatlive` (`fc_id`, `fc_idroom`, `fv_msg`, `fd_waktu`, `fc_pengirim`, `fc_baca`) VALUES
(2, '7', 'Apakah Boleh', '2017-07-07 04:26:37', 'P', 'N'),
(3, '7', 'Saya Ingin Coba', '2017-07-07 16:26:51', 'P', 'N'),
(4, '7', 'Mengapa setiap Kali saya merasa Galau ', '2017-07-07 16:27:18', 'P', 'N'),
(5, '7', 'Bukit Daun Hotel and Resort provides the wellness component in each Bukit Daun property, through facilities, treatments and products.', '2017-07-07 16:27:58', 'P', 'N'),
(6, '7', 'Halooo,, Apakah ada Orang ?', '2017-07-07 16:28:22', 'P', 'N'),
(7, '7', 'Iya selamat datang', '2017-07-07 00:00:00', 'A', 'N'),
(8, '7', 'Adakah yang bisa saya banntu', '2017-07-07 00:00:00', 'A', 'N'),
(9, '7', 'hayooo', '2017-07-07 16:47:47', 'A', 'N'),
(10, '7', 'Ada apa', '2017-07-07 16:47:59', 'P', 'N'),
(11, '7', 'apakaj', '2017-07-07 16:48:05', 'P', 'N'),
(12, '7', 'dsfsdg gd  f', '2017-07-07 16:48:14', 'A', 'N'),
(13, '7', 'sdsd fg d h gf', '2017-07-07 16:48:24', 'A', 'N'),
(14, '7', 'gkjkjk', '2017-07-07 17:28:57', 'P', 'N'),
(15, '8', 'Apakah saya bisa', '2017-07-07 05:35:59', 'P', 'N'),
(16, '8', 'Maaf Pertanyaan Anda Kurang Jelas', '2017-07-07 17:36:49', 'A', 'N'),
(17, '8', 'silahkan', '2017-07-07 17:37:07', 'P', 'N'),
(18, '8', 'qdries', '2017-07-07 17:37:50', 'P', 'N'),
(19, '8', 'coba', '2017-07-07 17:38:07', 'A', 'N'),
(20, '8', 'woeeeee', '2017-07-07 17:38:32', 'P', 'N'),
(21, '8', 'sdagjgdhadah', '2017-07-07 17:38:57', 'A', 'N'),
(22, '8', 'dancok', '2017-07-07 17:39:08', 'A', 'N'),
(23, '8', 'matanya', '2017-07-07 17:39:24', 'A', 'N'),
(24, '8', 'asu', '2017-07-07 17:39:32', 'A', 'N'),
(25, '8', 'hshgsgfscs', '2017-07-07 17:39:33', 'P', 'N'),
(26, '8', 'kamfret', '2017-07-07 17:39:45', 'A', 'N'),
(27, NULL, 'ha ha ha', '2017-07-07 19:31:42', 'P', 'N'),
(28, NULL, 'hdgdg', '2017-07-07 19:31:58', 'P', 'N'),
(29, '9', 'dsfsdfdsf', '2017-07-07 08:50:49', 'P', 'N'),
(30, '9', 'hjhjh', '2017-07-07 20:51:24', 'P', 'N'),
(31, '10', 'Apakah anda', '2017-07-07 09:11:38', 'P', 'N'),
(32, '10', 'Apakah anda', '2017-07-07 09:11:45', 'P', 'N'),
(33, '10', 'saya percaya', '2017-07-07 21:12:04', 'P', 'N'),
(34, '10', 'saya percaya', '2017-07-07 21:12:04', 'P', 'N'),
(35, '10', 'saya percaya', '2017-07-07 21:12:05', 'P', 'N'),
(36, '10', 'saya percaya', '2017-07-07 21:12:05', 'P', 'N'),
(37, '10', 'saya percaya', '2017-07-07 21:12:05', 'P', 'N'),
(38, '10', 'saya percaya', '2017-07-07 21:12:05', 'P', 'N'),
(39, '10', 'saya percaya', '2017-07-07 21:12:06', 'P', 'N'),
(40, '10', 'saya percaya', '2017-07-07 21:12:06', 'P', 'N'),
(41, '10', 'saya percaya', '2017-07-07 21:12:06', 'P', 'N'),
(42, '10', 'saya percaya', '2017-07-07 21:12:06', 'P', 'N'),
(43, '10', 'saya percaya', '2017-07-07 21:12:06', 'P', 'N'),
(44, '10', 'saya percaya', '2017-07-07 21:12:07', 'P', 'N'),
(45, '10', 'saya percaya', '2017-07-07 21:12:07', 'P', 'N'),
(46, '10', 'saya percaya', '2017-07-07 21:12:07', 'P', 'N'),
(47, '10', 'hsg', '2017-07-07 21:12:13', 'P', 'N'),
(48, '12', 'Apakah Saya Bisa Bicara Dengan Anda ?', '2017-07-08 01:55:55', 'P', 'N'),
(49, '12', 'Haloo', '2017-07-08 01:57:09', 'P', 'N'),
(50, '13', 'Apakah Saya Bisa ?', '2017-07-11 09:26:41', 'P', 'N'),
(51, '13', 'haloo ', '2017-07-11 09:26:47', 'P', 'N'),
(52, '13', 'Apakah Ada Orang', '2017-07-11 09:26:55', 'P', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `td_keranjang_belanja`
--

CREATE TABLE IF NOT EXISTS `td_keranjang_belanja` (
  `fc_id` int(11) NOT NULL,
  `fc_kdkeranjang_belanja` char(30) DEFAULT NULL,
  `fc_kdbarang` char(20) DEFAULT NULL,
  `fc_kdgudang` int(11) DEFAULT NULL,
  `fm_harga_produk` double(15,0) DEFAULT NULL,
  `fn_jumlah_produk` int(10) DEFAULT NULL,
  `fm_subtotal_belanja` double(15,0) DEFAULT NULL,
  `fc_status_stok` char(30) DEFAULT NULL,
  `fc_status` char(10) DEFAULT NULL,
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `f_kode_voucher` char(30) DEFAULT NULL,
  `fd_exp_date` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `td_keranjang_belanja`
--

INSERT INTO `td_keranjang_belanja` (`fc_id`, `fc_kdkeranjang_belanja`, `fc_kdbarang`, `fc_kdgudang`, `fm_harga_produk`, `fn_jumlah_produk`, `fm_subtotal_belanja`, `fc_status_stok`, `fc_status`, `time`, `f_kode_voucher`, `fd_exp_date`) VALUES
(1, '::1', 'BI00001', 1, 1000000, 1, 1000000, 'in stok', 'visitor', '2018-11-06 14:20:22', '', '2018-11-07 21:20:22');

-- --------------------------------------------------------

--
-- Table structure for table `td_konfirmasi_bayar`
--

CREATE TABLE IF NOT EXISTS `td_konfirmasi_bayar` (
  `fc_kdkonfirmasi` int(11) NOT NULL,
  `fd_tgl_konfirmasi` date DEFAULT NULL,
  `fc_kdorder` char(15) DEFAULT NULL,
  `fm_jumlah_bayar` double(10,0) DEFAULT NULL,
  `fc_bank_bayar` char(20) DEFAULT NULL,
  `fc_rekening_bayar` char(30) DEFAULT NULL,
  `fv_nama_bayar` varchar(30) DEFAULT NULL,
  `fc_img` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `td_order`
--

CREATE TABLE IF NOT EXISTS `td_order` (
  `fc_id_order_detail` int(11) NOT NULL,
  `fc_kdorder` char(30) DEFAULT NULL,
  `fc_penjual` char(30) DEFAULT NULL,
  `fc_kdbarang` char(15) DEFAULT NULL,
  `f_jumlah_produk` int(10) DEFAULT NULL,
  `f_berat_produk` int(10) DEFAULT NULL,
  `fm_harga` double(15,0) DEFAULT NULL,
  `fm_harga_pajak` double(15,0) DEFAULT NULL,
  `fm_subtotal` double(15,0) DEFAULT NULL,
  `fm_subtotal_pajak` double(15,0) DEFAULT NULL,
  `fm_pembayaran` double(15,0) DEFAULT NULL,
  `fm_tagihan` double(15,0) DEFAULT NULL,
  `f_kode_voucher` char(30) DEFAULT NULL,
  `fc_kdgudang` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `td_order`
--

INSERT INTO `td_order` (`fc_id_order_detail`, `fc_kdorder`, `fc_penjual`, `fc_kdbarang`, `f_jumlah_produk`, `f_berat_produk`, `fm_harga`, `fm_harga_pajak`, `fm_subtotal`, `fm_subtotal_pajak`, `fm_pembayaran`, `fm_tagihan`, `f_kode_voucher`, `fc_kdgudang`) VALUES
(2, 'T181102001', 'TOKONEO', 'BI00001', 2, 4000, 1000000, NULL, 2000000, NULL, NULL, NULL, '', 1),
(3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'T181107001', 'TOKONEO', 'BI00001', 2, 4000, 1000000, NULL, 2000000, NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `td_stok_barang_gudang`
--

CREATE TABLE IF NOT EXISTS `td_stok_barang_gudang` (
  `fc_kdstok_gudang` int(11) NOT NULL,
  `fc_kdgudang` int(11) DEFAULT NULL,
  `fc_kdbarang` char(15) DEFAULT NULL,
  `fc_qty_barang` char(10) DEFAULT NULL,
  `id_user` varchar(200) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `td_stok_barang_gudang`
--

INSERT INTO `td_stok_barang_gudang` (`fc_kdstok_gudang`, `fc_kdgudang`, `fc_kdbarang`, `fc_qty_barang`, `id_user`) VALUES
(1, 1, 'BI00001', '23', NULL),
(2, 2, 'BI00001', '2', NULL),
(3, 1, 'BI00002', '6', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tm_gudang`
--

CREATE TABLE IF NOT EXISTS `tm_gudang` (
  `fc_kdgudang` int(11) NOT NULL,
  `fv_nmgudang` varchar(30) DEFAULT NULL,
  `fv_alamat` text,
  `id_user` varchar(200) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tm_gudang`
--

INSERT INTO `tm_gudang` (`fc_kdgudang`, `fv_nmgudang`, `fv_alamat`, `id_user`) VALUES
(1, 'Gudang Galunggung', 'Jalan Galunggung', 'admin'),
(2, 'Matos', 'Malang Town Square', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `tm_kategori_barang`
--

CREATE TABLE IF NOT EXISTS `tm_kategori_barang` (
  `fc_id` int(11) NOT NULL,
  `fv_nama_kategori` varchar(100) DEFAULT NULL,
  `id_user` varchar(200) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tm_kategori_barang`
--

INSERT INTO `tm_kategori_barang` (`fc_id`, `fv_nama_kategori`, `id_user`) VALUES
(1, 'Civing Room', NULL),
(2, 'Bad Room', NULL),
(3, 'Dining Room', NULL),
(4, 'Dekorasi', NULL),
(5, 'Interior', NULL),
(6, 'Eterior + Pagar', NULL),
(7, 'Kusen Pintu', NULL),
(8, 'Gebyok', NULL),
(9, 'Gazebo', NULL),
(10, 'Joglo', NULL),
(11, 'Classic', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tm_order`
--

CREATE TABLE IF NOT EXISTS `tm_order` (
  `fc_kdorder` char(30) NOT NULL,
  `fd_tgl_order` datetime DEFAULT NULL,
  `fm_total` double(15,0) DEFAULT NULL,
  `fv_nama_order` varchar(30) DEFAULT NULL,
  `fv_email_order` varchar(30) DEFAULT NULL,
  `fv_alamat_order` text,
  `fc_telp` char(12) DEFAULT NULL,
  `fc_kode_pos_order` char(8) DEFAULT NULL,
  `fv_provinsi_order` varchar(30) DEFAULT NULL,
  `fv_kota_order` varchar(30) DEFAULT NULL,
  `fm_ongkir_order` double(10,0) DEFAULT NULL,
  `fm_grandtotal_order` double(15,0) DEFAULT NULL,
  `fc_status_kirim` char(1) DEFAULT '1',
  `fc_jenis_stok_order` char(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tm_order`
--

INSERT INTO `tm_order` (`fc_kdorder`, `fd_tgl_order`, `fm_total`, `fv_nama_order`, `fv_email_order`, `fv_alamat_order`, `fc_telp`, `fc_kode_pos_order`, `fv_provinsi_order`, `fv_kota_order`, `fm_ongkir_order`, `fm_grandtotal_order`, `fc_status_kirim`, `fc_jenis_stok_order`) VALUES
('T181102001', '2018-11-02 12:36:23', 2000000, 'edwin yordan', 'edwinlaksono12@gmail.com', 'kediri\r\nmalng', '85607369653', '64181', 'Jawa Timur', 'Malang', 34000, 2034000, '4', 'in stok'),
('T181107001', '2018-11-07 00:43:16', 2000000, 'edwin', 'edwinfrontman@gmail.com', 'aaa', '00000', '11', 'Jawa Timur', 'Batu', 24000, 2024000, '6', 'in stok');

-- --------------------------------------------------------

--
-- Table structure for table `tm_poin`
--

CREATE TABLE IF NOT EXISTS `tm_poin` (
  `fc_kdpoin` int(11) NOT NULL,
  `fc_jumlah_poin` char(15) DEFAULT NULL,
  `fc_min_persen` char(15) DEFAULT NULL,
  `fc_max_persen` char(15) DEFAULT NULL,
  `id_user` varchar(200) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tm_poin`
--

INSERT INTO `tm_poin` (`fc_kdpoin`, `fc_jumlah_poin`, `fc_min_persen`, `fc_max_persen`, `id_user`) VALUES
(1, '1', '1', '25', 'admn'),
(2, '2', '26', '50', 'admin'),
(3, '3', '51', '75', 'admin'),
(4, '4', '76', '99', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `tm_user`
--

CREATE TABLE IF NOT EXISTS `tm_user` (
  `id_user` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(30) NOT NULL,
  `level` varchar(30) NOT NULL,
  `status` varchar(1) NOT NULL,
  `foto` text,
  `password` varchar(100) NOT NULL,
  `provinsi` varchar(50) NOT NULL,
  `kota` varchar(50) NOT NULL,
  `id_ongkir` int(11) NOT NULL,
  `aktif_user` tinyint(1) NOT NULL,
  `nama_rek_user` varchar(255) NOT NULL,
  `no_rek_user` varchar(255) NOT NULL,
  `bank_rek_user` varchar(255) NOT NULL,
  `view_password` varchar(100) DEFAULT NULL,
  `admin_level` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tm_user`
--

INSERT INTO `tm_user` (`id_user`, `nama`, `email`, `level`, `status`, `foto`, `password`, `provinsi`, `kota`, `id_ongkir`, `aktif_user`, `nama_rek_user`, `no_rek_user`, `bank_rek_user`, `view_password`, `admin_level`) VALUES
('admin', 'admin', 'admin@admin.com', 'admin', '1', NULL, 'e00cf25ad42683b3df678c61f42c6bda', '', '', 0, 0, '', '', '', 'admin1', 1),
('afif', 'wildan afif', 'wildan@gmail.com', 'member', '', NULL, 'e353cc5a41c4b79ffa728ce8b80d1b62', 'Jawa Timur', 'Malang', 256, 1, 'wildan afif', '3244325423', 'Mandiri', NULL, NULL),
('AMurleloli', 'AMurleloli', 'quonahquee@bestmailonline.com', 'member', '', NULL, '68856c90365e35e3e8df64cc4e9c3345', '', '', 0, 0, 'AMurleloli', 'AMurleloli', 'La Strada  hardcore', NULL, NULL),
('Briandab', 'Briandab', 'xrumm88965@gmail.com', 'member', '', NULL, '63710fe5626abcda4dc059508f4a7fd1', '', '', 0, 0, 'Briandab', 'Briandab', 'Pearl Jam', NULL, NULL),
('danaukerinciraya', 'Admin', 'admin@admin.com', 'admin', '1', '', '827ccb0eea8a706c4c34a16891f84e7b', '0', '0', 0, 0, '', '', '', NULL, NULL),
('david', 'David', 'davis', 'member', '', NULL, '172522ec1028ab781d9dfd17eaca4427', 'Jawa Timur', 'Malang', 255, 1, 'David', '23402840', 'Mandiri', NULL, NULL),
('edoedo', 'edoedoeo', 'edo_guitarman.4171@yahoo.com', 'member', '', NULL, '827ccb0eea8a706c4c34a16891f84e7b', 'Jawa Barat', 'Bandung', 22, 1, '', '', '', NULL, NULL),
('eka', 'eka ramdani', 'admin@admin.com', 'member', '', NULL, '827ccb0eea8a706c4c34a16891f84e7b', 'DKI Jakarta', 'Jakarta Pusat', 152, 1, 'Eka Ramdani', '12324', 'BRI', NULL, NULL),
('Emma', 'Fathimatuz Zahra', 'emma_zahra@gmail.com', 'member', '', NULL, '972a9f0dc30d2d552063983763dab7d8', 'Jawa Timur', 'Malang', 256, 1, '', '', '', NULL, NULL),
('Ontownnageenano', 'Ontownnageenano', 'cxufmhorrespesia@maldonadomail', 'member', '', NULL, '6d1324945fb216a6c6348a04abc2058f', '', '', 0, 0, 'Ontownnageenano', 'Ontownnageenano', '', NULL, NULL),
('pisang', 'Pisang', 'pisang@gmail.com', 'member', '', NULL, '4dc2a159b17b4725943816b8ba6d7ff5', 'Jawa Timur', 'Malang', 255, 1, 'Pisang', '23432532523', 'Mandiri', NULL, NULL),
('rhetech', 'Rheza Arief', 'rhetech@yahoo.co.uk', 'member', '', NULL, '827ccb0eea8a706c4c34a16891f84e7b', 'Jawa Timur', 'Sidoarjo', 409, 1, 'rheza arief ', '712537512375133', 'BNI', NULL, NULL),
('ridhorobby', 'RIdho', 'ridho.robby50@yahoo.com', 'member', '1', NULL, '827ccb0eea8a706c4c34a16891f84e7b', 'Jawa Timur', 'Bondowoso', 86, 1, '', '', '', NULL, NULL),
('robby1', 'Robby', 'roby@robyyahoo.com', 'member', '', NULL, '827ccb0eea8a706c4c34a16891f84e7b', '0', '0', 0, 0, '', '', '', NULL, NULL),
('robby12', 'Robby Bibi', 'roby@robyyahoo.com', 'member', '', NULL, '827ccb0eea8a706c4c34a16891f84e7b', 'Jawa Timur', 'Malang', 256, 1, '', '', '', NULL, NULL),
('saya', 'coba', 'coba@gmail.com', 'member', '', NULL, '81dc9bdb52d04dc20036dbd8313ed055', 'Jawa Timur', 'Jember', 160, 1, 'COba', '11111111', 'BCA', NULL, NULL),
('test', 'test', 'ic_troumax@yahoo.com', 'admin', '1', '', '22476aa1575b6ef6032e4c2c2e3a7b25', '0', '0', 0, 0, '', '', '', NULL, NULL),
('TOKONEO', 'TOKONEO', 'yayanraw@gmail.com', 'member', '', NULL, 'd8578edf8458ce06fbc5bb76a58c5ca4', 'Jawa Timur', 'Malang', 255, 1, 'Yayan Rahmat Wijaya', '021223224244', 'BRI', NULL, NULL),
('Usernam', 'Admin', 'admin@admin.com', 'member', '', NULL, 'e10adc3949ba59abbe56e057f20f883e', 'DI Yogyakarta', 'Bantul', 39, 0, 'nama pem', 'no 12312', 'Mandiri', NULL, NULL),
('wildan', 'wildan afif', 'wildanafif00@gmail.com', 'member', '', NULL, '827ccb0eea8a706c4c34a16891f84e7b', 'Jawa Timur', 'Malang', 255, 1, 'Wildan Afif', '34234233', 'BRI', NULL, NULL),
('wildanafif', 'wildan afif a', 'wildanafif.id@gmail.com', 'member', '', NULL, '827ccb0eea8a706c4c34a16891f84e7b', 'Jawa Timur', 'Blitar', 74, 1, 'wildan', '43534534', 'Mandiri', NULL, NULL),
('wld', 'wildan afif', 'wildanafif.id@gmail.com', 'member', '', NULL, 'e353cc5a41c4b79ffa728ce8b80d1b62', 'Jawa Timur', 'Lumajang', 243, 0, 'erda', '324232', 'Mandiri', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tm_voucher`
--

CREATE TABLE IF NOT EXISTS `tm_voucher` (
  `fc_id_voucher` int(11) NOT NULL,
  `fc_kdbarang` char(15) DEFAULT NULL,
  `id_user` char(25) DEFAULT NULL,
  `fm_nominal` double(11,0) DEFAULT NULL,
  `fd_tgl_exp_voucher` datetime DEFAULT NULL,
  `f_kode_voucher` char(30) DEFAULT NULL,
  `fd_tgl_terbit_voucher` datetime DEFAULT NULL,
  `fc_status` enum('0','1','2') DEFAULT '0' COMMENT '0 : status belum di pakai,1: status kalau sudah cek voucher , 2 : status kalau sudah check out'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tm_voucher`
--

INSERT INTO `tm_voucher` (`fc_id_voucher`, `fc_kdbarang`, `id_user`, `fm_nominal`, `fd_tgl_exp_voucher`, `f_kode_voucher`, `fd_tgl_terbit_voucher`, `fc_status`) VALUES
(1, 'BI00001', 'admin', 2000, '2018-10-17 17:08:00', 'Promo1', '2014-01-09 11:23:00', '1'),
(2, 'BI00001', 'admin', 100000, '2018-10-17 17:08:00', 'Promo2', '2014-01-09 11:23:02', '1'),
(3, 'BI00001', 'admin', 100000, '2018-10-17 17:08:00', 'Promo3', '2014-01-09 11:23:03', '1'),
(4, 'BI00001', 'admin', 100000, '2018-10-17 17:08:00', 'Promo3', '2014-01-09 11:23:04', '1'),
(5, 'BI00001', 'admin', 100000, '2018-10-17 17:08:00', 'Promo3', '2014-01-09 11:23:05', '1');

-- --------------------------------------------------------

--
-- Table structure for table `t_barang_pindah`
--

CREATE TABLE IF NOT EXISTS `t_barang_pindah` (
  `fc_kdbarang_pindah` int(11) NOT NULL,
  `fd_tgl_barang_pindah` date DEFAULT NULL,
  `fc_kdgudang_asal` int(11) DEFAULT NULL,
  `fc_kdgudang_tujuan` int(11) DEFAULT NULL,
  `fc_kdbarang` char(20) DEFAULT NULL,
  `f_jumlah_barang` int(20) DEFAULT NULL,
  `id_user` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_bpbdtl`
--

CREATE TABLE IF NOT EXISTS `t_bpbdtl` (
  `fc_id` int(11) NOT NULL,
  `fc_nobpb` char(15) DEFAULT NULL,
  `fc_kdbarang` char(15) DEFAULT NULL,
  `fc_kdgudang` int(11) DEFAULT NULL,
  `fn_qtyterima` int(11) DEFAULT NULL,
  `fm_harsat` double(10,0) DEFAULT NULL,
  `fm_subtot` double(10,0) DEFAULT NULL,
  `id_user` varchar(200) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_bpbdtl`
--

INSERT INTO `t_bpbdtl` (`fc_id`, `fc_nobpb`, `fc_kdbarang`, `fc_kdgudang`, `fn_qtyterima`, `fm_harsat`, `fm_subtot`, `id_user`) VALUES
(16, 'BPB-00005', 'BI00001', 1, 4, 300000, 1200000, NULL),
(17, 'BPB-00006', 'BI00001', 1, 1, 20000, 20000, NULL),
(18, 'BPB-00007', 'BI00001', 1, 1, 13000, 13000, NULL),
(19, 'BPB-00008', 'BI00002', 1, 2, 300000, 600000, NULL),
(20, 'BPB-00009', 'BI00001', 1, 3, 1, 3, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_bpbdtl_temp`
--

CREATE TABLE IF NOT EXISTS `t_bpbdtl_temp` (
  `fc_id` int(11) NOT NULL,
  `fc_nobpb` char(15) DEFAULT NULL,
  `fc_kdbarang` char(15) DEFAULT NULL,
  `fc_kdgudang` int(11) DEFAULT NULL,
  `fn_qtyterima` int(11) DEFAULT NULL,
  `fm_harsat` double(10,0) DEFAULT NULL,
  `fm_subtot` double(10,0) DEFAULT NULL,
  `fd_tglbpb` date NOT NULL,
  `fv_nama_supplier` varchar(50) NOT NULL,
  `fd_tglinput` datetime NOT NULL,
  `fc_userinput` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_bpbmst`
--

CREATE TABLE IF NOT EXISTS `t_bpbmst` (
  `fc_id` int(11) NOT NULL,
  `fc_nobpb` char(15) DEFAULT NULL,
  `fd_tglbpb` date DEFAULT NULL,
  `fv_nama_supplier` varchar(30) DEFAULT NULL,
  `fd_tglinput` datetime DEFAULT NULL,
  `fc_userinput` char(15) DEFAULT NULL,
  `fn_qtytot` int(11) DEFAULT NULL,
  `fm_total` double(15,0) DEFAULT NULL,
  `id_user` varchar(200) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_bpbmst`
--

INSERT INTO `t_bpbmst` (`fc_id`, `fc_nobpb`, `fd_tglbpb`, `fv_nama_supplier`, `fd_tglinput`, `fc_userinput`, `fn_qtytot`, `fm_total`, `id_user`) VALUES
(16, 'BPB-00005', '2018-11-06', '', '2018-11-06 21:24:38', NULL, 4, 300000, 'admin'),
(17, 'BPB-00006', '2018-11-06', '', '2018-11-06 21:25:52', NULL, 1, 20000, 'admin'),
(18, 'BPB-00007', '2018-11-06', '', '2018-11-06 21:32:14', NULL, 1, 13000, 'admin'),
(19, 'BPB-00008', '2018-11-06', '', '2018-11-06 21:34:53', NULL, 2, 300000, 'admin'),
(20, 'BPB-00009', '2018-11-06', '', '2018-11-06 21:36:04', NULL, 3, 1, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `t_bpbmst_temp`
--

CREATE TABLE IF NOT EXISTS `t_bpbmst_temp` (
  `fc_id` int(11) NOT NULL,
  `fc_nobpb` char(15) DEFAULT NULL,
  `fd_tglbpb` date DEFAULT NULL,
  `fv_nama_supplier` varchar(30) DEFAULT NULL,
  `fd_tglinput` datetime DEFAULT NULL,
  `fc_userinput` char(15) DEFAULT NULL,
  `fn_qtytot` int(11) DEFAULT NULL,
  `fm_total` double(15,0) DEFAULT NULL,
  `id_user` varchar(200) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_bpbmst_temp`
--

INSERT INTO `t_bpbmst_temp` (`fc_id`, `fc_nobpb`, `fd_tglbpb`, `fv_nama_supplier`, `fd_tglinput`, `fc_userinput`, `fn_qtytot`, `fm_total`, `id_user`) VALUES
(1, 'admin', '2018-11-04', 'aa', '2018-11-04 10:08:09', NULL, NULL, 1000000, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_nomor`
--

CREATE TABLE IF NOT EXISTS `t_nomor` (
  `kode` char(10) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `awalan` char(15) COLLATE latin1_general_ci DEFAULT NULL,
  `akhiran` char(15) COLLATE latin1_general_ci DEFAULT NULL,
  `panjang` int(4) unsigned DEFAULT '0',
  `nomor` int(4) unsigned DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci ROW_FORMAT=FIXED;

--
-- Dumping data for table `t_nomor`
--

INSERT INTO `t_nomor` (`kode`, `awalan`, `akhiran`, `panjang`, `nomor`) VALUES
('BRG', 'BRG-', NULL, 5, 1),
('BPB', 'BPB-', NULL, 5, 10);

-- --------------------------------------------------------

--
-- Table structure for table `t_setup`
--

CREATE TABLE IF NOT EXISTS `t_setup` (
  `ID` int(11) NOT NULL,
  `fc_param` char(20) DEFAULT NULL,
  `fc_kode` char(1) DEFAULT NULL,
  `fc_isi` char(200) DEFAULT NULL,
  `id_user` varchar(200) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_setup`
--

INSERT INTO `t_setup` (`ID`, `fc_param`, `fc_kode`, `fc_isi`, `id_user`) VALUES
(1, 'WAKTU', '1', '24', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_status`
--

CREATE TABLE IF NOT EXISTS `t_status` (
  `fc_param` char(10) NOT NULL,
  `fc_kode` char(2) NOT NULL,
  `fv_value` char(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_temp_order`
--

CREATE TABLE IF NOT EXISTS `t_temp_order` (
  `fc_id` int(11) NOT NULL,
  `fc_kdbarang` char(15) DEFAULT NULL,
  `fn_quantity` int(10) DEFAULT NULL,
  `fc_kdgudang` int(11) DEFAULT NULL,
  `fc_kdkeranjang_belanja` char(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_temp_order`
--

INSERT INTO `t_temp_order` (`fc_id`, `fc_kdbarang`, `fn_quantity`, `fc_kdgudang`, `fc_kdkeranjang_belanja`) VALUES
(3, 'BI00001', 1, 1, '::1'),
(4, 'BI00002', 1, 1, '::1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_bank`
--
ALTER TABLE `data_bank`
  ADD PRIMARY KEY (`id_data`);

--
-- Indexes for table `konfirmasi_bayar`
--
ALTER TABLE `konfirmasi_bayar`
  ADD PRIMARY KEY (`id_konfirmasi`);

--
-- Indexes for table `mainmenu`
--
ALTER TABLE `mainmenu`
  ADD PRIMARY KEY (`seq`);

--
-- Indexes for table `ongkir_pembeli`
--
ALTER TABLE `ongkir_pembeli`
  ADD PRIMARY KEY (`id_ongkir`);

--
-- Indexes for table `pencairan_poin`
--
ALTER TABLE `pencairan_poin`
  ADD PRIMARY KEY (`kode_cairpoin`);

--
-- Indexes for table `submenu`
--
ALTER TABLE `submenu`
  ADD PRIMARY KEY (`id_sub`);

--
-- Indexes for table `tab_akses_mainmenu`
--
ALTER TABLE `tab_akses_mainmenu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tab_akses_submenu`
--
ALTER TABLE `tab_akses_submenu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_about`
--
ALTER TABLE `tb_about`
  ADD PRIMARY KEY (`id_about`);

--
-- Indexes for table `tb_kontak`
--
ALTER TABLE `tb_kontak`
  ADD PRIMARY KEY (`id_kontak`);

--
-- Indexes for table `tb_slider`
--
ALTER TABLE `tb_slider`
  ADD PRIMARY KEY (`id_slider`);

--
-- Indexes for table `td_barang`
--
ALTER TABLE `td_barang`
  ADD PRIMARY KEY (`fc_id`);

--
-- Indexes for table `td_chatlive`
--
ALTER TABLE `td_chatlive`
  ADD PRIMARY KEY (`fc_id`);

--
-- Indexes for table `td_keranjang_belanja`
--
ALTER TABLE `td_keranjang_belanja`
  ADD PRIMARY KEY (`fc_id`);

--
-- Indexes for table `td_konfirmasi_bayar`
--
ALTER TABLE `td_konfirmasi_bayar`
  ADD PRIMARY KEY (`fc_kdkonfirmasi`);

--
-- Indexes for table `td_order`
--
ALTER TABLE `td_order`
  ADD PRIMARY KEY (`fc_id_order_detail`);

--
-- Indexes for table `td_stok_barang_gudang`
--
ALTER TABLE `td_stok_barang_gudang`
  ADD PRIMARY KEY (`fc_kdstok_gudang`);

--
-- Indexes for table `tm_gudang`
--
ALTER TABLE `tm_gudang`
  ADD PRIMARY KEY (`fc_kdgudang`);

--
-- Indexes for table `tm_kategori_barang`
--
ALTER TABLE `tm_kategori_barang`
  ADD PRIMARY KEY (`fc_id`);

--
-- Indexes for table `tm_order`
--
ALTER TABLE `tm_order`
  ADD PRIMARY KEY (`fc_kdorder`);

--
-- Indexes for table `tm_poin`
--
ALTER TABLE `tm_poin`
  ADD PRIMARY KEY (`fc_kdpoin`);

--
-- Indexes for table `tm_user`
--
ALTER TABLE `tm_user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `tm_voucher`
--
ALTER TABLE `tm_voucher`
  ADD PRIMARY KEY (`fc_id_voucher`);

--
-- Indexes for table `t_barang_pindah`
--
ALTER TABLE `t_barang_pindah`
  ADD PRIMARY KEY (`fc_kdbarang_pindah`);

--
-- Indexes for table `t_bpbdtl`
--
ALTER TABLE `t_bpbdtl`
  ADD PRIMARY KEY (`fc_id`);

--
-- Indexes for table `t_bpbdtl_temp`
--
ALTER TABLE `t_bpbdtl_temp`
  ADD PRIMARY KEY (`fc_id`);

--
-- Indexes for table `t_bpbmst`
--
ALTER TABLE `t_bpbmst`
  ADD PRIMARY KEY (`fc_id`);

--
-- Indexes for table `t_bpbmst_temp`
--
ALTER TABLE `t_bpbmst_temp`
  ADD PRIMARY KEY (`fc_id`);

--
-- Indexes for table `t_nomor`
--
ALTER TABLE `t_nomor`
  ADD PRIMARY KEY (`kode`);

--
-- Indexes for table `t_setup`
--
ALTER TABLE `t_setup`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `t_status`
--
ALTER TABLE `t_status`
  ADD PRIMARY KEY (`fc_param`,`fc_kode`);

--
-- Indexes for table `t_temp_order`
--
ALTER TABLE `t_temp_order`
  ADD PRIMARY KEY (`fc_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_bank`
--
ALTER TABLE `data_bank`
  MODIFY `id_data` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `konfirmasi_bayar`
--
ALTER TABLE `konfirmasi_bayar`
  MODIFY `id_konfirmasi` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mainmenu`
--
ALTER TABLE `mainmenu`
  MODIFY `seq` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `ongkir_pembeli`
--
ALTER TABLE `ongkir_pembeli`
  MODIFY `id_ongkir` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `pencairan_poin`
--
ALTER TABLE `pencairan_poin`
  MODIFY `kode_cairpoin` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `submenu`
--
ALTER TABLE `submenu`
  MODIFY `id_sub` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tab_akses_mainmenu`
--
ALTER TABLE `tab_akses_mainmenu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `tab_akses_submenu`
--
ALTER TABLE `tab_akses_submenu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tb_about`
--
ALTER TABLE `tb_about`
  MODIFY `id_about` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_kontak`
--
ALTER TABLE `tb_kontak`
  MODIFY `id_kontak` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_slider`
--
ALTER TABLE `tb_slider`
  MODIFY `id_slider` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `td_barang`
--
ALTER TABLE `td_barang`
  MODIFY `fc_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `td_chatlive`
--
ALTER TABLE `td_chatlive`
  MODIFY `fc_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `td_keranjang_belanja`
--
ALTER TABLE `td_keranjang_belanja`
  MODIFY `fc_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `td_konfirmasi_bayar`
--
ALTER TABLE `td_konfirmasi_bayar`
  MODIFY `fc_kdkonfirmasi` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `td_order`
--
ALTER TABLE `td_order`
  MODIFY `fc_id_order_detail` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `td_stok_barang_gudang`
--
ALTER TABLE `td_stok_barang_gudang`
  MODIFY `fc_kdstok_gudang` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tm_gudang`
--
ALTER TABLE `tm_gudang`
  MODIFY `fc_kdgudang` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tm_kategori_barang`
--
ALTER TABLE `tm_kategori_barang`
  MODIFY `fc_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tm_poin`
--
ALTER TABLE `tm_poin`
  MODIFY `fc_kdpoin` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tm_voucher`
--
ALTER TABLE `tm_voucher`
  MODIFY `fc_id_voucher` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `t_barang_pindah`
--
ALTER TABLE `t_barang_pindah`
  MODIFY `fc_kdbarang_pindah` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_bpbdtl`
--
ALTER TABLE `t_bpbdtl`
  MODIFY `fc_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `t_bpbdtl_temp`
--
ALTER TABLE `t_bpbdtl_temp`
  MODIFY `fc_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_bpbmst`
--
ALTER TABLE `t_bpbmst`
  MODIFY `fc_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `t_bpbmst_temp`
--
ALTER TABLE `t_bpbmst_temp`
  MODIFY `fc_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_setup`
--
ALTER TABLE `t_setup`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_temp_order`
--
ALTER TABLE `t_temp_order`
  MODIFY `fc_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
DELIMITER $$
--
-- Events
--
CREATE DEFINER=`root`@`localhost` EVENT `reset` ON SCHEDULE EVERY 5 MINUTE STARTS '2018-10-18 16:25:52' ON COMPLETION NOT PRESERVE ENABLE DO update tm_voucher 
set fc_status='1' 
where fd_tgl_terbit_voucher < date_sub(now(),interval 24 HOUR) 
  and (fc_status='0')$$

DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
