<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('email');
        $this->load->model('toko_online_model');
        date_default_timezone_set("Asia/Jakarta");
    }

	public function index()
	{	
		echo "order_index";
	}

	public function konfirmasi_checkout(){

		// $pembeli = array(
		// 	'fullname'	=> $this->input->post('fullname'),
		// 	'email'		=> $this->input->post('email'),
		// 	'phone'		=> $this->input->post('phone'),
		// 	'address'	=> $this->input->post('kodepos'),
		// 	'provinsi'	=> $this->input->post('provinsi'),
		// 	'kota'		=> $this->input->post('kota')

		// 	);
		// $penjual = $this->toko_online_model->get_penjual_cart(array('keranjang_belanja.id_keranjang_belanja'=> $_SERVER['REMOTE_ADDR']));
		// $angka = 0;
		// foreach ($penjual as $p) {
		// 	$data_penjual[$angka] = $this->toko_online_model->get_table_where('user', array('id_user' => $p['id_user']));
		// 	$data_penjual[$angka]['ongkos_penjual'.$p['id_user']] = $this->input->post('ongkos_penjual'.$p['id_user']);
		// 	$data_penjual[$angka]['produk'] = $this->toko_online_model->get_produk_penjual_cart(array('keranjang_belanja.id_keranjang_belanja' =>  $_SERVER['REMOTE_ADDR'], 'produk.id_user' => $p['id_user']));
		// 	// print_r($data_penjual[$angka]['produk']);
		// 	// echo "<br><br><br>";
			
		// 	$angka++;
		// }
		// $data['pembeli'] = $pembeli;
		// $data['content'] = 'user/konfirmasi_checkout';
		// $data['data_penjual'] = $data_penjual;
		// $this->load->view('user/dashboard', $data);

		
	}

	public function simpan()
	{	
		//require_once(APPPATH.'libraries/PHPMailerAutoload.php');
		// if(($nama == '') || ($alamat  == '') || ($hp == '' || $prov == '' || $kota == '' || $kodepos == ''  || $total == 0 || !(is_numeric($ongkos)) )  ){
		// 	redirect("keranjang_belanja/check_out?error=ok");
		// }
		// echo $this->input->post('totalsimpan');




		$today = date("ymd"); //digunakan untuk menentukan format tanggal dan juga memanggil data tanggal saat ini.
		$query = $this->db->query("SELECT max(fc_kdorder) AS last FROM `tm_order` WHERE fc_kdorder LIKE '%$today%'");
		$data = $query->result_array();
		$last_id_order = $data[0]['last'];

		$last_no_urut  = substr($last_id_order, 7, 3); //memecah string yang ada di id order terakhir untuk membedakan tanggal dengan id yang di buat increment
		$next_no_urut  = $last_no_urut + 1;
		$next_id_order = "T".$today.sprintf('%03s', $next_no_urut); //menentukan huruf 'T' disetiap awal transaksi, di ikuti dengan tanggal sekarang, kemudian nomer id

		$nama 			= $this->input->post('fullname');
		$email			= $this->input->post('email');
		$phone			= $this->input->post('phone');
		$address		= $this->input->post('address');
		$desprovince	= $this->input->post('provinsi');
		$descity		= $this->input->post('kota');
		$kodepos		= $this->input->post('kodepos');
		$status_order 			= $this->input->post('status_order');
		$total		= $this->input->post('total');
		$ongkos_penjual2		= $this->input->post('ongkos_penjual2');
		$totalsimpan 			= $this->input->post('totalsimpan');
		//echo $ongkos_penjual2;
		// $query = $this->db->query('update tm_voucher set fc_status="2" where f_kode_voucher="'.$voucher.'"');    

		$data_insert = array(
			'fc_kdorder'		=> $next_id_order,
			'fd_tgl_order'		=> date("Y/m/d H:i:s"),
			'fm_total'	=> $total,
			'fc_status_kirim'	=> 1,
			'fv_nama_order'	=> $nama,
			'fv_email_order'	=> $email,
			'fv_alamat_order'	=> $address,
			'fc_telp'		=> $phone,
			'fc_kode_pos_order'=> $kodepos,
			'fv_provinsi_order'=> $desprovince,
			'fv_kota_order'	=> $descity,
			'fc_jenis_stok_order'	=> $status_order,
			'fm_ongkir_order' => $ongkos_penjual2,
			'fm_grandtotal_order' => $totalsimpan
			);
		$insert_order = $this->toko_online_model->insert_table('tm_order', $data_insert);
		//print_r($this->db->last_query());

		//poin
		$today = date("ymd"); //digunakan untuk menentukan format tanggal dan juga memanggil data tanggal saat ini.
		$query = $this->db->query("SELECT max(fc_kdorder) AS last FROM `tm_order` WHERE fc_kdorder LIKE '%$today%'");
		$data = $query->result_array();
		$last_id_order = $data[0]['last'];

		$last_no_urut  = substr($last_id_order, 7, 3); //memecah string yang ada di id order terakhir untuk membedakan tanggal dengan id yang di buat increment
		$next_no_urut  = $last_no_urut + 1;
		$next_id_orders = "CP".$today.sprintf('%03s', $next_no_urut); //menentukan huruf 'CP' disetiap awal transaksi, di ikuti dengan tanggal 
		if($insert_order){
			$data_order1 = $this->toko_online_model->get_keranjang_belanja(array('td_keranjang_belanja.fc_kdkeranjang_belanja'=> $_SERVER['REMOTE_ADDR']));
			
			$data_order = $this->toko_online_model->get_keranjang_belanja(array('td_keranjang_belanja.fc_kdkeranjang_belanja'=> $_SERVER['REMOTE_ADDR']));
			// print_r($data_order);
			foreach ($data_order as $d) {


				$where = array(
					'fc_kdorder' 		=> $next_id_order,
					'fc_penjual'	=> $d['fc_user'],
					'fc_kdbarang'		=> $d['fc_kdbarang']
				);
				$cek_data = $this->toko_online_model->get_table_where('td_order', $where);
				if($cek_data == null){
					$data_produk = $this->toko_online_model->get_table_where('td_barang', array('fc_kdbarang'=>$d['fc_kdbarang']));


					$data = array(
						'fc_kdorder'		=> $next_id_order,
						'fc_penjual' 	=> $d['fc_user'],
						'fc_kdbarang'		=> $d['fc_kdbarang'],
						'f_jumlah_produk'	=> $d['fn_jumlah_produk'],
						'f_berat_produk'	=> $d['fn_jumlah_produk'] * $d['fv_berat'],
						'fm_harga'			=> $d['fm_harga_produk'],
						'fm_subtotal'		=> $d['fm_subtotal_belanja'],
						'f_kode_voucher'    => $d['f_kode_voucher'],
						'fc_kdgudang'    => $d['fc_kdgudang'],
						);
					$insert_data = $this->toko_online_model->insert_table('td_order',$data);

				foreach ($data_order as $f) {
						$where = array(
							'fc_kdorder'	=>$next_id_order,
							'fc_kdbarang'		=> $f['fc_kdbarang']
						);

						// $where2 = array(

						// 	'fc_kdorder' 		=> $f['fc_kdorder'],
						// 	'fc_kdbarang'		=> $f['fc_kdbarang']
						// );

						$cek_voucher =   $this->toko_online_model->get_table_where('td_order', $where);

					if(@$cek_voucher[0]['f_kode_voucher']!=''){

					$where3 = array(

							'f_kode_voucher' 		=> $cek_voucher[0]['f_kode_voucher'],
						);	

					$ambil_user = 	$this->toko_online_model->get_table_where('tm_voucher', $where3);

					$data_produk = $this->toko_online_model->get_table_join_where('td_barang','td_keranjang_belanja','td_barang.fc_kdbarang = td_keranjang_belanja.fc_kdbarang', array('td_barang.fc_kdbarang'=>$f['fc_kdbarang']));
				    foreach($data_produk as $g){
					$data_voucher = $this->toko_online_model->get_table_join_where('td_barang','tm_voucher','td_barang.fc_kdbarang = tm_voucher.fc_kdbarang', array('td_barang.fc_kdbarang'=>$g['fc_kdbarang']));

					$where_id =   array(

							'f_kode_voucher' 		=> $cek_voucher[0]['f_kode_voucher'],
						);	

					$data_voucher2  =  $this->toko_online_model->get_table_where('tm_voucher', $where_id);

					$selisih = $g['fd_harga_barang_publish'] - $g['fd_harga_barang_min'];
					$keuntungan = ($g['fd_harga_barang_publish']-$data_voucher2[0]['fm_nominal']) - $g['fd_harga_barang_min'];
					$persentase = ($keuntungan/$selisih)*100;



					if($persentase>= 1  && $persentase<=25){
						if($data_produk[0]['fv_jenis_poin']=='dua poin'){
							$dapat_poin = 2;
						}else{
							$dapat_poin = 1;
						}
						$id_poin = 1;
					}else if($persentase>=26 && $persentase<=50){
						if($data_produk[0]['fv_jenis_poin']=='dua poin'){
							$dapat_poin = 4;
						}else{
							$dapat_poin = 2;
						}	
						
						$id_poin = 2;
					}else if($persentase>=51 && $persentase<=75){
						if($data_produk[0]['fv_jenis_poin']=='dua poin'){
							$dapat_poin = 6;
						}else{
							$dapat_poin = 3;
						}	
						
						$id_poin = 3;
					}else{
						if($data_produk[0]['fv_jenis_poin']=='dua poin'){
							$dapat_poin = 8;
						}else{
							$dapat_poin = 4;
						}
						$id_poin = 4;
					}
				  }

				 
				 //  	echo $persentase;
				 //  	echo "<br />";
					// echo $dapat_poin;
					$data_order2 = $this->toko_online_model->get_table_where('td_order', array('fc_kdbarang'=>$f['fc_kdbarang']));
					foreach($data_order2 as $ag){
						$ok = $ag['fc_penjual'];
						$ko = $ag['fc_id_order_detail'];
					}
				
					$data1 = array(
						'kode_cairpoin'		=> $next_id_orders,
						'id_user_dapat' 	=> $ambil_user[0]['id_user'],
						'fc_id_order_detail'=> $ko,
						'fc_kdpoin'			=> $id_poin,
						'selisih_harga'		=> $selisih,
						'keuntungan_harga'	=> $keuntungan,
						'persentasi'		=> $persentase,
						'total_poin'		=> $dapat_poin,
						'status_ambil'		=> 0,
						'tgl_ambil'			=> date("Y/m/d"),
						'id_user_pencair'	=> 3,
						);
				
					$insert_data = $this->toko_online_model->insert_table('pencairan_poin',$data1);

					}

				
					}	
					$total_ongkir = 0;
					if($insert_data){
						$penjual = $this->toko_online_model->get_penjual_cart(array('td_keranjang_belanja.fc_kdkeranjang_belanja'=> $_SERVER['REMOTE_ADDR']));
						foreach ($penjual as $p) {
							$total_ongkir = $total_ongkir +  $this->input->post('ongkos_penjual'.$p['fc_user']);
							$data = array(
								'ongkir'		=> $this->input->post('ongkos_penjual'.$p['fc_user']),
								'id_order'		=> $next_id_order,
								'id_penjual'	=> $p['fc_user'],
								'jasa_pengiriman'=>	$this->input->post('jenis_layanan_ongkir'.$p['fc_user'])
								);
							$insert_ongkir = $this->toko_online_model->insert_table('ongkir_pembeli', $data);
							$where = array(
								'id_order' 		=> $next_id_order,
								'id_penjual'	=> $p['fc_user']
								);
							$id_akhir = $this->toko_online_model->select_table_order_limit('id_ongkir', 'ongkir_pembeli', 'id_ongkir', 1);
							// $update_ongkir_detail_order = $this->toko_online_model->update_table('td_order', array('id_ongkir_pembeli' => $id_akhir[0]['id_ongkir']), $where);
						}
						$total_ongkir=0;
						$data_ongkir=$this->toko_online_model->get_table_where("ongkir_pembeli",array('id_order'=>$next_id_order));
						foreach ($data_ongkir as $key) {
							$total_ongkir=$total_ongkir+$key['ongkir'];
						}
						$update_ongkir_order = $this->toko_online_model->update_table('tm_order', 
						array('fm_ongkir_order' => $total_ongkir, 'fm_grandtotal_order' => $total_ongkir+$total),
						 array('fc_kdorder' => $next_id_order));
						$where = array('fc_kdkeranjang_belanja' => $_SERVER['REMOTE_ADDR']);
						$delete_keranjang = $this->toko_online_model->delete_table('td_keranjang_belanja',$where);
						$kode_barang = $d['fc_kdbarang'];
					

						
						
					}
					else{
						$berhasil = 0;
					}
				}
			}
			

			//echo $next_id_order;

			$this->load->library('email');

               $config['charset'] = 'utf-8';
               $config['useragent'] = 'Invoice Tagihan';
               $config['protocol'] = 'smtp';
               $config['mailtype'] = 'html';
               $config['smtp_host'] = 'ssl://smtp.gmail.com';
               $config['smtp_port'] = '465';
               $config['smtp_timeout'] = '5';
               $config['smtp_user'] = 'edwinlaksono12@gmail.com'; //isi dengan email gmail
               $config['smtp_pass'] = 'Aero1996'; //isi dengan password
               $config['crlf'] = "\r\n";
               $config['newline'] = "\r\n";
               $config['wordwrap'] = TRUE;
               $config['charset'] = 'iso-8859-1';


               $this->email->initialize($config);

               //$key = md5(md5(time()));
               $this->email->from('edwinlaksono12@gmail.com', "Invoice Tagihan");
               $this->email->to($email);
               $this->email->subject('Invoice Tagihan');

			   $data['id_order'] = $next_id_order;

			   $data['order']=$this->toko_online_model->get_table_where('tm_order', array('fc_kdorder' => $next_id_order));
		       $data['detail_order']=$this->toko_online_model->get_table_join_where('td_order','td_barang','td_order.fc_kdbarang=td_barang.fc_kdbarang', array('fc_kdorder' => $next_id_order));

		       //print_r($this->db->last_query());
               $body = $this->load->view('emails/email.php',$data,TRUE);
               $this->email->message($body);
               $this->email->send();
		       redirect(base_url('order/konfirmasi_bayar/'.$next_id_order));
			
		}
		else{
			echo "gagal,ada kesalahan";
		}
	}

	public function konfirmasi_pembayaran(){
		$data['content'] = 'user/konfirmasi_pembayaran';
		$this->load->view('user/dashboard', $data);
	}

	public function search_order($id_order){
		if($id_order == '0'){
			$id_order = $this->input->post('id_order');
		}
		$data_penjual = array();
		$angka = 0;
		$penjual = $this->toko_online_model->get_penjual_order(array('fc_kdorder' => $id_order));
		
		foreach ($penjual as $p) {
			$data_penjual[$angka] = $this->toko_online_model->get_order(array('fc_kdorder' => $id_order, 'fc_penjual' => $p['fc_penjual']));
			$angka++;


		}
		$data['data_penjual'] = $data_penjual;
		$data['penjual'] = $penjual;
		$data_order = $this->toko_online_model->get_table_where('tm_order', array('fc_kdorder' => $id_order));

		
			if($data_order[0]['fc_status_kirim'] == 1){
				$data['status_order'] = "Belum melakukan pembayaran, Silahkan lakukan pembayaran";
			}
			elseif($data_order[0]['fc_status_kirim'] == 2){
				$data['status_order'] = "Menunggu Konfirmasi admin, silahkan tunggu selama 2x24 jam";
			}
			elseif($data_order[0]['fc_status_kirim'] == 3){
				$data['status_order'] = "Admin telah menghubungi penjual untuk segera mengirim pesanan";
			}
			elseif($data_order[0]['fc_status_kirim'] == 4){
				$data['status_order'] = "Penjual Telah mengirim pesanan";
			}
			elseif($data_order[0]['fc_status_kirim'] == 5){
				$data['status_order'] = "Pesanan telah diterima pembeli,terima kasih";
			}
			elseif($data_order[0]['fc_status_kirim'] == 6){
				$data['status_order'] = "Pesanan telah lunas";
			}
			$data['content'] = 'user/form_konfirmasi_pembayaran';
			$data['data_order'] = $data_order;

			if($data_order[0]['fc_kdorder']==$id_order){

				$this->load->view('user/dashboard',$data);
			}else{
				 echo "<script language='javascript' >alert('Kode Order Anda Salah!!'); document.location='".base_url('home')."'</script>";
			}

	}
	
	function konfirmasi_bayar($next_id_order=null){
		
		$data['order']=$this->toko_online_model->get_table_where('tm_order', array('fc_kdorder' => $next_id_order));
		$data['detail_order']=$this->toko_online_model->get_table_join_where('td_order','td_barang','td_order.fc_kdbarang=td_barang.fc_kdbarang', array('fc_kdorder' => $next_id_order));
		$data['bank']=$this->db->query("select * from data_bank")->result_array();
			$data['id_order'] = $next_id_order;
		$data['content'] = "user/create_id_order";
		$this->load->view('user/dashboard',$data);	
	}

	function konfirmasi_bayar2(){
		$data['content'] = "user/create_id_order";
		$this->load->view('user/dashboard',$data);
	}

	public function form_konfirmasi_pembayaran(){
		$id_order 		= $this->input->post('id_order');
		$tanggal_order 	= $this->input->post('tanggal_transfer');
		$nama_pemilik	= $this->input->post('nama_pemilik');
		$no_rekening	= $this->input->post('no_rekening');
		$bank			= $this->input->post('bank');
		$total_transfer	= $this->input->post('total_transfer');
		$email	= $this->input->post('email');

		$olah = explode('.', $_FILES['userfile']['name']);
		$nama_file = $this->input->post('no_rekening').'.'.$olah[1];
		
		$gambar = str_replace(' ', '_', $nama_file);

		$config['upload_path'] = realpath('assets/images');
		$config['allowed_types']        = 'gif|jpg|png';
		$config['max_size'] = '2000000';
        $config['max_width'] = '2024';
        $config['max_height']= '1468';
		$config['file_name'] = $nama_file;	
		
		$this->load->library('upload', $config);
 		$this->upload->initialize($config);
		$this->upload->do_upload('userfile');

		$this->load->library('email');

               $config['charset'] = 'utf-8';
               $config['useragent'] = 'Konfirmasi Pembayaran';
               $config['protocol'] = 'smtp';
               $config['mailtype'] = 'html';
               $config['smtp_host'] = 'ssl://smtp.gmail.com';
               $config['smtp_port'] = '465';
               $config['smtp_timeout'] = '5';
               $config['smtp_user'] = 'edwinlaksono12@gmail.com'; //isi dengan email gmail
               $config['smtp_pass'] = 'Aero1996'; //isi dengan password
               $config['crlf'] = "\r\n";
               $config['newline'] = "\r\n";
               $config['wordwrap'] = TRUE;
               $config['charset'] = 'iso-8859-1';


               $this->email->initialize($config);

               //$key = md5(md5(time()));
               $this->email->from('edwinlaksono12@gmail.com', "Konfirmasi Pembayaran");
               $this->email->to($email);
               $this->email->subject('Konfirmasi Pembayaran');

			   $data['id_order'] = $id_order;

			   $data['order']=$this->toko_online_model->get_table_where('tm_order', array('fc_kdorder' => $id_order));
		       $data['detail_order']=$this->toko_online_model->get_table_join_where('td_order','td_barang','td_order.fc_kdbarang=td_barang.fc_kdbarang', array('fc_kdorder' => $id_order));

		       //print_r($this->db->last_query());
               $body = $this->load->view('emails/email_konfirmasi.php',$data,TRUE);
               $this->email->message($body);
               $this->email->send();

		$data_insert = array(
			'fd_tgl_konfirmasi'	=> $tanggal_order,
			'fc_kdorder'			=> $id_order,
			'fm_jumlah_bayar'		=> $total_transfer,
			'fc_bank_bayar'		=> $bank,
			'fc_rekening_bayar'	=> $no_rekening,
			'fv_nama_bayar'		=> $nama_pemilik,
			'fc_img'		=> $gambar
			);

		$insert_konfirmasi_bayar = $this->toko_online_model->insert_table('td_konfirmasi_bayar',$data_insert);
		if($insert_konfirmasi_bayar){
			$data = array('fc_status_kirim' => 2 );
			$where = array('fc_kdorder' => $id_order);
			$update_status_order = $this->toko_online_model->update_table('tm_order',$data,$where);
			redirect('order/search_order/'.$id_order);
		}
		else{
			echo "<script>
				alert('Maaf, ada kesalahan pengisian Formulir');
				window.history.go(-1);

		</script>";	
		}
	}

	public function komplain_barang($id_detail_order=null){

		$data_order= $this->toko_online_model->get_table_where('detail_order', array('id_detail_order' => $id_detail_order ));
		$user= $this->toko_online_model->get_table_where('user', array('id_user' => $data_order[0]['id_penjual'] ));
		$order= $this->toko_online_model->get_table_where('order', array('id_order' => $data_order[0]['id_order'] ));

		$produk= $this->toko_online_model->get_table_where('produk', array('id_produk' => $data_order[0]['id_produk'] ));


		$data_komplain= $this->toko_online_model->get_table_where('komplain_barang', array('id_detail_order' => $id_detail_order ));
		// $id_detail_order= $this->input->post('id_detail_order');
		// $data_detail_order = $this->toko_online_model->get_order(array('id_detail_order' => $id_detail_order));
		// $data_order= $this->toko_online_model->get_table_where('order', array('id_order' => $data_detail_order[0]['id_order']));
		// $data_penjual = $this->toko_online_model->get_table_where('user', array('id_user' => $data_detail_order[0]['id_penjual']));

		
		$data = array(
				'data_order'			=> $data_order,
				'user'		=>	$user,
				'order'		=> $order,
				'produk'	=>	$produk,
				'komplain_barang'	=>	$data_komplain
			
			);
		$data['content'] = 'user/komplain_barang';
		$this->load->view('user/dashboard', $data);
	}

	public function simpan_komplain_barang(){
		$id_detail_order = $this->input->post('id_detail_order');
		$id_penjual = $this->input->post('id_penjual');
		$pesan_komplain = $this->input->post('pesan_komplain');

		$jenis_komplain = $this->input->post('jenis_komplain');

		$jumlah_produk_komplain = $this->input->post('jumlah_produk_komplain');

		$data_insert = array(
				'id_detail_order'		=> $id_detail_order,
				'id_penjual'			=> $id_penjual,
				'pesan_komplain'		=> $pesan_komplain,
				'bukti_komplain'		=> '',
				'jumlah_produk_komplain'=>	$jumlah_produk_komplain,
				'jenis_komplain'		=>	$jenis_komplain,
				'tgl_komplain'			=>	date("Y-m-d")
				);
		
		$insert =$this->toko_online_model->insert_table('komplain_barang', $data_insert);
		if($insert){
			
                        $foto1 = $_FILES['foto1']['name'];
                        $ext_foto1 = pathinfo($foto1, PATHINFO_EXTENSION);
                        $nama_foto_bukti = 'bukti_' . $id_detail_order.$jenis_komplain.$pesan_komplain;
                        $config['upload_path'] = './assets/img/bukti_komplain/';
                        $config['allowed_types'] = 'gif|jpg|jpeg|png|GIF|JPG|JPEG|PNG';
                        $config['file_name'] = 'foto_' . $nama_foto_bukti;
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);
                        $this->upload->do_upload('foto1');

                        $data_gambar=$this->upload->data();


            $where = array(
            	'id_detail_order'		=> $id_detail_order,
            	'id_penjual'			=> $id_penjual 
            	);
            $update = $this->toko_online_model->update_table('komplain_barang', array('bukti_komplain' =>  $data_gambar['file_name']), $where);


            $dtl = $this->toko_online_model->update_table('detail_order', array('status_detail_komplain' =>  1),array('detail_order.id_detail_order' => $id_detail_order) );

            // $update_status_order = $this->toko_online_model->update_table('order',array('status_order' => 5) , array('id_order' => $id_order));
            // $update_status_pengiriman = $this->toko_online_model->update_table('detail_order',array('status_kirim' => 2) , array('id_order' => $id_order, 'id_penjual' => $id_penjual));
            // $cek_status_kirim = $this->toko_online_model->get_table_where('detail_order' , array('id_order' => $id_order , 'status_kirim' => 1));
            // if($cek_status_kirim == null){
            // 	 $update_status_order = $this->toko_online_model->update_table('order',array('status_order' => 5) , array('id_order' => $id_order));
            // }
		}
		$detail_order = $this->toko_online_model->get_order(array('detail_order.id_detail_order' => $id_detail_order));
		$get_penjual = $this->toko_online_model->get_table_where('user', array('id_user' => $id_penjual));
		$data = array(
				'nama_penjual'	=> $get_penjual[0]['nama'],
				'id_order'		=> $detail_order[0]['id_order'],
				'detail_order'	=> $detail_order
			);

		$data['content'] = 'user/komplain';
		$this->load->view('user/dashboard', $data);
		
		
	}

	public function refund_uang(){
		$id_detail_order= $this->input->post('id_detail_order');
		$data_detail_order = $this->toko_online_model->get_order(array('id_detail_order' => $id_detail_order));
		$data_order= $this->toko_online_model->get_table_where('order', array('id_order' => $data_detail_order[0]['id_order']));
		$data_penjual = $this->toko_online_model->get_table_where('user', array('id_user' => $data_detail_order[0]['id_penjual']));

		
		$data = array(
				'produk'			=> $data_detail_order,
				'data_order'		=> $data_order,
				'penjual'			=> $data_penjual
			);
		$data['content'] = 'user/refund_uang';
		$this->load->view('user/dashboard', $data);
	}

	public function simpan_refund_uang(){
		$id_detail_order	= $this->input->post('id_detail_order');
		$no_rekening		= $this->input->post('no_rekening');
		$atm				= $this->input->post('atm');
		$nama_penerima		= $this->input->post('nama_penerima');

		$data_insert = array(
				'id_detail_order'	=> $id_detail_order,
				'no_rekening'		=> $no_rekening,
				'ATM'				=> $atm,
				'nama_penerima'		=> $nama_penerima
			);

		$insert = $this->toko_online_model->insert_table('data_refund', $data_insert);
		if($insert){
			$this->toko_online_model->update_table('detail_order', array('status_kirim' => 3), array('id_detail_order'=> $id_detail_order));
		}

		$data_order = $this->toko_online_model->get_order(array('id_detail_order' => $id_detail_order));
		$data = array(
			'no_rekening'	=> $no_rekening,
			'atm'			=> $atm,
			'nama_penerima'	=> $nama_penerima,
			'data_order'	=> $data_order,
			'content'		=> 'user/refund',
			'id_order'		=> $data_order[0]['id_order']
			);

		$this->load->view('user/dashboard', $data);
	}


	function konfirmasi_pengembalian_barang(){
		//var_dump($this->input->post());

		$data_insert=array(
			'id_komplain_barang'	=>	$this->input->post('id_komplain'),
			'id_detail_order'	=>	$this->input->post('id_detail_order'),
			'id_order'	=>	$this->input->post('id_order'),
			'no_resi_pengembalian'=>$this->input->post('no_resi'),
			'no_rek'=>$this->input->post('no_rek'),
			'jenis_bank'=>$this->input->post('jenis_bank'),
			
			'nama_rek'=>$this->input->post('nama_rek')

		);

		$insert = $this->toko_online_model->insert_table('konfirmasi_pengembalian_produk', $data_insert);

		$this->toko_online_model->update_table('komplain_barang', array('status_komplain' => "Disetujui dan Dalam Proses Pengiriman"), array('id_detail_order'=> $this->input->post('id_detail_order')));

		redirect(base_url('user/order/komplain_barang/'.$this->input->post('id_detail_order')));
	}
	public function ajax_edit($id)
	{
		echo json_encode($this->toko_online_model->get_tmorder($id));
	}
	public function ubahstatus()
	{
		$data_insert=array(
			'fc_status_kirim'	=>	$this->input->post('fc_status_kirim')
		);

		// $insert = $this->toko_online_model->insert_table('konfirmasi_pengembalian_produk', $data_insert);

		$this->toko_online_model->update_table('tm_order', $data_insert, array('fc_kdorder' => $this->input->post('fc_kdorder')));
		// echo $this->db->last_query();

		redirect(base_url('user/order/search_order/'.$this->input->post('fc_kdorder')));
	}


	
}
