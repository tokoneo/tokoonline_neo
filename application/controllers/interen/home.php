<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('toko_online_model');
		$this->load->model('mdl_cart');
		$this->load->model('mdl_cart_user');
		$this->load->model('mdl_stok');
		$this->load->library('session');
		$this->auth->restrict();
	}

	public function index()
	{	
		// $data['terbaru'] = $this->toko_online_model->get_produk('terbaru');
	 
	    $data['data_slider']	= $this->toko_online_model->get_slider();
	    $data['data_terbaru']   = $this->toko_online_model->get_produk_terbaru();
	    $data['kategori']		=  $this->toko_online_model->get_kategori_produk();
	    $data['barang']	=  $this->toko_online_model->get_data_produk();
	   // $data['barang']			=  $this->toko_online_model->view();
		$data['content'] = 'interen/index';
		$this->load->view('interen/dashboard', $data );
		
		
	}

	 public function search(){
	    // Ambil data NIS yang dikirim via ajax post
	    $keyword = $this->input->post('keyword');
	    // $barang['kategori']		=  $this->toko_online_model->get_kategori_produk();
	    $barang = $this->toko_online_model->search($keyword);
	    //print_r($this->db->last_query());
	    // Kita load file view.php sambil mengirim data siswa hasil query function search di SiswaModel
	    $hasil = $this->load->view('interen/view', array('td_barang'=>$barang), true);
	    
	    // Buat sebuah array
	    $callback = array(
	      'hasil' => $hasil, // Set array hasil dengan isi dari view.php yang diload tadi
	    );
	    echo json_encode($callback); // konversi varibael $callback menjadi JSON
	  }

	function add_chart($id_produk){
		$data['content'] = 'interen/add_chart';
		$data['produk_detail'] = $this->toko_online_model->get_table_where('td_barang', array('fc_kdbarang' => $id_produk));
		$data['stok_produk'] = $this->toko_online_model->get_where_stok($id_produk);
		//print_r($this->db->last_query());
		$this->load->view('interen/dashboard', $data );
	}

	function add_chart_preorder($id_produk){
		$data['content'] = 'interen/add_chart_preorder';
		$data['produk_detail'] = $this->toko_online_model->get_table_where('td_barang', array('fc_kdbarang' => $id_produk));
		$data['stok_produk'] = $this->toko_online_model->get_where_stok($id_produk);
		//print_r($this->db->last_query());
		$this->load->view('interen/dashboard', $data );
	}

	function cek_pesan(){
		echo json_encode($this->toko_online_model->get_cek_pesan());
	}

	function cek_pesan_user(){
		$user = $this->session->userdata('id_user');
		echo json_encode($this->toko_online_model->get_cek_pesan_user($user));
	}
	

	function load_cart(){ //load data cart
		echo $this->show_cart();
	}

	function view_cart(){
		$user = $this->session->userdata('id_user');
		$data['cart'] = $this->toko_online_model->get_keranjang_belanja2(array('td_keranjang_belanja.fc_kdkeranjang_belanja'=> $user));
		$data['content'] = 'interen/view_cart';
		$this->load->view('interen/dashboard', $data );
	}

	public function keranjang_belanja(){
		$ip = $this->input->post('ip_number');
		$id_produk = $this->input->post('fc_kdbarang');
		$harga = $this->input->post('harga');
		$quantity = $this->input->post('quantity');
		$status_stok = $this->input->post('fc_status_stok');
		$kode_gudang = $this->input->post('fc_kdgudang');
		$status = $this->input->post('fc_status');
		$subtotal = $harga * $quantity;

		// echo $ip;
		$ambil_data = $this->toko_online_model->get_table_where('td_keranjang_belanja', array('fc_kdkeranjang_belanja' => $ip , 'fc_kdbarang' => $id_produk));

		

		
		if($ambil_data != null){
			$quantity_new = $ambil_data[0]['fn_jumlah_produk'] + $quantity;
			$subtotal_new = $ambil_data[0]['fm_subtotal_belanja'] + $subtotal;

			$data = array(
					'fn_jumlah_produk' => $quantity_new,
					'fm_subtotal_belanja' => $subtotal_new
				);

			$update = $this->toko_online_model->update_table('td_keranjang_belanja',$data, array('fc_kdkeranjang_belanja' => $ip , 'fc_kdbarang' => $id_produk));
		}
		else{
			$data = array(
			'fc_kdkeranjang_belanja' 	=> $ip,
			'fc_kdbarang'				=> $id_produk,
			'fc_kdgudang'				=> $kode_gudang,
			'fm_harga_produk'			=> $harga,
			'fn_jumlah_produk'			=> $quantity,
			'fm_subtotal_belanja'		=> $subtotal,
			'fc_status_stok'            => $status_stok,
			'fc_status'					=> $status
			);
			$insert = $this->toko_online_model->insert_table('td_keranjang_belanja',$data);
		}

		// $data_temporary = array(
		// 						'fc_kdbarang' 		=> $id_produk,
		// 						'fn_quantity' 		=> $quantity,
		// 						'fc_kdgudang' 		=> $kode_gudang,
		// 						'fc_kdkeranjang_belanja' 	=> $ip,
		// 					);	

		// $insert_temporary = $this->toko_online_model->insert_table('t_temp_order', $data_temporary);


		$ambil_data_qty = $this->toko_online_model->get_table_where('td_stok_barang_gudang', array('fc_kdbarang' => $id_produk , 'fc_kdgudang' => $kode_gudang));

		if($ambil_data_qty != null){
			$quantity_update = $ambil_data_qty[0]['fc_qty_barang'] - $quantity;

				$data_qty = array(
					'fc_qty_barang' => $quantity_update,
				);

			$update_qty = $this->toko_online_model->update_table('td_stok_barang_gudang',$data_qty, array('fc_kdbarang' => $id_produk , 'fc_kdgudang' => $kode_gudang));

			//print_r($this->db->last_query());
		}	

		// $query_update = $this->db->query("update td_stok_barang_gudang set fc_qty_barang = fc_qty_barang-".$quantity." where fc_kdbarang=".$id_produk." and fc_kdgudang=".$fc_kdgudang."");
		// print_r($this->db->last_query());
	}



	public function remove_keranjang_belanja($id){
		$where = array(
			'fc_id' 	=> $id
			);
		$this->toko_online_model->delete_table('td_keranjang_belanja', $where);
		//print_r($this->db->last_query());
		redirect('interen/view_cart');
	}

	public function checkout(){
		$user = $this->session->userdata('id_user');
		$data['cart'] = $this->toko_online_model-> get_keranjang_belanja(array('td_keranjang_belanja.fc_kdkeranjang_belanja'=> $user));
		$penjual = $this->toko_online_model->get_penjual_cart(array('td_keranjang_belanja.fc_kdkeranjang_belanja'=> $user));
		$angka = 0;
		foreach ($penjual as $p) {
			$data_penjual[$angka] = $this->toko_online_model->get_table_where('tm_user', array('id_user' => $p['fc_user']));
			$angka++;
		}
		
		// $data_penjual = array();
		// $angka = 0;
		// foreach ($data['cart'] as $cart) {
		// 	$data_penjual[$angka] = $this->toko_online_model->get_penjual(array('produk.id_produk' => $cart['id_produk']));
		// 	// print_r($data_penjual[$angka]);
		// 	// echo "<br>";
		// 	$angka++;
		// }
		// foreach ($data_penjual as $penjual) {
		// 	if
		// }
		
		$data['penjual'] = $data_penjual;
		
		$data['content'] = 'interen/checkout';
		$this->load->view('interen/dashboard',$data);
	}

	public function tentang(){
		$data['content'] = 'interen/tentang';
		$data['tentang'] = $this->toko_online_model->tentang()->row();
		$this->load->view('interen/dashboard',$data);
	}

	public function foto(){
		$data['content'] = 'interen/foto';
		$data['foto'] = $this->toko_online_model->foto()->result_array();
		$this->load->view('interen/dashboard',$data);
	}

	// public function kontak(){
	// 	$data['content'] = 'interen/kontak';
	// 	$data['kontak'] = $this->toko_online_model->kontak()->row();
	// 	$this->load->view('interen/dashboard',$data);
	// }

	public function update_keranjang_belanja(){
		$kode_barang = $this->input->post('kode_barang');
		$kode_gudange = $this->input->post('kode_gudang');
		$id_produk = $this->input->post('fc_kdbarang');
		$kode_gudang = $this->input->post('fc_kdgudang');
		$id_keranjang_belanja = $this->input->post('fc_kdkeranjang_belanja');
		$quantity = $this->input->post('quantity');
		// print_r($id_produk);
		// print_r();
		foreach ($id_produk as $id) {
			$where = array(
					'fc_kdkeranjang_belanja' 	=> $id_keranjang_belanja,
					'fc_kdbarang'				=> $id
				);

			
			$cek_q = $this->toko_online_model->get_table_where('td_keranjang_belanja', $where);
			if($cek_q[0]['fn_jumlah_produk'] != $quantity[$id] ){
				$data = array(
						'fn_jumlah_produk' 	=> $quantity[$id],
						'fm_subtotal_belanja'	=> $quantity[$id]*$cek_q[0]['fm_harga_produk']
					);
				$this->toko_online_model->update_table('td_keranjang_belanja',$data,$where);

				$ambil_data_qty = $this->toko_online_model->get_table_where('td_stok_barang_gudang', array('fc_kdbarang' => $kode_barang , 'fc_kdgudang' => $kode_gudange));
				//hasil = jumlah sekarang - jumlah lama 
				$hasil = $quantity[$id] -  $cek_q[0]['fn_jumlah_produk'];
				$hasil2  = $cek_q[0]['fn_jumlah_produk'] - $quantity[$id];
				if(  $cek_q[0]['fn_jumlah_produk'] < $quantity[$id] ){
				$quantity_update = $ambil_data_qty[0]['fc_qty_barang'] - $hasil;

							$data_qty = array(
								'fc_qty_barang' => $quantity_update,
							);

				$update_qty = $this->toko_online_model->update_table('td_stok_barang_gudang',$data_qty, array('fc_kdbarang' => $kode_barang , 'fc_kdgudang' => $kode_gudange));
				//print_r($this->db->last_query());
				}else{
					$quantity_update2 = $ambil_data_qty[0]['fc_qty_barang'] + $hasil2;

							$data_qty2 = array(
								'fc_qty_barang' => $quantity_update2,
							);

				$update_qtye = $this->toko_online_model->update_table('td_stok_barang_gudang',$data_qty2, array('fc_kdbarang' => $kode_barang , 'fc_kdgudang' => $kode_gudange));
				//print_r($this->db->last_query());
				}
			}

			
		}
		 echo json_encode(array("status" => TRUE));

	}

	public function ajax_list() {
		$where = $this->uri->segment(3);
		$list = $this->mdl_cart_user->get_datatables($where);
		//print_r($this->db->last_query());
		$data = array();
		$no = $_REQUEST['start'];
		foreach ($list as $produk) {
			if(@$produk->fc_kdbarang){
			if($produk->fc_img_1==''){ $cover = 'no_image.jpg'; }else{ $cover = $produk->fc_img_1; }
			$row2 = '<img src="'.base_url().'assets/images/'.$cover.'">';
			$no++;
			$row = array();
			$row[] = '
			<input type="hidden" name="kode_barang" value="'.$produk->fc_kdbarang.'">
			<input type="hidden" name="kode_gudang" value="'.$produk->gudang.'">
			<input type="hidden" name="fc_kdbarang['.$produk->fc_kdbarang.']" value="'.$produk->fc_kdbarang.'">
			<input type="hidden" name="fc_kdgudang['.$produk->fc_kdgudang.']" value="'.$produk->fc_kdgudang.'">
            <input type="checkbox" name="cb_data[]" id="cb_data[]" value="'.$produk->fc_id.'">'; 
			$row[] = '<div class="how-itemcart1">'.$row2.'</div>';
			$row[] = ''.$produk->fv_nama_barang.'';
			$row[] = 'Rp. '.nominal($produk->fm_harga_produk).'';
			$awal = '<select class="js-select2" name="quantity['.$produk->fc_kdbarang.']">';
			$akhir = '</select><div class="dropDownSelect2"></div>';
			$tengah = '';
			for ($i=1; $i <= $produk->fc_qty_barang ; $i++) { 
              if($i == $produk->fn_jumlah_produk){
              
                  $tengah =$tengah.'<option selected value="'.$i.'">'.$i.'</option>';
                
              }
              else{
              
                  $tengah = $tengah.'<option value="'.$i.'">'.$i.'</option>';
                
              }
            }
			$row[] = $awal.$tengah.$akhir;
			$row[] = 'Rp. '.nominal($produk->fm_subtotal_belanja).'';
			$row[] = '<a href="javascript:void(0)" onclick="hapus('.$produk->id.',\''.$produk->fc_kdbarang.'\','.$produk->gudang.','.$produk->fn_jumlah_produk.')" type="button"  class="hapus_cart btn btn-danger btn-xs">Batal</a></td>';
			$data[] = $row;
		}
		}

		$output = array(
						"draw" => $_REQUEST['draw'],
						"recordsTotal" => $this->mdl_cart_user->count_all($where),
						"recordsFiltered" => $this->mdl_cart_user->count_filtered($where),
						"data" => $data,
				);
		echo json_encode($output);
	}

	public function ajax_delete($id, $id2, $id3, $id4) {
	$ambil_data_qty = $this->toko_online_model->get_table_where('td_stok_barang_gudang', array('fc_kdbarang' => $id2 , 'fc_kdgudang' => $id3));
	//print_r($this->db->last_query());
	$quantity_update = $ambil_data_qty[0]['fc_qty_barang'] + $id4;

				$data_qty = array(
					'fc_qty_barang' => $quantity_update,
				);

	$update_qty = $this->toko_online_model->update_table('td_stok_barang_gudang',$data_qty, array('fc_kdbarang' => $id2 , 'fc_kdgudang' => $id3));
	// print_r($this->db->last_query());
		
    $this->mdl_cart->delete_by_id($id);
      // print_r($this->db->last_query());
     echo json_encode(array("status" => TRUE));
    }

    function load_subtotal(){ //load data cart
		echo $this->show_subtotal();
	}

	function show_subtotal(){ //Fungsi untuk menampilkan Cart
		$user = $this->session->userdata('id_user');
		$cart = $this->toko_online_model->get_subtotal(array('td_keranjang_belanja.fc_kdkeranjang_belanja'=> $user));
		$output = '';

		$total = 0;

		foreach ($cart as $items) {
			$total = $total + $items['fm_subtotal_belanja'];
		}	
		$output .= '
				Rp. '.@nominal($total).'
		';

		return $output;
	}	

	function kat_detail_produk($id){
		$data['kat_nama']= $this->toko_online_model->tampil_kategori($id)->row();
		$data['produk']= $this->toko_online_model->tampil_kategori($id)->result_array();
		$data['content'] = 'interen/produk';
		$this->load->view('interen/dashboard',$data);
	}

	function login(){
		$this->load->view('login');
	}

	function popup_stok(){
	 	$this->load->view('interen/popup_stok');
	 }

	function create_load($id){
		$data2['fc_kdbarang'] = $id;
		$this->load->view('interen/popup_stok', $data2);
	} 

	public function ajax_list_stok() {
		$where = $this->uri->segment(4);
		$list = $this->mdl_stok->get_datatables($where);
		//print_r($this->db->last_query());
		$data = array();
		$no = $_REQUEST['start'];
		foreach ($list as $produk) {
			if(@$produk->fc_kdbarang){
			$no++;
			$row = array();
		
			$row[] = $produk->fv_nmgudang;
			$row[] = $produk->fv_nama_barang;
			$awal = '
			<select class="js-select2" id="quantityne'.$produk->fc_kdstok_gudang.'" name="quantity['.$produk->fc_kdbarang.']">';
			$akhir = '</select><div class="dropDownSelect2"></div>';
			$tengah = '';
			for ($i=1; $i <= $produk->fc_qty_barang ; $i++) { 
              
                  $tengah =$tengah.'<option selected value="'.$i.'">'.$i.'</option>';
                
            
            }
			$row[] = $awal.$tengah.$akhir;
				$row[] = '<button type="button" class="btn btn-primary" onclick="pilihDataitem('.$produk->fc_kdstok_gudang.')">Ambil Data</button>';
			$data[] = $row;
		}
		}

		$output = array(
						"draw" => $_REQUEST['draw'],
						"recordsTotal" => $this->mdl_stok->count_all($where),
						"recordsFiltered" => $this->mdl_stok->count_filtered($where),
						"data" => $data,
				);
		echo json_encode($output);
	} 

	function about(){
		$data['tentang'] = $this->toko_online_model->tentang();
 		$data['content'] = 'interen/about';
		$this->load->view('interen/dashboard',$data);
	}

	function kontak(){
		$data['kontak'] = $this->toko_online_model->kontak();
 		$data['content'] = 'interen/kontak';
		$this->load->view('interen/dashboard',$data);
	}

}

/* End of file welcome.php */
