<?php

class Cart extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->model('cart_model');
		$this->load->model('toko_online_model');
	}

	function index(){
		$data['data']=$this->cart_model->get_all_produk();
		$this->load->view('v_cart',$data);
	}

	function add_to_cart(){ //fungsi Add To Cart
		$data = array(
			'id' => $this->input->post('produk_id'), 
			'name' => $this->input->post('produk_nama'), 
			'price' => $this->input->post('produk_harga'), 
			'qty' => $this->input->post('quantity'), 
		);
		$this->cart->insert($data);
		echo $this->show_cart(); //tampilkan cart setelah added
	}

	function show_cart(){ //Fungsi untuk menampilkan Cart
		$user = $this->session->userdata('id_user');

		$cart = $this->toko_online_model-> get_keranjang_belanja(array('td_keranjang_belanja.fc_kdkeranjang_belanja'=> $user));
		$output = '';
		$no = 0;
		 $total = 0;

		 if($cart==true){
		foreach ($cart as $items) {
		 $total = $total + $items['fm_subtotal_belanja'];
			$output .='
				
                    <li class="header-cart-item flex-w flex-t m-b-12">
                        <div class="header-cart-item-img">
                            <img src="'.base_url().'assets/images/'.$items['fc_img_1'].'" alt="IMG">
                        </div>

                        <div class="header-cart-item-txt p-t-6">
                            <div  class="header-cart-item-name m-b-18 hov-cl1 trans-04">
                                '.$items['fv_nama_barang'].'
                                
                            </div>

                            <span class="header-cart-item-info">
                               '.$items['fn_jumlah_produk'].' x Rp. '.nominal($items['fm_harga_produk']).'
                            </span>
                           
                        </div>

                    </li>

			';
		}
		$output .= '
			  <div class="header-cart-total w-full p-tb-40">
                        Total: Rp. '.nominal($total).'
                    </div>
              <div class="header-cart-buttons flex-w w-full">
						<a href="'.base_url('interen/home/view_cart').'" class="flex-c-m stext-101 cl0 size-107 bg3 bor2 hov-btn3 p-lr-15 trans-04 m-r-8 m-b-10">
							View Cart
						</a>

						<a href="'.base_url('interen/home/checkout').'" class="flex-c-m stext-101 cl0 size-107 bg3 bor2 hov-btn3 p-lr-15 trans-04 m-b-10">
                            Check Out
                		</a>      
					</div>      
              
		';
		return $output;
	}else{
		echo "<b><h3>Keranjang Anda masih kosong</h3></b><br /><hr />
	Temukan berbagai produk unggulan kami dan nikmati penawaran terbaik lainnya!";
	}
	}

	function load_cart(){ //load data cart
		echo $this->show_cart();
	}

	function hapus_cart(){ //fungsi untuk menghapus item cart
		$data = array(
			'rowid' => $this->input->post('row_id'), 
			'qty' => 0, 
		);
		$this->cart->update($data);
		echo $this->show_cart();
	}
}