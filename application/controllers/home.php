<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('toko_online_model');
		$this->load->model('mdl_cart');
		$this->load->library('session');
		date_default_timezone_set("Asia/Jakarta");
	}
 
	public function index()
	{	
		// $data['terbaru'] = $this->toko_online_model->get_produk('terbaru');
	 
	    $data['data_slider']	= $this->toko_online_model->get_slider();
	    $data['data_terbaru']   = $this->toko_online_model->get_produk_terbaru();
	    $data['kategori']		=  $this->toko_online_model->get_kategori_produk();
	    $data['barang']	=  $this->toko_online_model->get_data_produk();
	   // $data['barang']			=  $this->toko_online_model->view();
		$data['content'] = 'user/index';
		$this->load->view('user/dashboard', $data );
		
		
	}

	 public function search(){
	    // Ambil data NIS yang dikirim via ajax post
	    $keyword = $this->input->post('keyword');
	    // $barang['kategori']		=  $this->toko_online_model->get_kategori_produk();
	    $barang = $this->toko_online_model->search($keyword);
	    //print_r($this->db->last_query());
	    // Kita load file view.php sambil mengirim data siswa hasil query function search di SiswaModel
	    $hasil = $this->load->view('user/view', array('td_barang'=>$barang), true);
	    
	    // Buat sebuah array
	    $callback = array(
	      'hasil' => $hasil, // Set array hasil dengan isi dari view.php yang diload tadi
	    );
	    echo json_encode($callback); // konversi varibael $callback menjadi JSON
	  }

	function add_chart($id_produk){
		$data['content'] = 'user/add_chart';
		$data['produk_detail'] = $this->toko_online_model->get_table_where('td_barang', array('fc_kdbarang' => $id_produk));
		$data['stok_produk'] = $this->toko_online_model->get_where_stok($id_produk);
		//print_r($this->db->last_query());
		$this->load->view('user/dashboard', $data );
	}

	function add_chart_preorder($id_produk){
		$data['content'] = 'user/add_chart_preorder';
		$data['produk_detail'] = $this->toko_online_model->get_table_where('td_barang', array('fc_kdbarang' => $id_produk));
		$data['stok_produk'] = $this->toko_online_model->get_where_stok($id_produk);
		//print_r($this->db->last_query());
		$this->load->view('user/dashboard', $data );
	}

	function cek_pesan(){
		echo json_encode($this->toko_online_model->get_cek_pesan());
	}
	

	function load_cart(){ //load data cart
		echo $this->show_cart();
	}

	function view_cart(){
		$data['cart'] = $this->toko_online_model->get_keranjang_belanja2(array('td_keranjang_belanja.fc_kdkeranjang_belanja'=> $_SERVER['REMOTE_ADDR']));
		$data['content'] = 'user/view_cart';
		$this->load->view('user/dashboard', $data );
	}

	public function keranjang_belanja(){
		$ip = $this->input->post('ip_number');
		$id_produk = $this->input->post('fc_kdbarang');
		$harga = $this->input->post('harga');
		$quantity = $this->input->post('quantity');
		$status_stok = $this->input->post('fc_status_stok');
		$kode_gudang = $this->input->post('fc_kdgudang');
		$status = $this->input->post('fc_status');
		$kode_voucher = $this->input->post('f_kode_voucher');
		$subtotal = $harga * $quantity;

		// echo $ip;

		$ambil_data = $this->toko_online_model->get_table_where('td_keranjang_belanja', array('fc_kdkeranjang_belanja' => $ip , 'fc_kdbarang' => $id_produk));

		//$exp_date = date_sub('Y-m-d H:i:s',  'INTERVAL -24 HOUR');	

		$date=date_create("Y-m-d H:i:s");
		date_sub($date,date_interval_create_from_date_string("1 days"));
		$exp_date = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s'). " + 1 days"));

		if($ambil_data != null){
			$quantity_new = $ambil_data[0]['fn_jumlah_produk'] + $quantity;
			$subtotal_new = $ambil_data[0]['fm_subtotal_belanja'] + $subtotal;

			$data = array(
					'fn_jumlah_produk' => $quantity_new,
					'fm_subtotal_belanja' => $subtotal_new
				);

			$update = $this->toko_online_model->update_table('td_keranjang_belanja',$data, array('fc_kdkeranjang_belanja' => $ip , 'fc_kdbarang' => $id_produk));
		}

		
		else{
			$data = array(
			'fc_kdkeranjang_belanja' 	=> $ip,
			'fc_kdbarang'				=> $id_produk,
			'fc_kdgudang'				=> $kode_gudang,
			'fm_harga_produk'			=> $harga,
			'fn_jumlah_produk'			=> $quantity,
			'fm_subtotal_belanja'		=> $subtotal,
			'fc_status_stok'            => $status_stok,
			'fc_status'					=> $status,
			'f_kode_voucher'			=> $kode_voucher,
			'fd_exp_date'				=> $exp_date
			);
			$insert = $this->toko_online_model->insert_table('td_keranjang_belanja',$data);
		}

		 $query = $this->db->query('update tm_voucher set fc_status="1" where f_kode_voucher="'.$kode_voucher.'"');    


		$ambil_data_qty = $this->toko_online_model->get_table_where('td_stok_barang_gudang', array('fc_kdbarang' => $id_produk , 'fc_kdgudang' => $kode_gudang));

		if($ambil_data_qty != null){
			$quantity_update = $ambil_data_qty[0]['fc_qty_barang'] - $quantity;

				$data_qty = array(
					'fc_qty_barang' => $quantity_update,
				);

			$update_qty = $this->toko_online_model->update_table('td_stok_barang_gudang',$data_qty, array('fc_kdbarang' => $id_produk , 'fc_kdgudang' => $kode_gudang));
		}	

		// $query_update = $this->db->query("update td_stok_barang_gudang set fc_qty_barang = fc_qty_barang-".$quantity." where fc_kdbarang=".$id_produk." and fc_kdgudang=".$fc_kdgudang."");
		// print_r($this->db->last_query());
	}



	public function remove_keranjang_belanja($id){
		$where = array(
			'fc_id' 	=> $id
			);
		$this->toko_online_model->delete_table('td_keranjang_belanja', $where);
		//print_r($this->db->last_query());
		redirect('home/view_cart');
	}

	public function checkout(){
		$data['cart'] = $this->toko_online_model-> get_keranjang_belanja(array('td_keranjang_belanja.fc_kdkeranjang_belanja'=> $_SERVER['REMOTE_ADDR']));
		$penjual = $this->toko_online_model->get_penjual_cart(array('td_keranjang_belanja.fc_kdkeranjang_belanja'=> $_SERVER['REMOTE_ADDR']));
		$angka = 0;
		foreach ($penjual as $p) {
			$data_penjual[$angka] = $this->toko_online_model->get_table_where('tm_user', array('id_user' => $p['fc_user']));
			$angka++;
		}
		
		// $data_penjual = array();
		// $angka = 0;
		// foreach ($data['cart'] as $cart) {
		// 	$data_penjual[$angka] = $this->toko_online_model->get_penjual(array('produk.id_produk' => $cart['id_produk']));
		// 	// print_r($data_penjual[$angka]);
		// 	// echo "<br>";
		// 	$angka++;
		// }
		// foreach ($data_penjual as $penjual) {
		// 	if
		// }
		
		$data['penjual'] = $data_penjual;
		
		$data['content'] = 'user/checkout';
		$this->load->view('user/dashboard',$data);
	}

	public function tentang(){
		$data['content'] = 'user/tentang';
		$data['tentang'] = $this->toko_online_model->tentang()->row();
		$this->load->view('user/dashboard',$data);
	}

	public function foto(){
		$data['content'] = 'user/foto';
		$data['foto'] = $this->toko_online_model->foto()->result_array();
		$this->load->view('user/dashboard',$data);
	}

	// public function kontak(){
	// 	$data['content'] = 'user/kontak';
	// 	$data['kontak'] = $this->toko_online_model->kontak()->row();
	// 	$this->load->view('user/dashboard',$data);
	// }

	public function update_keranjang_belanja(){
		$kode_barang = $this->input->post('kode_barang');
		$kode_gudange = $this->input->post('kode_gudang');
		$id_produk = $this->input->post('fc_kdbarang');
		$kode_gudang = $this->input->post('fc_kdgudang');
		$id_keranjang_belanja = $this->input->post('fc_kdkeranjang_belanja');
		$quantity = $this->input->post('quantity');
		//print_r($id_produk);
		// print_r();
		foreach ($id_produk as $id) {
			$where = array(
					'fc_kdkeranjang_belanja' 	=> $id_keranjang_belanja,
					'fc_kdbarang'				=> $id
				);

			
			$cek_q = $this->toko_online_model->get_table_where('td_keranjang_belanja', $where);
			if($cek_q[0]['fn_jumlah_produk'] != $quantity[$id] ){
				$data = array(
						'fn_jumlah_produk' 	=> $quantity[$id],
						'fm_subtotal_belanja'	=> $quantity[$id]*$cek_q[0]['fm_harga_produk']
					);
				$this->toko_online_model->update_table('td_keranjang_belanja',$data,$where);

				$ambil_data_qty = $this->toko_online_model->get_table_where('td_stok_barang_gudang', array('fc_kdbarang' => $kode_barang , 'fc_kdgudang' => $kode_gudange));
				//hasil = jumlah sekarang - jumlah lama 
				$hasil = $quantity[$id] -  $cek_q[0]['fn_jumlah_produk'];
				$hasil2  = $cek_q[0]['fn_jumlah_produk'] - $quantity[$id];
				if(  $cek_q[0]['fn_jumlah_produk'] < $quantity[$id] ){
				$quantity_update = $ambil_data_qty[0]['fc_qty_barang'] - $hasil;

							$data_qty = array(
								'fc_qty_barang' => $quantity_update,
							);

				$update_qty = $this->toko_online_model->update_table('td_stok_barang_gudang',$data_qty, array('fc_kdbarang' => $kode_barang , 'fc_kdgudang' => $kode_gudange));
				//print_r($this->db->last_query());
				}else{
					$quantity_update2 = $ambil_data_qty[0]['fc_qty_barang'] + $hasil2;

							$data_qty2 = array(
								'fc_qty_barang' => $quantity_update2,
							);

				$update_qtye = $this->toko_online_model->update_table('td_stok_barang_gudang',$data_qty2, array('fc_kdbarang' => $kode_barang , 'fc_kdgudang' => $kode_gudange));
				//print_r($this->db->last_query());
				}
			}

			
		}
		 echo json_encode(array("status" => TRUE));

	}

	public function ajax_list() {
		$where = $this->uri->segment(3);
		$list = $this->mdl_cart->get_datatables($where);
		//print_r($this->db->last_query());
		$data = array();
		$no = $_REQUEST['start'];
		foreach ($list as $produk) {
			if(@$produk->fc_kdbarang){
			if($produk->fc_img_1==''){ $cover = 'no_image.jpg'; }else{ $cover = $produk->fc_img_1; }
			$row2 = '<img src="'.base_url().'assets/images/'.$cover.'">';
			$no++;
			$row = array();
			$row[] = '
			<input type="hidden" name="kode_barang" value="'.$produk->fc_kdbarang.'">
			<input type="hidden" name="kode_gudang" value="'.$produk->fc_kdgudang.'">
			<input type="hidden" name="fc_kdbarang['.$produk->fc_kdbarang.']" value="'.$produk->fc_kdbarang.'">
			<input type="hidden" name="fc_kdgudang['.$produk->fc_kdgudang.']" value="'.$produk->fc_kdgudang.'">
            <input type="checkbox" name="cb_data[]" id="cb_data[]" value="'.$produk->fc_id.'">'; 
			$row[] = '<div class="how-itemcart1">'.$row2.'</div>';
			$row[] = ''.$produk->fv_nama_barang.'';
			$row[] = 'Rp. '.nominal($produk->fm_harga_produk).'';
			$awal = '<select class="js-select2" name="quantity['.$produk->fc_kdbarang.']">';
			$akhir = '</select><div class="dropDownSelect2"></div>';
			$tengah = '';
			for ($i=1; $i <= $produk->fc_qty_barang ; $i++) { 
              if($i == $produk->fn_jumlah_produk){
              
                  $tengah =$tengah.'<option selected value="'.$i.'">'.$i.'</option>';
                
              }
              else{
              
                  $tengah = $tengah.'<option value="'.$i.'">'.$i.'</option>';
                
              }
            }
			$row[] = $awal.$tengah.$akhir;
			$row[] = 'Rp. '.nominal($produk->fm_subtotal_belanja).'';
			$row[] = '<a href="javascript:void(0)" onclick="hapus('.$produk->id.',\''.$produk->fc_kdbarang.'\','.$produk->fc_kdgudang.','.$produk->fn_jumlah_produk.')" type="button"  class="hapus_cart btn btn-danger btn-xs">Batal</a></td>';
			$data[] = $row;
		}
		}

		$output = array(
						"draw" => $_REQUEST['draw'],
						"recordsTotal" => $this->mdl_cart->count_all($where),
						"recordsFiltered" => $this->mdl_cart->count_filtered($where),
						"data" => $data,
				);
		echo json_encode($output);
	}

	public function ajax_delete($id, $id2, $id3, $id4) {
		$ambil_data_qty = $this->toko_online_model->get_table_where('td_stok_barang_gudang', array('fc_kdbarang' => $id2 , 'fc_kdgudang' => $id3));
		//print_r($this->db->last_query());
		$quantity_update = $ambil_data_qty[0]['fc_qty_barang'] + $id4;

					$data_qty = array(
						'fc_qty_barang' => $quantity_update,
					);

		$update_qty = $this->toko_online_model->update_table('td_stok_barang_gudang',$data_qty, array('fc_kdbarang' => $id2 , 'fc_kdgudang' => $id3));
		//print_r($this->db->last_query());	
	    $this->mdl_cart->delete_by_id($id);
	    echo json_encode(array("status" => TRUE));
    }

    function load_subtotal(){ //load data cart
		echo $this->show_subtotal();
	}

	function show_subtotal(){ //Fungsi untuk menampilkan Cart
		$cart = $this->toko_online_model->get_subtotal(array('td_keranjang_belanja.fc_kdkeranjang_belanja'=> $_SERVER['REMOTE_ADDR']));
		$output = '';

		$total = 0;

		foreach ($cart as $items) {
			$total = $total + $items['fm_subtotal_belanja'];
		}	
		$output .= '
				Rp. '.@nominal($total).'
		';

		return $output;
	}	

	function show_stok(){
			$stok_produk = $this->toko_online_model->get_stok();
			$tengah = '';
			for ($i=1; $i <= $stok_produk[0]['fc_qty_barang'] ; $i++) { 
            if($i == 1){
                  
                 $tengah =$tengah.'<option selected value="'.$i.'">'.$i.'</option>';
                
              }
              else{
              
                  $tengah = $tengah.'<option value="'.$i.'">'.$i.'</option>';
                
              }
            }

            $output = '';

            $output .= 
				$tengah
			;

			return $output;
	}

	function load_stok(){ //load data cart
		echo $this->show_stok();
	}

	function kat_detail_produk($id){
		$data['kat_nama']= $this->toko_online_model->tampil_kategori($id)->row();
		$data['produk']= $this->toko_online_model->tampil_kategori($id)->result_array();
		$data['content'] = 'user/produk';
		$this->load->view('user/dashboard',$data);
	}

	function login(){
		$this->load->view('login');
	}

	function cek_stok(){
		$ambil_waktu = $this->toko_online_model->get_ambil_waktu();
		$waktune =  $ambil_waktu[0]['fc_isi'];
		$ambil_barang = $this->toko_online_model->stok_barang();
		$ambil_data_qty = $this->db->get('td_stok_barang_gudang')->result_array();
		$date = date('Y-m-d H:i:s');
		$query = $this->db->query('SELECT * FROM td_stok_barang_gudang');
		foreach ($query->result() as $val) {
			$query2 = $this->db->query(
	                        '
	                        SELECT *
	                        FROM  td_keranjang_belanja
	                        '
	                    );
			foreach ($query2->result() as $value) {
			 	if($date > $value->fd_exp_date){
					$quantity_update = $val->fc_qty_barang + $value->fn_jumlah_produk;
                        $data_qty = array(
                         'fc_qty_barang' => $quantity_update,
                        );
                    $update_qty = $this->toko_online_model->update_table('td_stok_barang_gudang',$data_qty, array('fc_kdbarang' => $value->fc_kdbarang , 'fc_kdgudang' => $value->fc_kdgudang));
                    print_r($this->db->last_query());
            	$delete_keranjang = $this->db->query('delete from td_keranjang_belanja ');  
				}
			} 
		
		}
		//print_r($delete_keranjang);	
	}

	function about(){
		$data['tentang'] = $this->toko_online_model->tentang();
 		$data['content'] = 'user/about';
		$this->load->view('user/dashboard',$data);
	}

	function kontak(){
		$data['kontak'] = $this->toko_online_model->kontak();
 		$data['content'] = 'user/kontak';
		$this->load->view('user/dashboard',$data);
	}

}

/* End of file welcome.php */
