<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produk extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('toko_online_model');
		$this->load->library('session');
	}

	public function index()
	{	
		$data['semua'] = $this->toko_online_model->get_produk('semua');
	 
	 	$data['data_slider']	= $this->toko_online_model->get_produk('terbaru');
		$data['content'] = 'user/produk';
		$this->load->view('user/dashboard' , $data);
		
		
	}
}	