<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once(dirname(__FILE__) . '/mpdf60/mpdf.php');
class Pdf extends MPDF
{ 
	protected function ci()
	{
		return get_instance();
	} 
	public function load_view($view, $data = array())
	{
		$html = $this->ci()->load->view($view, $data, TRUE);
		$this->WriteHtml($html);
	}
}