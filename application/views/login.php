<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?php echo base_url();?>assetslogin/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assetslogin/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assetslogin/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assetslogin/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assetslogin/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assetslogin/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assetslogin/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assetslogin/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assetslogin/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assetslogin/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assetslogin/css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form action="index.html" class="login100-form validate-form">
				<div class="alert alert-danger" role='alert' id="pesan" hidden>
					<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
						<span aria-hidden='true'>×</span><span class='sr-only'>Close</span>
					</button> 
							Username dan password tidak sesuai, silahkan coba kembali
				</div>
					<span class="login100-form-title p-b-26">
						Login
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Valid email is: a@b.c">
						<input class="input100" autocomplete="off" type="text" required="" id="username" name="username">
						<span class="focus-input100" data-placeholder="Username"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<span class="btn-show-pass">
							<i class="zmdi zmdi-eye"></i>
						</span>
						<input class="input100" autocomplete="off" type="password" required="" id="password" name="password">
						<span class="focus-input100" data-placeholder="Password"></span>
					</div>
					<?php
						$info = $this->session->flashdata('info');
	                        if(!empty($info))
	                        {
	                            echo $info;
	                        }
					?><br>
					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button type="button" value="Login" id="login" class="login100-form-btn">
								Login
							</button>
						
						</div>
					</div>

					
				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assetslogin/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assetslogin/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assetslogin/vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url();?>assetslogin/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assetslogin/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assetslogin/vendor/daterangepicker/moment.min.js"></script>
	<script src="<?php echo base_url();?>assetslogin/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assetslogin/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assetslogin/js/main.js"></script>

</body>
</html>
<script type="text/javascript">
        // $(window).load(function(){
        //     setTimeout(function() {
        //         $('#loading').fadeOut( 400, "linear" );
        //     }, 300);
        // });
         function cekform(){
            if(!$("#username").val())
            {
                alert('maaf username tidak boleh kosong');
                $("#username").focus();
                return false;
            }
            if(!$("#password").val())
            {
                alert('maaf password tidak boleh kosong');
                $("#password").focus();
                return false;
            }
        }

        $(function() {
            $('#login').click(function(){
                var pm1=$('#username').val();
                var pm2=$('#password').val();
                $.ajax({
                        type: "POST",
                        url : "<?php echo base_url(); ?>login/getlogin",
                        data : "username="+pm1+"&password="+pm2+"",
                        datatype : 'json',
                        beforeSend: function(msg){$("#login").val('Loading...');},
                        success: function(msg){
                            var data_cek = JSON.parse(''+msg+'' );
                            if (data_cek.hasil=='1') {
                                $("#login").val('Login Sukses');
                                window.location.assign("<?=base_url()?>interen/home");
                            }else{
                                $("#login").val('Login');
                                $("#pesan").attr('hidden',false);
                            }
                        }
                    });
            });
          });
        $("#password").keyup(function (e) {
            if (e.keyCode == 13) {
                $('#login').click();
            }
        });
        $("#username").keyup(function (e) {
            if (e.keyCode == 13) {
                $('#password').focus();
            }
        });

  </script>