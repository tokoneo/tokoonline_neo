<!-- Modal -->
<div class="modal fade bs-example-modal-sm" id="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <center><img src="<?php echo base_url('assets/img/loader.gif'); ?>"></center>
    </div>
  </div>
</div>
 
<div class="modal bs-example-modal-sm" id="loading" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <center>
  <div class="modal-dialog modal-sm" role="document" style="margin-top: 17%;     ">
    <div class="modal-content" style="width: 42%;" >
       <img src="<?php echo base_url('assets/img/loader.gif'); ?>">
       <p>Loading</p>
    </div>
  </div>
  </center>
</div>
<section class="bg-img1 txt-center p-lr-15 p-tb-92" style="background-image: url('<?php echo base_url() ?>assets/images/bg-01.jpg');">
        <h2 class="ltext-105 cl0 txt-center">
           Lihat Sekilas
        </h2>
    </section>  
 <section class="bg0 p-t-75 p-b-120">
 <div class="bread-crumb flex-w p-l-25 p-r-15 p-t-30 p-lr-0-lg">
                <a href="index.html" class="stext-109 cl8 hov-cl1 trans-04">
                    Beranda
                    <i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
                </a>


                <span class="stext-109 cl4">
                    Check Out
                </span>
            </div>    
<form name="form_checkout" id="form_checkout" action="<?php echo base_url() ?>interen/order/simpan" method="POST">            
<div class="bg0 p-t-75 p-b-85">
      <div class="row">
<div class="col-lg-10 col-xl-7 m-lr-auto m-b-50">
          <div class="bor10 p-lr-40 p-t-30 p-b-40 m-l-63 m-r-40 m-lr-0-xl p-lr-15-sm">
            <h4 class="mtext-109 cl2 p-b-30">
            Detail Penagihan
            </h4>

             <div class="form-group">
                 <label for="email">Nama Lengkap:</label>
                  <input class="form-control" type="text" name="fullname" placeholder="Full Name*" required>
             </div>  
              <div class="form-group">
                 <div >
                               <label for="email">Email:</label>
                                <input class="form-control" type="email" name="email" placeholder="Email Address*" required>
                              </div>                             
                            </div>
                           <div class="form-group">
                              <div >
                              <label for="email">No. Telp:</label>
                                <input class="form-control" type="text" name="phone" placeholder="Phone*" required>
                              </div>
                            </div>
                            <div class="form-group">
                            <div >
                              <label for="email">Alamat:</label>
                                <textarea class="form-control" cols="8" rows="3" name="address" required></textarea>
                            </div>    
                           </div>  
                           <div class="form-group">
                            <label for="email">Provinsi:</label>
                           <select class="form-control" name="desprovince" id="desprovince" width="90" required >
                                  <option> -- Pilih Provinsi -- </option>
                            </select>      
                           </div>
                           <div class="form-group">
                            <label for="email">Kota:</label>
                           <select class="form-control" name="descity" id="descity" required >
                                    <option value="">-- Pilih Kota --</option>
                            </select> 
                           </div>
                           <div class="form-group">
                            <label for="email">Kode Pos:</label>
                            <input class="form-control" type="text" name="kodepos" placeholder="Kode Pos*" required>
                           </div>

          <hr />                 

        
            <?php 
            foreach ($penjual as $p) {
                      ?>
                      <?php 
                                $user = $this->session->userdata('id_user');
                                  $angka = 0;
                                  $data_keranjang = $this->toko_online_model->get_data_keranjang(array('td_keranjang_belanja.fc_kdkeranjang_belanja' => $user , 'td_barang.fc_user' => $p[0]['id_user']));
                                  foreach ($data_keranjang as $keranjang) {
                                    $data_produk = $this->toko_online_model->get_table_rows_where('fv_berat','td_barang', array('fc_kdbarang' => $keranjang['fc_kdbarang']));
                                    // echo  $data_produk[0]['berat']."*".$keranjang['jumlah_produk'];
                                    $berat_produk[$angka] = $keranjang['fn_jumlah_produk'] * $data_produk[0]['fv_berat'];
                                    $angka++;
                                    $status_order = $keranjang['fc_status_stok'];
                                  }
                                  $berat_total = 0;
                                  foreach ($berat_produk as $berat) {
                                    $berat_total = $berat_total + $berat;
                                  }

         ?>
         <input type="hidden" name="status_order" value="<?php echo $status_order;?>">

         <div class="panel panel-default aa-checkout-billaddress">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $p[0]['id_user']?>">
                          <h4 class="mtext-109 cl2 p-b-30">
                              Pilih Jasa Pengiriman 
                          </h4>
                            
                          </a>
                        </h4>
                      </div>
                        <div class="panel-body">
                         
                          <div class="row">
                            <div class="col-md-12">
                              <div class="aa-checkout-single-bill">
                                <select id="service<?php echo $p[0]['id_user']?>" onchange="cekHarga('<?php echo $p[0]['id_user'] ?>')" class="form-control">
                                  <option value="" disabled="Silahkan Pilih" selected="" >- Silahkan Pilih Kurir -</option>
                                  <option value="jne">JNE</option>
                                  <option value="pos">POS</option>
                                  <option value="tiki">TIKI</option>
                                  <option value="lainnya">Lain nya</option>
                                </select>
                             
                                <input type="hidden" name="ori_city" id="oricity<?php echo $p[0]['id_user'] ?>" value="<?php echo $p[0]['id_ongkir']?>">
                                <input type="hidden" name="pelapak" id="pelapak" value="<?php echo $p[0]['id_user']?>">
                                <input type="hidden" name="berat" id="berat<?php echo $p[0]['id_user'] ?>" value="<?php echo $berat_total ?>">
<!--                                 <button type = "button" onclick="cekHarga('<?php echo $p[0]['id_user'] ?>')"  class="btn btn-default">Cek Ongkir</button>
 -->                              </div>                             
                            </div>                            
                          </div>
              
                          <div class="row">
                            <div class="col-md-12">
                              <table  class="table table-condensed">
                                  <tbody id="resultsbox<?php echo $p[0]['id_user']?>"></tbody>
                              </table>
                            </div>
                          </div>
                                   
                        </div>
                    </div>
                      <?php
                    }
                   ?> 

         
          </div>

        </div>





        <div class="col-sm-10 col-lg-7 col-xl-5 m-lr-auto m-b-50">
        <div class="bor10 p-lr-40 p-t-30 p-b-40 m-l-63 m-r-40 m-lr-0-xl p-lr-15-sm">
        <h4 class="mtext-109 cl2 p-b-30">
            Ringkasan Pesanan
        </h4>
        <table class="table table-responsive">
                      <thead>
                        <tr>
                          <th>Product</th>
                          <th>Total</th>
                        </tr>
                      </thead>
                      <tbody>
                     
                       <?php 

                      $subtotal = 0;
                      $berat = 0;
                      foreach ($cart as $c) {
                        $subtotal = $subtotal + $c['fm_subtotal_belanja'];
                        $berat = $berat + $c['fv_berat'];

                        ?>
                        <tr>
                          <td><?php echo $c['fv_nama_barang'] ?><strong> x  <?php echo $c['fn_jumlah_produk'] ?></strong></td>
                          <td>Rp. <?php echo $c['fm_subtotal_belanja'] ?></td>
                        </tr>
                        <?php
                      } ?>  

                      </tbody>
                      
                      <tfoot>

                        <tr>
                          <th>Total Belanja</th>
                          <td>Rp. <?php echo $subtotal ?></td>
                        </tr>
                        <? <?php foreach ($penjual as $p): ?>
                          <tr>
                          <th>Ongkir</th>

                           <td > <input class="form-control" type="text" name="ongkos_penjual2" id="ongkos_penjual<?php echo $p[0]['id_user'] ?>" required="" readonly="" value=""  placeholder="Silakan Cek Ongkir"></td>
                        </tr>
                        <?php endforeach ?>


                          <?php foreach ($penjual as $p): ?>
                          <tr>
                          <th>Jasa Kirim </th>
                            
                          <td > <input type="text" name="jenis_layanan_ongkir" id="jenis_layanan_ongkir<?php echo $p[0]['id_user'] ?>" required="" readonly="" value="" placeholder="Silahkan Pilih Kurir "></td>
                        </tr>
                        <?php endforeach ?>
                      </tfoot>
                    </table>

              <div class="panel panel-warning">
              <div class="panel-heading">Metode pembayaran</div>
              <div class="panel-body">
              <table class="table table-responsive">
              </tbody>
                
                     
                      <input type="hidden" name="provinsi" id='provinsi'>
                      <input type="hidden" name="kota" id='kota'>
                      <input type="hidden" name="berat" id="berat" value="">
                      <input type="hidden" name="total" id='total' value="<?php echo $subtotal;?>" >
                     
                 <tr>
                  
                          <td> <label class="radio-inline"><input type="radio" id="paypal" name="optionsRadios" checked> Via Transfer </label></td>
                        </tr>     
              </tbody>        
              </table>        
                     <!--  <img src="https://www.paypalobjects.com/webstatic/mktg/logo/AM_mc_vs_dc_ae.jpg" border="0" alt="PayPal Acceptance Mark">     -->
                    <input type="submit" value="Place Order" onclick="return confirm_check();"  class="flex-c-m stext-101 cl0 size-116 bg3 bor14 hov-btn3 p-lr-15 trans-04 pointer">
                     
              </div>
              </div>    
              </div>      
        </div>
</div>
</div> 

</form>
</section>       

<script type = "text/javascript">
function confirm_check()
{   
    var data=true;
    <?php foreach ($penjual as $p){ ?>

      console.log($("#jenis_layanan_ongkir<?php echo $p[0]['id_user'] ?>").val());
      if ($("#jenis_layanan_ongkir<?php echo $p[0]['id_user'] ?>").val()=="") {
        data=false;
      }

  <?php  } ?>

  if (data) {
    
     return true;
  }else{
    alert("Harap Lengkapi data yang di sediakan");
     return false;
  }
   
  
}
$(document).ready(function(){
  loadProvinsi('#desprovince');
  $('#desprovince').change(function(){
    $('#descity').show();
    var idprovince = $('#desprovince').val();
    loadCity(idprovince,'#descity');
    $('#provinsi').val(getSelectedText('desprovince'));
  });
});

$('#descity').change(function(){
    $('#kota').val(getSelectedText('descity'));
  });
  
function getSelectedText(elementId) {
    var elt = document.getElementById(elementId);

    if (elt.selectedIndex == -1)
        return null;
  var  selected = elt.options[elt.selectedIndex].text;
    return selected;
}


function loadProvinsi(id){

  $.ajax({
    url: '<?php echo base_url("interen/ongkir/showprovince");?>',
    dataType:'json',
    success:function(response){
      $(id).html('');
      province = '';
        province = '<option> -- Pilih Provinsi-- </option>';
        province = province + '';
        $(id).append(province);
        
        $.each(response['rajaongkir']['results'], function(i,n){
          province = '<option value="'+n['province_id']+'">'+n['province']+'</option>';
          province = province + '';
          $(id).append(province);
        });
    },
    error:function(){
      alert('ERROR ! Check your internet connection');
      //$(id).html('ERROR');
    }
  });
}

function loadCity(idprovince,id){
  $.ajax({
    url: '<?php echo base_url("interen/ongkir/showcity/");?>'+'/'+idprovince,
    dataType:'json',
    data:{province:idprovince},
    success:function(response){
      $(id).html('');
      city = '';

        city = '<option >-- Pilih Kota --</option>';
        city = city + '';
        $(id).append(city);
        $.each(response['rajaongkir']['results'], function(i,n){
          city = '<option value="'+n['city_id']+'">'+n['city_name']+'</option>';
          city = city + '';
          $(id).append(city);
        });
    },
    error:function(){
      $(id).html('ERROR');
    }
  });
}

function cekHarga(trigger){
  var origin = $('#oricity'+trigger).val();
  var destination = $('#descity').val();
  var weight = $('#berat'+trigger).val();
  var courier = $('#service'+trigger).val();
  var pelapak = $('#pelapak').val();

 

   var jenis_layanan=$("#jenis_layanan_ongkir"+trigger);

   jenis_layanan.val("");

   $("#loading").modal('show');

  console.log('<?php echo base_url("interen/ongkir/cost");?>'+'?origin='+origin+'?destination='+destination+'?weight='+weight+'?courier='+courier+'?trigger='+trigger);
  //var jenis_layanan=$("#jenis_layanan_ongkir"+trigger);
  $.ajax({
    url: '<?php echo base_url("interen/ongkir/cost");?>'+'?origin='+origin+'?destination='+destination+'?weight='+weight+'?courier='+courier+'?trigger='+trigger,
    data:{origin:origin,destination:destination,weight:weight,courier:courier,trigger:trigger},
    success:function(response){
      $('#resultsbox'+trigger).html(response);
      $("#loading").modal('hide');
      
      
    },
    error:function(){
      //$('#resultsbox'+trigger).html('ERROR');
      $("#loading").modal('hide');
    }
  });
}


function pilihOngkir2(id_penjual,kurir){
var jenis_layanan=$("#jenis_layanan_ongkir"+id_penjual);
var jenis_layanan=$("#jenis_layanan_ongkir"+id_penjual);

  var radios = document.getElementsByName('tarif');
  var harga_lain = document.getElementsByName('biaya');
  console.log(harga_lain);
  var tarif, totaltarif;
  var total = <?php echo $subtotal?>;
  var totalberat = <?php echo $berat;?>;
  for (var i = 0, length = radios.length; i < length; i++) {
    if (radios[i].checked) {
      tarif = radios[i].value;
      totaltarif =  parseInt(tarif);
      total = parseInt(total)+parseInt(totaltarif);
      $('#ongkoskirim'+id_penjual).html('');
      // $('#totalakhir').html('');
      $('#ongkoskirim'+id_penjual).html("Rp. "+totaltarif);
      // $('#ongkos_penjual'+id_penjual).html("Rp. "+totaltarif);
      // $('#ongkos_penjual'+id_penjual).val(totaltarif);
      // $('#ongkos_penjual2'+id_penjual).val(totaltarif);
     $('#totalsimpan').val(total);
//      $('#totalsimpan').val(total);
//      $('#totalsimpan').val(total);
      $('#ongkos').val(totaltarif);
      $("#ongkos_penjual<?php echo $p[0]['id_user'] ?>").attr("readonly","");
      jenis_layanan.val("");
      jenis_layanan.val(kurir);
      // $('#totalakhir').html("Rp. "+total);
      // for (var prop in obj) {
      //      $('#totalakhir').html("Rp. "+total);
      // }
    }
  }
}  

function pilihOngkir(id_penjual,kurir){
  var jenis_layanan=$("#jenis_layanan_ongkir"+id_penjual);
  var radios = document.getElementsByName('tarif');
  var harga_lain = document.getElementsByName('biaya');
   console.log(kurir);
  var tarif, totaltarif;
  var total = <?php echo $subtotal?>;
  var totalberat = <?php echo $berat;?>;
  for (var i = 0, length = radios.length; i < length; i++) {
    if (radios[i].checked) {
      tarif = radios[i].value;
      totaltarif =  parseInt(tarif);
      total = parseInt(total)+parseInt(totaltarif);
      $('#ongkoskirim'+id_penjual).html('');
      // $('#totalakhir').html('');
      $('#ongkoskirim'+id_penjual).html("Rp. "+totaltarif);
      $('#ongkos_penjual'+id_penjual).html("Rp. "+totaltarif);
      $('#ongkos_penjual'+id_penjual).val(totaltarif);
      $('#ongkos_penjual2'+id_penjual).val(totaltarif);
     $('#totalsimpan').val(total);
//      $('#totalsimpan').val(total);
//      $('#totalsimpan').val(total);
      $('#ongkos').val(totaltarif);

      jenis_layanan.val("");
      jenis_layanan.val(kurir);
      $("#ongkos_penjual<?php echo $p[0]['id_user'] ?>").attr('readonly', 'true');
      // $('#totalakhir').html("Rp. "+total);
      // for (var prop in obj) {
      //      $('#totalakhir').html("Rp. "+total);
      // }
    }
  }
}


function format1(n, currency) {
  return currency + " " + n.toFixed(2).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
    });
}




</script>