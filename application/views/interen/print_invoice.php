  <link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/images/icons/favicon.png"/>
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/fonts/linearicons-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/vendor/animate/animate.css">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/vendor/select2/select2.min.css">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/vendor/slick/slick.css">
<!--===============================================================================================-->
   <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/vendor/MagnificPopup/magnific-popup.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/vendor/perfect-scrollbar/perfect-scrollbar.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/util.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/main.css">
<div class="container" id="content">
      <div class="row">
        <div class="col-lg-12 ">
          
          <div class="m-l-25 m-r--38 m-lr-0-xl">
            <div class="wrap-table-shopping-cart">
              <table border="1" style="border-collapse: collapse;" class="table-shopping-cart">
                <tbody><tr class="table_head">
                  <th class="column-1">Product</th>
                  <th class="column-2"></th>
                  <th class="column-3">Price</th>
                  <th class="column-4">Quantity</th>
                  <th class="column-5">Total</th>
                </tr>
                <?php 
                $total_perpenjual = 0;
                foreach ($detail_order as $produk): ?>
                <tr class="table_row">
                  <td class="column-1">
                    <div class="how-itemcart1">
                      <img src="<?php echo base_url();?>assets/images/<?php echo $produk['fc_img_1'] ?>" alt="IMG" style="width:50px;height: 60px;">
                    </div>
                  </td>
                  <td class="column-2"><?php echo $produk['fv_nama_barang'] ?></td>
                  <td class="column-3">Rp. <?php echo nominal($produk['fm_harga'])?></td>
                  <td class="column-4">
                    <?php echo $produk['f_jumlah_produk'] ?>
                  </td>
                  <td class="column-5">Rp. <?php echo nominal($produk['fm_subtotal'] )?></td>
                </tr>
                <?php 
                $total_perpenjual = $total_perpenjual + $produk['fm_subtotal'];
                endforeach ?>
               
              </tbody></table>
            </div>

            <div class="flex-w flex-sb-m bor15 p-t-18 p-b-15 p-lr-40 p-lr-15-sm">
              <div class="flex-w flex-m m-r-20 m-tb-5">
                Total
              </div>

          
            Rp.  <?php echo nominal($order[0]['fm_total'])?>
             
            </div>
            <div class="flex-w flex-sb-m bor15 p-t-18 p-b-15 p-lr-40 p-lr-15-sm">
              <div class="flex-w flex-m m-r-20 m-tb-5">
                Ongkir
              </div>

              
             Rp. <?php echo nominal($order[0]['fm_ongkir_order'])?>
           
            </div>
             <div class="flex-w flex-sb-m bor15 p-t-18 p-b-15 p-lr-40 p-lr-15-sm">
              <div class="flex-w flex-m m-r-20 m-tb-5">
                Grand Total
              </div>

          
             Rp. <?php echo nominal($order[0]['fm_grandtotal_order'])?>
            </div>
             

          </div>
        </div>

      
      </div>
    </div>