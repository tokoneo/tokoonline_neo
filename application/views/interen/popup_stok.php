<!DOCTYPE html>
<html>
<head>
    <title>Cara Menggunakan Datatables | Malas Ngoding</title>  
    
     <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/vendor/bootstrap/css/bootstrap.min.css">
    <!-- <link rel="stylesheet" type="text/css" href="assets/DataTables/media/css/jquery.dataTables.css"> -->
    <link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    
</head>
<body>
    <center>
        <h1>Menampilkan data dengan datatables | Malas Ngoding</h1>
    </center>
    <br/>
    <br/>
    <div class="container">
        <table class="table table-striped table-bordered data">
            <thead>
                <tr>            
                    <th>Nama</th>
                    <th>Alamat</th>
                    <th>Pekerjaan</th>
                    <th>Usia</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                <tr>                
                    <td>Andi</td>
                    <td>Jakarta</td>
                    <td>Web Designer</td>
                    <td>21</td>
                    <td>Aktif</td>
                </tr>
                <tr>                
                    <td>Malas Ngoding</td>
                    <td>Bandung</td>
                    <td>Web Developer</td>
                    <td>26</td>
                    <td>Aktif</td>
                </tr>
                <tr>                
                    <td>Malas Ngoding</td>
                    <td>Bandung</td>
                    <td>Web Developer</td>
                    <td>26</td>
                    <td>Aktif</td>
                </tr>
                <tr>                
                    <td>Andi</td>
                    <td>Jakarta</td>
                    <td>Web Designer</td>
                    <td>21</td>
                    <td>Aktif</td>
                </tr>
                <tr>                
                    <td>Andi</td>
                    <td>Jakarta</td>
                    <td>Web Designer</td>
                    <td>21</td>
                    <td>Aktif</td>
                </tr>
                <tr>                
                    <td>Andi</td>
                    <td>Jakarta</td>
                    <td>Web Designer</td>
                    <td>21</td>
                    <td>Aktif</td>
                </tr>
                <tr>                
                    <td>Andi</td>
                    <td>Jakarta</td>
                    <td>Web Designer</td>
                    <td>21</td>
                    <td>Aktif</td>
                </tr>
                <tr>                
                    <td>Andi</td>
                    <td>Jakarta</td>
                    <td>Web Designer</td>
                    <td>21</td>
                    <td>Aktif</td>
                </tr>
                <tr>                
                    <td>Andi</td>
                    <td>Jakarta</td>
                    <td>Web Designer</td>
                    <td>21</td>
                    <td>Aktif</td>
                </tr>
                <tr>                
                    <td>Andi</td>
                    <td>Jakarta</td>
                    <td>Web Designer</td>
                    <td>21</td>
                    <td>Aktif</td>
                </tr>
                <tr>                
                    <td>Andi</td>
                    <td>Jakarta</td>
                    <td>Web Designer</td>
                    <td>21</td>
                    <td>Aktif</td>
                </tr>
                <tr>                
                    <td>Andi</td>
                    <td>Jakarta</td>
                    <td>Web Designer</td>
                    <td>21</td>
                    <td>Aktif</td>
                </tr>
                <tr>                
                    <td>Andi</td>
                    <td>Jakarta</td>
                    <td>Web Designer</td>
                    <td>21</td>
                    <td>Aktif</td>
                </tr>
                <tr>                
                    <td>Andi</td>
                    <td>Jakarta</td>
                    <td>Web Designer</td>
                    <td>21</td>
                    <td>Aktif</td>
                </tr>
                <tr>                
                    <td>Andi</td>
                    <td>Jakarta</td>
                    <td>Web Designer</td>
                    <td>21</td>
                    <td>Aktif</td>
                </tr>
                <tr>                
                    <td>Andi</td>
                    <td>Jakarta</td>
                    <td>Web Designer</td>
                    <td>21</td>
                    <td>Aktif</td>
                </tr>
                <tr>                
                    <td>Andi</td>
                    <td>Jakarta</td>
                    <td>Web Designer</td>
                    <td>21</td>
                    <td>Aktif</td>
                </tr>
                <tr>                
                    <td>Andi</td>
                    <td>Jakarta</td>
                    <td>Web Designer</td>
                    <td>21</td>
                    <td>Aktif</td>
                </tr>
                <tr>                
                    <td>Andi</td>
                    <td>Jakarta</td>
                    <td>Web Designer</td>
                    <td>21</td>
                    <td>Aktif</td>
                </tr>
                <tr>                
                    <td>Andi</td>
                    <td>Jakarta</td>
                    <td>Web Designer</td>
                    <td>21</td>
                    <td>Aktif</td>
                </tr>
            </tbody>
        </table>
    </div>
</body>

 <script src="<?php echo base_url();?>assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url();?>assets/js/dataTables/jquery.dataTables.bootstrap.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.data').DataTable();
    });
</script>
</html>