<section class="bg-img1 txt-center p-lr-15 p-tb-92" style="background-image: url('<?php echo base_url() ?>assets/images/bg-01.jpg');">
        <h2 class="ltext-105 cl0 txt-center">
           Form Konfirmasi Pembayaran
        </h2>
</section> 
<section class="bg0 p-t-75 p-b-120">
        <div class="container">
         <div class="bread-crumb flex-w p-l-25 p-r-15 p-t-30 p-lr-0-lg">
                        <a href="index.html" class="stext-109 cl8 hov-cl1 trans-04">
                            Beranda
                            <i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
                        </a>


                        <span class="stext-109 cl4">
                           Form Konfirmasi Pembayaran
                        </span>
        </div> <br /><br />
       </div>
       <div class="col-lg-12">
        <div class="bor10 p-lr-40 p-t-30 p-b-40 m-l-63 m-r-40 m-lr-0-xl p-lr-15-sm">
       <table class="table table-condensed total-result">
                  <tbody><tr>
                    <td>ID Order</td>
                    <td> <?php echo $data_order[0]['fc_kdorder']; ?></td>
                  </tr>
                  <tr>
                    <td>Ditagihkan Kepada</td>
                    <td><?php echo $data_order[0]['fv_nama_order']; ?></td>
                  </tr>
                  <tr class="shipping-cost">
                    <td>Kota</td>
                    <td><?php echo $data_order[0]['fv_kota_order']; ?> </td>                   
                  </tr>
                  <tr class="shipping-cost">
                    <td>Provinsi</td>
                    <td><?php echo $data_order[0]['fv_provinsi_order']; ?> </td>                   
                  </tr>
                  <tr>
                    <td>Alamat</td>
                    <td><span><?php echo $data_order[0]['fv_alamat_order']; ?></span></td>
                  </tr>
                  <tr class="shipping-cost">
                    <td>No Telp</td>
                    <td><?php echo $data_order[0]['fc_telp']; ?></td>                    
                  </tr>
                  <tr class="shipping-cost">
                    <td>Email</td>
                    <td><?php echo $data_order[0]['fv_email_order']; ?></td>                    
                  </tr>
                    <tr class="shipping-cost">
                    <td>Kode Pos</td>
                    <td><?php echo $data_order[0]['fc_kode_pos_order']; ?></td>                    
                  </tr>
                   <tr class="shipping-cost">
                    <td>Status Order</td>
                    <td><?php echo $status_order; ?></td>                    
                  </tr>
                </tbody>
        </table> 
        </div>

        <form class="bg0 p-t-75 p-b-85">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 ">

          <div class="m-l-25 m-r--38 m-lr-0-xl">
            <div class="wrap-table-shopping-cart">
              <table class="table-shopping-cart">
                <tbody><tr class="table_head">
                  <th class="column-1">Product</th>
                  <th class="column-2"></th>
                  <th class="column-3">Price</th>
                  <th class="column-4">Quantity</th>
                  <th class="column-5">Total</th>
                </tr>
                <?php 
                $total_perpenjual = 0;
                foreach ($penjual as $produk): ?>
                <tr class="table_row">
                  <td class="column-1">
                    <div class="how-itemcart1">
                      <img src="<?php echo base_url();?>assets/images/<?php echo $produk['fc_img_1'] ?>" alt="IMG">
                    </div>
                  </td>
                  <td class="column-2"><?php echo $produk['fv_nama_barang'] ?></td>
                  <td class="column-3">Rp. <?php echo nominal($produk['fm_harga'])?></td>
                  <td class="column-4">
                    <?php echo $produk['f_jumlah_produk'] ?>
                  </td>
                  <td class="column-5">Rp. <?php echo nominal($produk['fm_subtotal'] )?></td>
                </tr>
                <?php 
                $total_perpenjual = $total_perpenjual + $produk['fm_subtotal'];
                endforeach ?>
               
              </tbody></table>
            </div>

            <div class="flex-w flex-sb-m bor15 p-t-18 p-b-15 p-lr-40 p-lr-15-sm">
              <div class="flex-w flex-m m-r-20 m-tb-5">
                Total
              </div>

          
            Rp.  <?php echo nominal($data_order[0]['fm_total'])?>
             
            </div>
            <div class="flex-w flex-sb-m bor15 p-t-18 p-b-15 p-lr-40 p-lr-15-sm">
              <div class="flex-w flex-m m-r-20 m-tb-5">
                Ongkir
              </div>

              
             Rp. <?php echo nominal($data_order[0]['fm_ongkir_order'])?>
           
            </div>
             <div class="flex-w flex-sb-m bor15 p-t-18 p-b-15 p-lr-40 p-lr-15-sm">
              <div class="flex-w flex-m m-r-20 m-tb-5">
                Grand Total
              </div>

          
             Rp. <?php echo nominal($data_order[0]['fm_grandtotal_order'])?>
            </div>
             

          </div>
        </div>

      
      </div>
    </div>
  </form>

        </div>
 <?php if($data_order[0]['fc_status_kirim'] == 1){ ?>        
<form class="bg0 p-t-75 p-b-85" action="<?php echo base_url() ?>order/form_konfirmasi_pembayaran" method="POST" enctype="multipart/form-data">
 <input type="hidden" name="id_order" value="<?php echo $data_order[0]['fc_kdorder'] ?>">
      <div class="row">
<div class="col-lg-12">
          <div class="bor10 p-lr-40 p-t-30 p-b-40 m-l-63 m-r-40 m-lr-0-xl p-lr-15-sm">
            <h4 class="mtext-109 cl2 p-b-30">
            Detail Penagihan
            </h4>

             <div class="form-group">
                 <label for="email">Tanggal Transfer*:</label>
                  <input class="form-control" type="date" name="tanggal_transfer" placeholder="Tanggal Transfer*" required>
             </div>  
              <div class="form-group">
                 <div >
                               <label for="email">Nama Pemilik Rekening Transfer*:</label>
                               <input class="form-control" type="text" name="nama_pemilik" placeholder="Nama Pemilik Rekening Transfer*" required>
                              </div>                             
                            </div>
                           <div class="form-group">
                              <div >
                              <label for="email">Nomor Rekening Transfer*:</label>
                                   <input class="form-control" type="text" name="no_rekening" placeholder="Nomor Rekening Transfer*" required>
                              </div>
                            </div>
                           
                           <div class="form-group">
                            <label for="email">Bank Transfer*:</label>
                               <input class="form-control" type="text" name="bank" placeholder="Bank Transfer*" required>
                           </div>
                           <div class="form-group">
                              <div >
                              <label for="email">Total Transfer*:</label>
                                <input class="form-control" type="text" name="total_transfer" placeholder="Total Transfer" required>
                              </div>
                            </div>
                           
                           <div class="form-group">
                            <label for="email">Foto Bukti Transfer*:</label>
                            <input class="form-control" type="file" name="userfile" placeholder="Total Transfer" required>
                           </div>
                            <div class="form-group">
                          <input  type="submit" value="Konfirmasi" class="flex-c-m stext-101 cl0 size-125 bg3 bor2 hov-btn3 p-lr-15 trans-04">
                            </div>
          </div>
        </div>  
</div>
</form>        
<?php } ?>