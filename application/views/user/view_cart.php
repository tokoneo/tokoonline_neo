<section class="bg-img1 txt-center p-lr-15 p-tb-92" style="background-image: url('<?php echo base_url() ?>assets/images/bg-01.jpg');">
        <h2 class="ltext-105 cl0 txt-center">
           Lihat Keranjang Belanja
        </h2>
</section>  

<div class="bg0 p-t-75 p-b-85">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 col-xl-7 m-lr-auto m-b-50">
					<div class="m-l-25 m-r--38 m-lr-0-xl">
						<div class="wrap-table-shopping-cart">
							<form method="post" id="formAksi2">
		             		 <?php if($cart != null){
		                        ?>
		                          <input type="hidden" name="fc_kdkeranjang_belanja" value="<?php echo $cart[0]['fc_kdkeranjang_belanja'] ?>">
		                        <?php
		                      } ?>
		                      
							<table id="dynamic-table" class="table-shopping-cart">
								<thead><tr class="table_head">
									<th class="column-2">Pilih</th>
									<th class="column-1">Produk</th>
									<th class="column-2"></th>
									<th class="column-3">Harga</th>
									<th class="column-4">Jumlah</th>
									<th class="column-5">Total</th>
									<th class="column-6">Aksi</th>
								</tr></thead>
								
								<tbody></tbody></table>
							<div class="flex-w flex-sb-m bor15 p-t-18 p-b-15 p-lr-40 p-lr-15-sm">
								

								
								
				              <div class="flex-w flex-m m-r-20 m-tb-5">
				                
				              </div>

				              <div class="flex-c-m stext-101 cl2 size-119 bg8 bor13 hov-btn3 p-lr-15 trans-04 pointer m-tb-10">
				            <button  type="submit"  >Perbarui Keranjang</button>
				              </div>
							</div>
							</form>	
						</div>
						
					</div>
				</div>

				<div class="col-sm-10 col-lg-7 col-xl-5 m-lr-auto m-b-50">
					<div class="bor10 p-lr-40 p-t-30 p-b-40 m-l-63 m-r-40 m-lr-0-xl p-lr-15-sm">
						<h4 class="mtext-109 cl2 p-b-30">
							Keranjang Total
						</h4>

						<div class="flex-w flex-t bor12 p-b-13">
							<div class="size-208">
								<span class="stext-110 cl2">
									Subtotal:
								</span>
							</div>

							<div class="size-209">
								<span class="mtext-110 cl2">
									<div id="subtotal"></div>
								</span>
							</div>
						</div>

					
						<br />
						<a href="<?php echo base_url('home/checkout')?>" class="flex-c-m stext-101 cl0 size-116 bg3 bor14 hov-btn3 p-lr-15 trans-04 pointer">
							Lanjutkan ke pembayaran
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php $id_produk = $this->uri->segment(3);?>
<script type="text/javascript">
	var zonk='';
	var save_method;
	var table;
	var link = "<?php echo site_url('home')?>";
	var kdProduk = "<?php echo @$id_produk;?>";
	$(document).ready(function(){    
        $('#subtotal').load("<?php echo base_url();?>home/load_subtotal");
    });

	$(document).ready(function() {
		 $.noConflict();
		 $('#dynamic-table tbody td').wrapInner( '<class>' ).append( 'column-1' );
		table = $('#dynamic-table').DataTable({ 
		"processing": true, //Feature control the processing indicator.
        "serverSide": true, 
        "paging": false,
        "searching": false,
        "info": false,
        "ordering": false,
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": link+"/ajax_list/"+kdProduk,
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],

    });
	
	});

	function reload_table() {
    	table.ajax.reload(null, false);
	}

	function hapus(id, id2, id3, id4) {
		if (confirm('Are you sure delete this data?')) {
			$.ajax ({
				url : "<?php echo site_url('home/ajax_delete')?>/"+id+'/'+id2+'/'+id3+'/'+id4,
				type: "POST",
				dataType: "JSON",
				success: function(data) {
					setTimeout(function(){
                        reload_table();
					}, 1000);
					setTimeout(function(){
						swal_berhasil_delet(); 
					}, 1000);
					setTimeout(function(){	
						detail_cart();
					}, 1000);
					setTimeout(function(){	
						cek();
					}, 1000);

					setTimeout(function(){
	                	detail_subtotal();
	                }, 1000);
				}, error: function (jqXHR, textStatus, errorThrown) {
					swal({ title:"ERROR", text:"Error delete data", type: "warning", closeOnConfirm: true}); 
					$('#btnSave').text('save'); $('#btnSave').attr('disabled',false); 
				}
			});
		}
	}

	 $(document).on('submit', '#formAksi2', function(e) {  
      e.preventDefault();
      if (confirm('Apakah Anda Yakin Akan Merubah Keranjang Belanja?')) {
            $.ajax({
                url : "<?php echo site_url('home/update_keranjang_belanja')?>/",
                type: "POST",
                data:  new FormData(this),
                contentType: false,
                cache: false,
                processData:false,
                success: function(data){  
                setTimeout(function(){
                    reload_table();
                }, 1000);

                swal_berhasil_update();
                setTimeout(function(){
                	detail_cart();
                }, 1000);	
                setTimeout(function(){
                	detail_subtotal();
                }, 1000);	
                }           
            });
        }
        return false;
  	});

  	  

  	function detail_subtotal(){
        $('#subtotal').load("<?php echo base_url();?>home/load_subtotal");
    }
</script>	