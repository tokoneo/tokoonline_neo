
<section class="bg-img1 txt-center p-lr-15 p-tb-92" style="background-image: url('<?php echo base_url() ?>assets/images/bg-01.jpg');">
        <h2 class="ltext-105 cl0 txt-center">
           Lihat Sekilas
        </h2>
    </section>  


    <!-- Content page -->
    <section class="bg0 p-t-75 p-b-120">
        <div class="container">
            <div class="bread-crumb flex-w p-l-25 p-r-15 p-t-30 p-lr-0-lg">
                <a href="index.html" class="stext-109 cl8 hov-cl1 trans-04">
                    Beranda
                    <i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
                </a>


                <span class="stext-109 cl4">
                    Produk Detail
                </span>
            </div>
            <div class="bg0 p-t-60 p-b-30 p-lr-15-lg how-pos3-parent">
            <!--     <button class="how-pos3 hov3 trans-04 js-hide-modal1">
                    <img src="<?php echo base_url() ?>assets/images/icons/icon-close.png" alt="CLOSE">
                </button> -->

                <div class="row">
                    <div class="col-md-6 col-lg-7 p-b-30">
                        <div class="p-l-25 p-r-30 p-lr-0-lg">
                            <div class="wrap-slick3 flex-sb flex-w">
                                <div class="wrap-slick3-dots"></div>
                                <div class="wrap-slick3-arrows flex-sb-m flex-w"></div>

                                <div class="slick3 gallery-lb">
                                    <div class="item-slick3" data-thumb="<?php echo base_url() ?>assets/images/<?php echo $produk_detail[0]['fc_img_1'] ?>">
                                        <div class="wrap-pic-w pos-relative">
                                            <img src="<?php echo base_url() ?>assets/images/<?php echo $produk_detail[0]['fc_img_1'] ?>" alt="IMG-PRODUCT">

                                           
                                            <a class="flex-c-m size-108 how-pos1 bor0 fs-16 cl10 bg0 hov-btn3 trans-04" href="<?php echo base_url() ?>assets/images/<?php echo $produk_detail[0]['fc_img_1'] ?>">
                                                <i class="fa fa-expand"></i>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="item-slick3" data-thumb="<?php echo base_url() ?>assets/images/kotak_2.jpg">
                                        <div class="wrap-pic-w pos-relative">
                                            <img src="<?php echo base_url() ?>assets/images/<?php echo $produk_detail[0]['fc_img_2'] ?>" alt="IMG-PRODUCT">

                                            <a class="flex-c-m size-108 how-pos1 bor0 fs-16 cl10 bg0 hov-btn3 trans-04" href="<?php echo base_url() ?>assets/images/<?php echo $produk_detail[0]['fc_img_2'] ?>">
                                                <i class="fa fa-expand"></i>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="item-slick3" data-thumb="<?php echo base_url() ?>assets/images/kotak_3.jpg">
                                        <div class="wrap-pic-w pos-relative">
                                            <img src="<?php echo base_url() ?>assets/images/<?php echo $produk_detail[0]['fc_img_3'] ?>" alt="IMG-PRODUCT">

                                            <a class="flex-c-m size-108 how-pos1 bor0 fs-16 cl10 bg0 hov-btn3 trans-04" href="<?php echo base_url() ?>assets/images/<?php echo $produk_detail[0]['fc_img_3'] ?>">
                                                <i class="fa fa-expand"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6 col-lg-5 p-b-30">
                        <div class="p-r-50 p-t-5 p-lr-0-lg">
                            <h4 class="mtext-105 cl2 js-name-detail p-b-14 js-name-b2">
                                <?php echo $produk_detail[0]['fv_nama_barang'] ?>
                            </h4>
                             <p class="stext-102 cl3 p-t-23">
                                <?php echo $produk_detail[0]['fv_deskripsi'] ?>
                            </p>
                            <p class="stext-102 cl3 p-t-23">
                                <?php echo $produk_detail[0]['fv_dimensi'] ?>
                            </p>

                              
                        </div>
                         <br />  
                          


                             <div class="flex-w flex-r-m p-b-10">
                                    <div class="size-203 flex-c-m respon6">
                                        Harga
                                    </div>

                                    <div class="size-204 respon6-next">
                                        <div class="rs1-select2 bor8 bg0">
                                            <input class="stext-111 cl2 plh3 size-116 p-l-62 p-r-30" type="text" name="text" value="Rp. <?php echo nominal($produk_detail[0]['fd_harga_barang_publish']);?>" disabled="">
                                             
                                        </div>
                                    </div>
                                </div>
                           <form method="post" action="">
                             <div class="flex-w flex-r-m p-b-10">
                                    <div class="size-203 flex-c-m respon6">
                                        Kode Voucher
                                    </div>

                                    <div class="size-204 respon6-next">
                                   
                                        <div class="rs1-select2 bor8 bg0">
                                            <input class="stext-111 cl2 plh3 size-116 p-l-62 p-r-30" type="text" name="kode_voucher" placeholder="Kode Voucher">
                                             
                                        </div>
                                    
                                    </div>
                                       
                                </div>
                                <div class="flex-w flex-r-m p-b-10">
                                        <div class="size-204 flex-w flex-m respon6-next">
                                        <button type="submit" class="flex-c-m stext-101 cl0 size-101 bg1 bor1 hov-btn1 p-lr-15 trans-04 ">
                                                Cek Voucher
                                            </button>
                                        </div>
                                    </div>  
                            </form> 
                            <?php if(!empty($_POST['kode_voucher'])){
                             $voucher = $_POST['kode_voucher'];
                             $barang =  $produk_detail[0]['fc_kdbarang'];
                             $harga =  $produk_detail[0]['fd_harga_barang_publish'];
                             $query = $this->db->query('select * from tm_voucher where f_kode_voucher="'.$voucher.'" AND fc_kdbarang="'.$barang.'" AND  fc_status="0"');
                            foreach ($query->result() as $value) {
                                    @$nominal = $value->fm_nominal;
                                    @$kode = $value->f_kode_voucher;
                                }   
                            // $this->db->where($where);
                            

                             $harga_diskon = $harga - @$nominal;
                             if (@$kode!=$voucher){
                            ?>
                            <div class="alert alert-danger">
                              <strong>Kode Voucher Yang Anda Masukan Salah!</strong> 
                            </div>
                         
                            <?php }else{?>
                              <div class="alert alert-success">
                              <strong>Kode Voucher Yang Anda Masukan Sukses!</strong> 
                            </div>
                            <?php } ?>
                            <?php }else{
                                $harga_diskon = $produk_detail[0]['fd_harga_barang_publish'];
                            }?>
                            <form id="formAksi">
                            <input type="hidden" name="f_kode_voucher" value="<?php echo @$kode;?>">    
                            <input type="hidden" name="fc_status_stok" value="in stok">
                            <input type="hidden" name="fc_status" value="visitor">
                                 <div class="flex-w flex-r-m p-b-10">
                                    <div class="size-203 flex-c-m respon6">
                                        Jumlah
                                    </div>

                                    <div class="size-204 respon6-next">
                                        <div class="rs1-select2 bor8 bg0">
                                        <select class="js-select2" name="quantity" id="stok">
                                        </select><div class="dropDownSelect2"></div> 
                                          
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="fc_kdgudang" value="<?php echo $stok_produk[0]['fc_kdgudang'] ; ?>">
                                <input type="hidden" name="harga" value="<?php echo @$harga_diskon; ?>">
                                <input type="hidden" name="fc_kdbarang" value="<?php echo $produk_detail[0]['fc_kdbarang'] ?>">
                                <input type="hidden" name="ip_number" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>"><br /><br />
                                <div class="flex-w flex-r-m p-b-10">
                                    <div class="size-204 flex-w flex-m respon6-next">
                                      

                                        <button class="flex-c-m stext-101 cl0 size-101 bg1 bor1 hov-btn1 p-lr-15 trans-04" type="button" id="btn_save" onclick="save()">
                                           Tambah Ke Keranjang
                                        </button>
                                    </div>
                                </div>  
                            </form>    
                            </div>

                            <!--  -->
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>

     
    </section>
