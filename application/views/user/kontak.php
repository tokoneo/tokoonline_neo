<section class="bg-img1 txt-center p-lr-15 p-tb-92" style="background-image: url('<?php echo base_url() ?>assets/images/bg-01.jpg');">
        <h2 class="ltext-105 cl0 txt-center">
           Kontak Kami 
        </h2>
</section> 

<section class="bg0 p-t-75 p-b-120">
        <div class="container">
            <div class="row p-b-148">
                <div class="col-md-7 col-lg-8">
                    <div class="p-t-7 p-r-85 p-r-15-lg p-r-0-md">
                        <h3 class="mtext-111 cl2 p-b-16">
                            Kontak Kami
                        </h3>

                        <p class="stext-113 cl6 p-b-26">
                            <?php echo $kontak->kontak_deskripsi?>
                        </p>

                        
                    </div>
                </div>

                <div class="col-11 col-md-5 col-lg-4 m-lr-auto">
                    <div class="how-bor1 ">
                        <div class="hov-img0">
                            <div id="peta" style="width:472px;height:295px;"></div>  
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </section>

<script     src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAFTimIhQoFCg8bF7PAMgDWi38QqqvaCx8">
  </script>
    <script type="text/javascript">
        (function() {
        window.onload = function() {
        var map;
        //Parameter Google maps
        var options = {
        zoom: 16, //level zoom
        //posisi tengah peta
        center:new google.maps.LatLng('<?php echo @$kontak->kontak_lat;?>' ,'<?php echo @$kontak->kontak_long?>'),
        mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        // Buat peta di
        var map = new google.maps.Map(document.getElementById('peta'), options);
        // Tambahkan Marker
        var locations = [
            ['<?php echo @$kontak->kontak_judul?>', '<?php echo @$kontak->kontak_lat;?>' ,'<?php echo @$kontak->kontak_long?>'],
        ];
        var infowindow = new google.maps.InfoWindow();

        var marker, i;
        /* kode untuk menampilkan banyak marker */
        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map,
        });
        /* menambahkan event clik untuk menampikan
        infowindows dengan isi sesuai denga
        marker yang di klik */

        google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
        infowindow.setContent(locations[i][0]);
        infowindow.open(map, marker);
        }
        })(marker, i));
        }


        };
        })();



</script>           