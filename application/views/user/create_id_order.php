<section class="bg-img1 txt-center p-lr-15 p-tb-92" style="background-image: url('<?php echo base_url() ?>assets/images/bg-01.jpg');">
        <h2 class="ltext-105 cl0 txt-center">
           Konfirmasi Pembayaran
        </h2>
    </section> 
 <section class="bg0 p-t-75 p-b-120">
        <div class="container">
         <div class="bread-crumb flex-w p-l-25 p-r-15 p-t-30 p-lr-0-lg">
                        <a href="index.html" class="stext-109 cl8 hov-cl1 trans-04">
                            Beranda
                            <i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
                        </a>


                        <span class="stext-109 cl4">
                            Konfirmasi Pembayaran
                        </span>
        </div> <br /><br />
        <div class="col-lg-12">
        <div class="bor10 p-lr-40 p-t-30 p-b-40 m-l-63 m-r-40 m-lr-0-xl p-lr-15-sm">
        <table class="table table-condensed total-result">
                  <tbody><tr>
                    <td>ID Order</td>
                    <td> <?php echo $id_order ?></td>
                  </tr>
                  <tr>
                    <td>Ditagihkan Kepada</td>
                    <td><?php echo $order[0]['fv_nama_order']; ?></td>
                  </tr>
                  <tr class="shipping-cost">
                    <td>Kota</td>
                    <td><?php echo $order[0]['fv_kota_order']; ?> </td>                   
                  </tr>
                  <tr class="shipping-cost">
                    <td>Provinsi</td>
                    <td><?php echo $order[0]['fv_provinsi_order']; ?> </td>                   
                  </tr>
                  <tr>
                    <td>Alamat</td>
                    <td><span><?php echo $order[0]['fv_alamat_order']; ?></span></td>
                  </tr>
                  <tr class="shipping-cost">
                    <td>No Telp</td>
                    <td><?php echo $order[0]['fc_telp']; ?></td>                    
                  </tr>
                  <tr class="shipping-cost">
                    <td>Email</td>
                    <td><?php echo $order[0]['fv_email_order']; ?></td>                    
                  </tr>
                    <tr class="shipping-cost">
                    <td>Kode Pos</td>
                    <td><?php echo $order[0]['fc_kode_pos_order']; ?></td>                    
                  </tr>
                </tbody>
        </table> 
        </div>
        <br /> <br />
        <div class="bor10 p-lr-40 p-t-30 p-b-40 m-l-63 m-r-40 m-lr-0-xl p-lr-15-sm">
         <h4 class="mtext-109 cl2 p-b-30">
            <b>Berikut merupakan detail order<b>
            </h4>
          <table class="table table-striped">
              <thead>
                  <tr>
                    <th>Nama Produk</th>
                    <th>Jumlah Produk</th>
                    <th>Subtotal</th>
                  </tr>
                </thead>

                <tbody>

                  <?php 
                      foreach($detail_order as $key){
                    
                  ?>
              
                                  <tr>
                                     <td><strong><?php echo $key['fv_nama_barang'] ?></strong></td>
                                     <td><?php echo $key['f_jumlah_produk'] ?></td>
                                      <td>Rp. <?php echo nominal($key['fm_subtotal']) ?></td>
                                  </tr>
                <?php } ?>                  
                                 


                    <tr>
                      <td>&nbsp;</th>
                      <td><b>Total Order Produk<b></td>
                      <td >Rp. <?php echo nominal($order[0]['fm_total']); ?></td>
                    </tr>
                     <tr>
                      <td>&nbsp;</th>
                      <td><b>Ongkir<b></td>
                      <td >Rp. <?php echo nominal($order[0]['fm_ongkir_order']); ?></td>
                    </tr>
                     <tr>
                      <td>&nbsp;</th>
                      <td><b>Jumlah Tagihan<b></td>
                      <td >Rp. <?php echo nominal($order[0]['fm_grandtotal_order']); ?></td>
                    </tr>
                          
                 
                </tbody>
            </table>
        </div>  
        <br /><br />
        <div class="bor10 p-lr-40 p-t-30 p-b-40 m-l-63 m-r-40 m-lr-0-xl p-lr-15-sm">
         <h4 class="mtext-109 cl2 p-b-30">
            <b>Silahkan Melakukan pembayaran ke salah satu rekening yang tersedia berikut ini:<b>
            </h4>
                       <table class="table">
              <thead>
                <tr>
                  <th>Jenis Bank</th>
                  <th>No Rekening</th>
                  <th>Atas nama</th>
                </tr>
              </thead>
              <tbody>
              
            <?php 
                      foreach($bank as $key){
                    
                     ?>
                      <tr>
                        <td><strong><?php echo $key['jenis_bank'] ?></strong></td>
                        <td><?php echo $key['no_rekening'] ?></td>
                        <td><?php echo $key['atas_nama_bank'] ?></td>
                      </tr>
                      
                      <?php } ?>
                      
               
              </tbody>
            </table>
        </div>  

        </div>
      </div>
</section> 
 <section class="bg0 p-t-75 p-b-120">
        <div class="container">
         <a href="<?php echo base_url('order/konfirmasi_pembayaran')?>" class="flex-c-m stext-101 cl0 size-116 bg3 bor14 hov-btn3 p-lr-15 trans-04 pointer">
                              Konfirmasi Pembayaran
                            </a>  
        </div>
</section>                               