<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_cart extends CI_Model {
	
	var $table = 'td_keranjang_belanja';
	var $column_order = array('fc_kdkeranjang_belanja','fc_kdbarang','fm_harga_produk','fn_jumlah_produk','fm_subtotal_belanja','fc_status_stok',null); //set column field database for datatable orderable
	var $column_search = array('fc_kdkeranjang_belanja','fc_kdbarang','fm_harga_produk','fn_jumlah_produk','fm_subtotal_belanja','fc_status_stok'); //set column field database for datatable searchable just title , author , category are searchable
	var $order = array('td_keranjang_belanja.fc_kdkeranjang_belanja' => 'asc'); // default order

	private function _get_datatables_query() {
		$this->db->select("td_keranjang_belanja.fc_id as id, td_keranjang_belanja.fc_kdkeranjang_belanja,td_keranjang_belanja.fc_kdbarang,td_keranjang_belanja.fm_harga_produk,td_keranjang_belanja.fn_jumlah_produk, fm_subtotal_belanja");
        $this->db->select("td_barang.*");
        $this->db->select("t_temp_order.*");
        $this->db->select('td_stok_barang_gudang.fc_kdstok_gudang,td_stok_barang_gudang.fc_kdgudang,td_stok_barang_gudang.fc_kdbarang,td_stok_barang_gudang.fc_qty_barang');
        $this->db->from("td_keranjang_belanja")->join("td_barang", "td_keranjang_belanja.fc_kdbarang=td_barang.fc_kdbarang",'left')->join("td_stok_barang_gudang", "td_stok_barang_gudang.fc_kdbarang=td_barang.fc_kdbarang",'left')->join("t_temp_order","t_temp_order.fc_kdkeranjang_belanja=td_keranjang_belanja.fc_kdkeranjang_belanja",'left');
		$i = 0;
		foreach ($this->column_search as $item) {
			if($_POST['search']['value']) // if datatable send POST for search
            {

                if($i===0) // first loop
                {
                    // $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i); //last loop
                    // $this->db->group_end(); //close bracket


            }

			$i++;
		}

		if (isset($_REQUEST['order'])) {
			$this->db->order_by($this->column_order[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function count_filtered($id) {
		$this->_get_datatables_query();
		$this->db->where('td_keranjang_belanja.fc_kdkeranjang_belanja',$_SERVER['REMOTE_ADDR']);
		$this->db->group_by('td_keranjang_belanja.fc_kdbarang');
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_all($id) {
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_datatables($id) {
		$this->_get_datatables_query();
		$this->db->where('td_keranjang_belanja.fc_kdkeranjang_belanja',$_SERVER['REMOTE_ADDR']);
		$this->db->group_by('td_keranjang_belanja.fc_kdbarang');
		if ($_REQUEST['length'] != -1) {
			$this->db->limit($_REQUEST['length'], $_REQUEST['start']);
		}

		$query = $this->db->get();
		return $query->result();
	}

	public function delete_by_id($id) {
		$this->db->where('fc_id', $id);
		$this->db->delete($this->table);
	}


}	