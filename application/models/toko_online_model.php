<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Toko_online_model extends CI_Model {


    public function get_slider(){
       $query = $this->db->get('tb_slider');
       return $query->result_array();
    }

    public function get_produk_terbaru(){
        $this->db->limit(3);
        $this->db->order_by('fc_id','desc');
        $query = $this->db->get('td_barang');
        return $query->result();
    }

    public function get_kategori_produk(){
        $query = $this->db->get('tm_kategori_barang');
        return $query->result_array();
    }

    public function get_data_produk(){
          $this->db->select('*');
          $this->db->from('td_barang');
          $this->db->order_by('fc_id', 'DESC');
          $query = $this->db->get();
          return $query->result_array();
    }

    function get_table_where($table, $where) {
        $this->db->where($where);
        $query = $this->db->get($table);
        // if ($this->db->_error_message()) header('Location: ../');
        return $query->result_array();
    }

    function get_where_stok($id_produk){
        $this->db->select('fc_qty_barang,fc_kdgudang');
        $this->db->from('td_stok_barang_gudang');
        $this->db->where('fc_kdbarang', $id_produk);
        $this->db->where('fc_kdgudang', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_stok(){
        $this->db->select('fc_qty_barang,fc_kdgudang');
        $this->db->from('td_stok_barang_gudang');
        $this->db->where('fc_kdgudang', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

     function get_keranjang_belanja($where) {
        $this->db->select("td_keranjang_belanja.*");
        $this->db->select("td_barang.*");
        // $this->db->select("produk.harga as harga_produk");
        // $this->db->select("produk.foto_produk1 as foto_produk1");
        // $this->db->select("produk.jumlah_stok as jumlah_stok");
        $this->db->from("td_keranjang_belanja")->join("td_barang", "td_keranjang_belanja.fc_kdbarang=td_barang.fc_kdbarang");
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result_array();
    }

      function get_keranjang_belanja2($where) {
        $this->db->select("td_keranjang_belanja.fc_id as id, td_keranjang_belanja.fc_kdkeranjang_belanja,td_keranjang_belanja.fc_kdbarang,td_keranjang_belanja.fm_harga_produk,td_keranjang_belanja.fn_jumlah_produk, fm_subtotal_belanja");
        $this->db->select("td_barang.*");
        $this->db->select_sum('td_stok_barang_gudang.fc_qty_barang');
        // $this->db->select("produk.harga as harga_produk");
        // $this->db->select("produk.foto_produk1 as foto_produk1");
        // $this->db->select("produk.jumlah_stok as jumlah_stok");
        $this->db->from("td_keranjang_belanja")->join("td_barang", "td_keranjang_belanja.fc_kdbarang=td_barang.fc_kdbarang")->join("td_stok_barang_gudang", "td_stok_barang_gudang.fc_kdbarang=td_barang.fc_kdbarang");
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_subtotal($where){
        $this->db->select('*');
        $this->db->from('td_keranjang_belanja');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result_array();
    }

    function update_table($table, $data, $where) {
        $this->db->where($where);
        $query = $this->db->update($table, $data);
    }

     function insert_table($table, $data) {
        $query = $this->db->insert($table, $data);
        return $query;
    }

    function get_cek_pesan(){
        $this->db->where('fc_status','visitor');
        $query = $this->db->get('td_keranjang_belanja')->num_rows();
        return $query;
    }

    function get_cek_pesan_user($user){
        $this->db->where('fc_kdkeranjang_belanja', $user);
        $query = $this->db->get('td_keranjang_belanja')->num_rows();
        return $query;
    }

    function delete_table($table, $where) {
        $this->db->where($where);
        $query = $this->db->delete($table);
        return $query;
    }

    
    function get_penjual_cart($where) {
        $this->db->select("td_barang.fc_user");
        // $this->db->select("produk.harga as harga_produk");
        // $this->db->select("produk.foto_produk1 as foto_produk1");
        // $this->db->select("produk.jumlah_stok as jumlah_stok");
        $this->db->from("td_keranjang_belanja")->join("td_barang", "td_keranjang_belanja.fc_kdbarang=td_barang.fc_kdbarang");
        $this->db->where($where);
        $this->db->group_by("td_barang.fc_user");
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_data_keranjang($where){
        $this->db->select("td_keranjang_belanja.*");
        $this->db->select("td_barang.*");
        $this->db->from("td_keranjang_belanja")->join("td_barang","td_keranjang_belanja.fc_kdbarang=td_barang.fc_kdbarang");
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result_array();
    }


    function get_table_rows_where($row,$table,$where) {
        $this->db->select($row);
        $this->db->from($table);
        $this->db->where($where);   
        $query = $this->db->get();
        return $query->result_array();
    }

    function select_table_order_limit($row, $namatabel, $order_trigger, $limit){
            $this->db->select($row);
            $this->db->from($namatabel);
            $this->db->order_by($order_trigger,"desc");
            $this->db->limit($limit); 
            $query = $this->db->get();
            return $query->result_array();

    }

    function get_table_join_where($table1, $table2, $on, $where) {
        $this->db->select('*');
        $this->db->from($table1);
        $this->db->join($table2, $on);
        $this->db->where($where);

        $query = $this->db->get();

        return $query->result_array();
    }

     function get_penjual_order($where){
        $this->db->select("a.*, b.*");
        $this->db->from("td_order a");
        $this->db->join("td_barang b","b.fc_kdbarang=a.fc_kdbarang");
        $this->db->where($where);
        $this->db->order_by("fc_penjual", "desc");
        $this->db->group_by("fc_penjual"); 
        $query = $this->db->get();
        return $query->result_array();
    }

     function get_order($where){
        $this->db->select("td_order.*");
        $this->db->select("tm_user.nama as nama");
        $this->db->select("td_barang.*");
        $this->db->from("td_order")->join("tm_user","td_order.fc_penjual=tm_user.id_user");
        $this->db->join('td_barang', 'td_order.fc_kdbarang = td_barang.fc_kdbarang');
        $this->db->where($where);
        $this->db->order_by("td_order.fc_penjual", "desc"); 
        $query = $this->db->get();
        return $query->result_array();
    }

    function view(){
        return $this->db->get('td_barang')->result_array();
    }

     public function search($keyword){
        $this->db->like('fv_nama_barang', $keyword);
        $this->db->or_like('fv_deskripsi', $keyword);
        
        $result = $this->db->get('td_barang')->result_array(); // Tampilkan data siswa berdasarkan keyword
        
        return $result; 
    }

    public function tampil_kategori($id=null, $status=null){
        if($id==null){
            $this->db->limit(100);
        }else{
            if($status==null){
                $this->db->where('tm_kategori_barang.fc_id', $id);
            }else{
                $this->db->where('td_barang.fc_id', $status);
            }
        }

        $this->db->order_by('tm_kategori_barang.fc_id', 'DESC');
        $this->db->group_by('td_barang.fc_id');
        $this->db->select('td_barang.*,tm_kategori_barang.*');
        $this->db->from('td_barang');
        $this->db->join('tm_kategori_barang','tm_kategori_barang.fc_id=td_barang.fc_kdkategori','left outer');
        return $this->db->get(); 
    }

    public function get_ambil_waktu(){
        $this->db->where('fc_param','waktu');
        $query = $this->db->get('t_setup');
        return $query->result_array();
    }

    public function stok_barang(){
        $query = $this->db->get('td_keranjang_belanja');
        return $query->result_array();
    }

    public function get_tmorder($id){
        $this->db->where('fc_kdorder',$id);
        $query = $this->db->get('tm_order');
        return $query->row();
    }

    public function tentang(){
        $this->db->where('id_about', 1);
        $query = $this->db->get('tb_about');
        return $query->row();
    }

    public function kontak(){
        $this->db->where('id_kontak', 1);
        $query = $this->db->get('tb_kontak');
        return $query->row();
    }
}
