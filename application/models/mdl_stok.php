<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_stok extends CI_Model {
	
	var $table = 'td_stok_barang_gudang';
	var $column_order = array('fc_kdstok_gudang','fc_kdbarang','fc_kdgudang','fc_qty_barang','fm_subtotal_belanja','fc_status_stok',null); //set column field database for datatable orderable
	var $column_search = array('fc_kdstok_gudang','fc_kdbarang','fc_kdgudang','fc_qty_barang','fm_subtotal_belanja','fc_status_stok'); //set column field database for datatable searchable just title , author , category are searchable
	var $order = array('td_stok_barang_gudang.fc_kdstok_gudang' => 'asc'); // default order

	private function _get_datatables_query() {
		$this->db->select("td_barang.*");
        $this->db->select("td_stok_barang_gudang.*");
        $this->db->select("tm_gudang.*");
        $this->db->from("td_stok_barang_gudang")->join("td_barang", "td_barang.fc_kdbarang=td_stok_barang_gudang.fc_kdbarang",'left')->join("tm_gudang", "tm_gudang.fc_kdgudang=td_stok_barang_gudang.fc_kdgudang",'left');
		$i = 0;
		foreach ($this->column_search as $item) {
			if($_POST['search']['value']) // if datatable send POST for search
            {

                if($i===0) // first loop
                {
                    // $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i); //last loop
                    // $this->db->group_end(); //close bracket


            }

			$i++;
		}

		if (isset($_REQUEST['order'])) {
			$this->db->order_by($this->column_order[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function count_filtered($id) {
		$this->_get_datatables_query();
		$this->db->where('td_stok_barang_gudang.fc_kdbarang',$id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_all($id) {
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_datatables($id) {
		$this->_get_datatables_query();
		$this->db->where('td_stok_barang_gudang.fc_kdbarang',$id);
		if ($_REQUEST['length'] != -1) {
			$this->db->limit($_REQUEST['length'], $_REQUEST['start']);
		}

		$query = $this->db->get();
		return $query->result();
	}

	public function delete_by_id($id) {
		$this->db->where('fc_id', $id);
		$this->db->delete($this->table);
	}


}	