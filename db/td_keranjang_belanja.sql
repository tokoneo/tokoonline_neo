/*
Navicat MySQL Data Transfer

Source Server         : Server Local
Source Server Version : 50532
Source Host           : 127.0.0.1:3306
Source Database       : db_shopneo_new

Target Server Type    : MYSQL
Target Server Version : 50532
File Encoding         : 65001

Date: 2018-10-26 18:50:33
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for td_keranjang_belanja
-- ----------------------------
DROP TABLE IF EXISTS `td_keranjang_belanja`;
CREATE TABLE `td_keranjang_belanja` (
  `fc_id` int(11) NOT NULL AUTO_INCREMENT,
  `fc_kdkeranjang_belanja` char(30) DEFAULT NULL,
  `fc_kdbarang` char(20) DEFAULT NULL,
  `fm_harga_produk` double(15,0) DEFAULT NULL,
  `fn_jumlah_produk` int(10) DEFAULT NULL,
  `fm_subtotal_belanja` double(15,0) DEFAULT NULL,
  `fc_status_stok` char(30) DEFAULT NULL,
  `fc_status` char(10) DEFAULT NULL,
  PRIMARY KEY (`fc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of td_keranjang_belanja
-- ----------------------------
