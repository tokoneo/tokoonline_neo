/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : db_shopneo_new

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-10-24 10:19:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for data_bank
-- ----------------------------
DROP TABLE IF EXISTS `data_bank`;
CREATE TABLE `data_bank` (
  `id_data` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_bank` varchar(255) NOT NULL,
  `atas_nama_bank` varchar(255) NOT NULL,
  `no_rekening` varchar(255) NOT NULL,
  `id_user` varchar(100) NOT NULL,
  PRIMARY KEY (`id_data`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of data_bank
-- ----------------------------
INSERT INTO `data_bank` VALUES ('1', 'Mandiri', 'Nama', '32523523', '');
INSERT INTO `data_bank` VALUES ('2', 'BRI', 'Nama', '52352', '');

-- ----------------------------
-- Table structure for konfirmasi_bayar
-- ----------------------------
DROP TABLE IF EXISTS `konfirmasi_bayar`;
CREATE TABLE `konfirmasi_bayar` (
  `id_konfirmasi` int(11) NOT NULL AUTO_INCREMENT,
  `tgl_konfirmasi` date NOT NULL,
  `id_order` varchar(255) NOT NULL,
  `jumlah_bayar` int(11) NOT NULL,
  `bank_bayar` varchar(20) NOT NULL,
  `rekening_bayar` varchar(30) NOT NULL,
  `nama_bayar` varchar(30) NOT NULL,
  `foto` text NOT NULL,
  PRIMARY KEY (`id_konfirmasi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of konfirmasi_bayar
-- ----------------------------

-- ----------------------------
-- Table structure for mainmenu
-- ----------------------------
DROP TABLE IF EXISTS `mainmenu`;
CREATE TABLE `mainmenu` (
  `seq` int(11) NOT NULL AUTO_INCREMENT,
  `idmenu` int(11) NOT NULL,
  `nama_menu` varchar(50) NOT NULL,
  `active_menu` varchar(50) NOT NULL,
  `icon_class` varchar(50) NOT NULL,
  `link_menu` varchar(50) NOT NULL,
  `menu_akses` varchar(12) NOT NULL,
  `entry_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `entry_user` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`seq`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mainmenu
-- ----------------------------
INSERT INTO `mainmenu` VALUES ('1', '1', 'Dashboard', '', 'menu-icon fa fa-tachometer', 'Home', '', '2017-10-18 00:28:54', null);
INSERT INTO `mainmenu` VALUES ('8', '8', 'Administrator', '', 'menu-icon fa fa-user', '#', '', '2017-10-14 00:57:17', null);
INSERT INTO `mainmenu` VALUES ('2', '2', 'Slider', '', 'menu-icon fa fa-file-image-o', 'Slider', '', '2017-10-18 00:28:56', null);
INSERT INTO `mainmenu` VALUES ('3', '3', 'Pencairan Poin', '', 'menu-icon fa fa-money', 'Pencairanpoin', '', '2018-10-24 08:49:50', null);

-- ----------------------------
-- Table structure for ongkir_pembeli
-- ----------------------------
DROP TABLE IF EXISTS `ongkir_pembeli`;
CREATE TABLE `ongkir_pembeli` (
  `id_ongkir` int(11) NOT NULL AUTO_INCREMENT,
  `ongkir` double NOT NULL,
  `id_order` varchar(50) NOT NULL,
  `id_penjual` varchar(50) NOT NULL,
  `tagihan_admin` int(11) NOT NULL DEFAULT '0',
  `pembayaran` int(11) NOT NULL DEFAULT '0',
  `jasa_pengiriman` varchar(255) NOT NULL,
  PRIMARY KEY (`id_ongkir`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ongkir_pembeli
-- ----------------------------
INSERT INTO `ongkir_pembeli` VALUES ('1', '19000', 'T170131001', 'eka', '19000', '0', 'JNE - OKE');
INSERT INTO `ongkir_pembeli` VALUES ('2', '22000', 'T170201001', 'eka', '0', '0', 'JNE - OKE');
INSERT INTO `ongkir_pembeli` VALUES ('3', '32000', 'T170201002', 'eka', '0', '0', 'JNE - OKE');
INSERT INTO `ongkir_pembeli` VALUES ('4', '19000', 'T170202001', 'eka', '0', '0', 'JNE - OKE');
INSERT INTO `ongkir_pembeli` VALUES ('5', '34000', 'T170202002', 'eka', '0', '0', 'JNE - OKE');
INSERT INTO `ongkir_pembeli` VALUES ('6', '5000', 'T170417001', 'wildan', '0', '0', 'JNE - CTC');
INSERT INTO `ongkir_pembeli` VALUES ('7', '7000', 'T170517001', 'wildanafif', '7000', '0', 'POS - Surat Kilat Khusus');
INSERT INTO `ongkir_pembeli` VALUES ('8', '4000', 'T170520001', 'afif', '0', '4000', 'JNE - CTCOKE');
INSERT INTO `ongkir_pembeli` VALUES ('9', '40000', 'T180920001', 'yayanraw', '0', '0', 'JNE - OKE');
INSERT INTO `ongkir_pembeli` VALUES ('10', '5000', 'T180922001', 'SMKN4Malang', '0', '0', 'JNE - OKE');
INSERT INTO `ongkir_pembeli` VALUES ('11', '0', 'T181018004', 'TOKONEO', '0', '0', '0');
INSERT INTO `ongkir_pembeli` VALUES ('12', '10000', 'T181018001', 'TOKONEO', '0', '0', 'JNE - OKE');
INSERT INTO `ongkir_pembeli` VALUES ('13', '10000', 'T181018002', 'TOKONEO', '0', '0', 'JNE - OKE');
INSERT INTO `ongkir_pembeli` VALUES ('14', '10000', 'T181018003', 'TOKONEO', '0', '0', 'JNE - OKE');
INSERT INTO `ongkir_pembeli` VALUES ('15', '14000', 'T181018001', 'TOKONEO', '0', '0', 'JNE - OKE');
INSERT INTO `ongkir_pembeli` VALUES ('16', '42000', 'T181018001', 'TOKONEO', '0', '0', 'JNE - OKE');
INSERT INTO `ongkir_pembeli` VALUES ('17', '10000', 'T181018001', 'TOKONEO', '0', '0', 'JNE - OKE');
INSERT INTO `ongkir_pembeli` VALUES ('18', '10000', 'T181018001', 'TOKONEO', '0', '0', 'JNE - OKE');
INSERT INTO `ongkir_pembeli` VALUES ('19', '10000', 'T181020001', 'TOKONEO', '0', '0', 'JNE - OKE');
INSERT INTO `ongkir_pembeli` VALUES ('20', '10000', 'T181020001', 'TOKONEO', '0', '0', 'JNE - OKE');
INSERT INTO `ongkir_pembeli` VALUES ('21', '10000', 'T181020001', 'TOKONEO', '0', '0', 'JNE - CTC');
INSERT INTO `ongkir_pembeli` VALUES ('22', '14000', 'T181020001', 'TOKONEO', '0', '0', 'JNE - OKE');
INSERT INTO `ongkir_pembeli` VALUES ('23', '10000', 'T181020001', 'TOKONEO', '0', '0', 'JNE - OKE');
INSERT INTO `ongkir_pembeli` VALUES ('24', '10000', 'T181020002', 'TOKONEO', '0', '0', 'JNE - OKE');
INSERT INTO `ongkir_pembeli` VALUES ('25', '14000', 'T181020004', 'TOKONEO', '0', '0', 'JNE - OKE');
INSERT INTO `ongkir_pembeli` VALUES ('26', '10000', 'T181020001', 'TOKONEO', '0', '0', 'JNE - OKE');
INSERT INTO `ongkir_pembeli` VALUES ('27', '10000', 'T181020001', 'TOKONEO', '0', '0', 'JNE - OKE');

-- ----------------------------
-- Table structure for pencairan_poin
-- ----------------------------
DROP TABLE IF EXISTS `pencairan_poin`;
CREATE TABLE `pencairan_poin` (
  `kode_cairpoin` int(11) NOT NULL AUTO_INCREMENT,
  `id_user_dapat` varchar(20) NOT NULL,
  `fc_id_order_detail` varchar(30) NOT NULL,
  `fc_kdpoin` int(11) NOT NULL,
  `selisih_harga` int(11) NOT NULL,
  `keuntungan_harga` int(11) NOT NULL,
  `persentasi` int(11) NOT NULL,
  `total_poin` int(11) NOT NULL,
  `status_ambil` int(11) NOT NULL,
  `tgl_ambil` datetime NOT NULL,
  `id_user_pencair` varchar(20) NOT NULL,
  PRIMARY KEY (`kode_cairpoin`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pencairan_poin
-- ----------------------------

-- ----------------------------
-- Table structure for submenu
-- ----------------------------
DROP TABLE IF EXISTS `submenu`;
CREATE TABLE `submenu` (
  `id_sub` int(11) NOT NULL AUTO_INCREMENT,
  `nama_sub` varchar(50) NOT NULL,
  `mainmenu_idmenu` int(11) NOT NULL,
  `active_sub` varchar(20) NOT NULL,
  `icon_class` varchar(100) NOT NULL,
  `link_sub` varchar(50) NOT NULL,
  `sub_akses` varchar(12) NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `entry_user` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_sub`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of submenu
-- ----------------------------
INSERT INTO `submenu` VALUES ('1', 'Entry User', '8', '', '', 'User', '', '2017-10-18 00:28:25', null);
INSERT INTO `submenu` VALUES ('2', 'Kategori Produk', '4', '', '', 'Produk', '', '2017-10-18 00:34:17', null);
INSERT INTO `submenu` VALUES ('3', 'Produk', '4', '', '', 'Produk/detail', '', '2018-02-06 11:33:07', null);
INSERT INTO `submenu` VALUES ('4', 'Album', '5', '', '', 'Gallery', '', '2017-10-18 00:34:34', null);
INSERT INTO `submenu` VALUES ('5', 'Foto', '5', '', '', 'Gallery/foto', '', '2018-02-06 11:27:39', null);

-- ----------------------------
-- Table structure for tab_akses_mainmenu
-- ----------------------------
DROP TABLE IF EXISTS `tab_akses_mainmenu`;
CREATE TABLE `tab_akses_mainmenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_menu` int(11) NOT NULL,
  `id_level` int(11) NOT NULL,
  `c` int(11) DEFAULT '0',
  `r` int(11) DEFAULT '0',
  `u` int(11) DEFAULT '0',
  `d` int(11) DEFAULT '0',
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `entry_user` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tab_akses_mainmenu
-- ----------------------------
INSERT INTO `tab_akses_mainmenu` VALUES ('1', '1', '1', null, '1', null, null, '2017-09-25 23:49:01', 'direktur');
INSERT INTO `tab_akses_mainmenu` VALUES ('2', '8', '1', '0', '0', '0', '0', '2017-10-19 01:47:26', '');
INSERT INTO `tab_akses_mainmenu` VALUES ('3', '2', '1', '0', '1', '0', '0', '2017-10-14 02:29:46', '');
INSERT INTO `tab_akses_mainmenu` VALUES ('4', '3', '1', '0', '1', '0', '0', '2017-10-14 02:29:46', '');

-- ----------------------------
-- Table structure for tab_akses_submenu
-- ----------------------------
DROP TABLE IF EXISTS `tab_akses_submenu`;
CREATE TABLE `tab_akses_submenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_sub_menu` int(11) NOT NULL,
  `id_level` int(11) NOT NULL,
  `c` int(11) DEFAULT '0',
  `r` int(11) DEFAULT '0',
  `u` int(11) DEFAULT '0',
  `d` int(11) DEFAULT '0',
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `entry_user` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tab_akses_submenu
-- ----------------------------
INSERT INTO `tab_akses_submenu` VALUES ('1', '1', '1', '0', '1', '0', '0', '2017-10-14 00:45:40', '');
INSERT INTO `tab_akses_submenu` VALUES ('2', '2', '1', '0', '1', '0', '0', '2017-10-16 05:59:02', '');
INSERT INTO `tab_akses_submenu` VALUES ('3', '3', '1', '0', '0', '0', '0', '2017-10-18 11:12:32', '');
INSERT INTO `tab_akses_submenu` VALUES ('4', '4', '1', '0', '1', '0', '0', '2017-10-16 05:59:16', '');
INSERT INTO `tab_akses_submenu` VALUES ('5', '5', '1', '0', '0', '0', '0', '2017-10-18 11:12:33', '');

-- ----------------------------
-- Table structure for tb_slider
-- ----------------------------
DROP TABLE IF EXISTS `tb_slider`;
CREATE TABLE `tb_slider` (
  `id_slider` int(11) NOT NULL AUTO_INCREMENT,
  `fv_slider_judul` varchar(200) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `fv_slider_deskripsi` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `fc_slider_gambar` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `id_user` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_slider`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_slider
-- ----------------------------
INSERT INTO `tb_slider` VALUES ('1', 'Furniture', 'Kursi Rotan', '1.jpg', null);
INSERT INTO `tb_slider` VALUES ('2', 'Furniture', 'Meja Artistik', '2.jpg', null);
INSERT INTO `tb_slider` VALUES ('3', 'Furniture', 'Meja Kayu Jati', '3.jpg', null);

-- ----------------------------
-- Table structure for td_barang
-- ----------------------------
DROP TABLE IF EXISTS `td_barang`;
CREATE TABLE `td_barang` (
  `fc_id` int(11) NOT NULL AUTO_INCREMENT,
  `fc_kdbarang` char(20) DEFAULT NULL,
  `fc_kdkategori` int(11) DEFAULT NULL,
  `fv_nama_barang` varchar(100) DEFAULT NULL,
  `fv_deskripsi` text,
  `fc_img_1` text,
  `fc_img_2` text,
  `fc_img_3` text,
  `fc_img_4` text,
  `fd_harga_barang_publish` double(15,0) DEFAULT NULL,
  `fd_harga_barang_min` double(15,0) DEFAULT NULL,
  `fv_jenis_poin` varchar(20) DEFAULT NULL,
  `fc_kdgudang` int(11) DEFAULT NULL,
  `fv_berat` char(10) DEFAULT NULL,
  `fv_dimensi` char(30) DEFAULT NULL,
  `id_user` char(20) DEFAULT NULL,
  `fc_status_stok` char(30) DEFAULT NULL,
  PRIMARY KEY (`fc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of td_barang
-- ----------------------------
INSERT INTO `td_barang` VALUES ('1', 'BI00001', '5', 'Meja Kayu Jati', 'Meja Kayu Jati Adalah', 'kotak_1.jpg', 'kotak_2.jpg', 'kotak_3.jpg', 'kotak_4.jpg', '1000000', '500000', 'satu poin', '1', '2000', '110x38x80cm', 'TOKONEO', 'in stok');
INSERT INTO `td_barang` VALUES ('2', 'BI00002', '5', 'Meja Kayu Jati Merah', 'Meja Kayu Jati Adalah', 'kotak_2.jpg', 'kotak_2.jpg', 'kotak_3.jpg', 'kotak_4.jpg', '1000000', '500000', 'satu poin', '1', '2000', '110x38x80cm', 'TOKONEO', 'pre order');

-- ----------------------------
-- Table structure for td_keranjang_belanja
-- ----------------------------
DROP TABLE IF EXISTS `td_keranjang_belanja`;
CREATE TABLE `td_keranjang_belanja` (
  `fc_id` int(11) NOT NULL AUTO_INCREMENT,
  `fc_kdkeranjang_belanja` char(30) DEFAULT NULL,
  `fc_kdbarang` char(20) DEFAULT NULL,
  `fm_harga_produk` double(15,0) DEFAULT NULL,
  `fn_jumlah_produk` int(10) DEFAULT NULL,
  `fm_subtotal_belanja` double(15,0) DEFAULT NULL,
  `fc_status_stok` char(30) DEFAULT NULL,
  PRIMARY KEY (`fc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of td_keranjang_belanja
-- ----------------------------
INSERT INTO `td_keranjang_belanja` VALUES ('4', '::1', 'BI00001', '1000000', '2', '2000000', 'in stok');

-- ----------------------------
-- Table structure for td_konfirmasi_bayar
-- ----------------------------
DROP TABLE IF EXISTS `td_konfirmasi_bayar`;
CREATE TABLE `td_konfirmasi_bayar` (
  `fc_kdkonfirmasi` int(11) NOT NULL AUTO_INCREMENT,
  `fd_tgl_konfirmsi` date DEFAULT NULL,
  `fc_kdorder` char(15) DEFAULT NULL,
  `fm_jumlah_bayar` double(10,0) DEFAULT NULL,
  `fc_bank_bayar` char(20) DEFAULT NULL,
  `fc_rekening_bayar` char(30) DEFAULT NULL,
  `fv_nama_bayar` varchar(30) DEFAULT NULL,
  `fc_img` text,
  PRIMARY KEY (`fc_kdkonfirmasi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of td_konfirmasi_bayar
-- ----------------------------

-- ----------------------------
-- Table structure for td_order
-- ----------------------------
DROP TABLE IF EXISTS `td_order`;
CREATE TABLE `td_order` (
  `fc_id_order_detail` int(11) NOT NULL AUTO_INCREMENT,
  `fc_kdorder` char(30) DEFAULT NULL,
  `fc_penjual` char(30) DEFAULT NULL,
  `fc_kdbarang` char(15) DEFAULT NULL,
  `f_jumlah_produk` int(10) DEFAULT NULL,
  `f_berat_produk` int(10) DEFAULT NULL,
  `fm_harga` double(15,0) DEFAULT NULL,
  `fm_harga_pajak` double(15,0) DEFAULT NULL,
  `fm_subtotal` double(15,0) DEFAULT NULL,
  `fm_subtotal_pajak` double(15,0) DEFAULT NULL,
  `fm_pembayaran` double(15,0) DEFAULT NULL,
  `fm_tagihan` double(15,0) DEFAULT NULL,
  PRIMARY KEY (`fc_id_order_detail`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of td_order
-- ----------------------------

-- ----------------------------
-- Table structure for td_stok_barang_gudang
-- ----------------------------
DROP TABLE IF EXISTS `td_stok_barang_gudang`;
CREATE TABLE `td_stok_barang_gudang` (
  `fc_kdstok_gudang` int(11) NOT NULL AUTO_INCREMENT,
  `fc_kdgudang` int(11) DEFAULT NULL,
  `fc_kdbarang` char(15) DEFAULT NULL,
  `fc_qty_barang` char(10) DEFAULT NULL,
  `id_user` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`fc_kdstok_gudang`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of td_stok_barang_gudang
-- ----------------------------
INSERT INTO `td_stok_barang_gudang` VALUES ('1', '1', 'BI00001', '15', null);
INSERT INTO `td_stok_barang_gudang` VALUES ('2', '2', 'BI00001', '2', null);
INSERT INTO `td_stok_barang_gudang` VALUES ('3', '1', 'BI00002', '2', null);

-- ----------------------------
-- Table structure for tm_gudang
-- ----------------------------
DROP TABLE IF EXISTS `tm_gudang`;
CREATE TABLE `tm_gudang` (
  `fc_kdgudang` int(11) NOT NULL AUTO_INCREMENT,
  `fv_nmgudang` varchar(30) DEFAULT NULL,
  `fv_alamat` text,
  `id_user` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`fc_kdgudang`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tm_gudang
-- ----------------------------
INSERT INTO `tm_gudang` VALUES ('1', 'Gudang Galunggung', null, null);
INSERT INTO `tm_gudang` VALUES ('2', 'Matos', null, null);

-- ----------------------------
-- Table structure for tm_kategori_barang
-- ----------------------------
DROP TABLE IF EXISTS `tm_kategori_barang`;
CREATE TABLE `tm_kategori_barang` (
  `fc_id` int(11) NOT NULL AUTO_INCREMENT,
  `fv_nama_kategori` varchar(100) DEFAULT NULL,
  `id_user` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`fc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tm_kategori_barang
-- ----------------------------
INSERT INTO `tm_kategori_barang` VALUES ('1', 'Civing Room', null);
INSERT INTO `tm_kategori_barang` VALUES ('2', 'Bad Room', null);
INSERT INTO `tm_kategori_barang` VALUES ('3', 'Dining Room', null);
INSERT INTO `tm_kategori_barang` VALUES ('4', 'Dekorasi', null);
INSERT INTO `tm_kategori_barang` VALUES ('5', 'Interior', null);
INSERT INTO `tm_kategori_barang` VALUES ('6', 'Eterior + Pagar', null);
INSERT INTO `tm_kategori_barang` VALUES ('7', 'Kusen Pintu', null);
INSERT INTO `tm_kategori_barang` VALUES ('8', 'Gebyok', null);
INSERT INTO `tm_kategori_barang` VALUES ('9', 'Gazebo', null);
INSERT INTO `tm_kategori_barang` VALUES ('10', 'Joglo', null);
INSERT INTO `tm_kategori_barang` VALUES ('11', 'Classic', null);

-- ----------------------------
-- Table structure for tm_order
-- ----------------------------
DROP TABLE IF EXISTS `tm_order`;
CREATE TABLE `tm_order` (
  `fc_kdorder` char(30) NOT NULL,
  `fd_tgl_order` date DEFAULT NULL,
  `fm_total` double(15,0) DEFAULT NULL,
  `fc_status_order` char(30) DEFAULT NULL,
  `fv_nama_order` varchar(30) DEFAULT NULL,
  `fv_email_order` varchar(30) DEFAULT NULL,
  `fv_alamat_order` text,
  `fc_telp` char(12) DEFAULT NULL,
  `fc_kode_pos_order` char(8) DEFAULT NULL,
  `fv_provinsi_order` varchar(30) DEFAULT NULL,
  `fv_kota_order` varchar(30) DEFAULT NULL,
  `fm_ongkir_order` double(10,0) DEFAULT NULL,
  `fm_grandtotal_order` double(15,0) DEFAULT NULL,
  `fc_status_kirim` char(1) DEFAULT '1',
  `fc_status_konf` char(1) DEFAULT NULL,
  `fc_jenis_stok_order` char(30) DEFAULT NULL,
  PRIMARY KEY (`fc_kdorder`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tm_order
-- ----------------------------

-- ----------------------------
-- Table structure for tm_poin
-- ----------------------------
DROP TABLE IF EXISTS `tm_poin`;
CREATE TABLE `tm_poin` (
  `fc_kdpoin` int(11) NOT NULL AUTO_INCREMENT,
  `fc_jumlah_poin` char(15) DEFAULT NULL,
  `fc_min_persen` char(15) DEFAULT NULL,
  `fc_max_persen` char(15) DEFAULT NULL,
  `id_user` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`fc_kdpoin`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tm_poin
-- ----------------------------
INSERT INTO `tm_poin` VALUES ('1', '1', '1', '25', 'admn');
INSERT INTO `tm_poin` VALUES ('2', '2', '26', '50', 'admin');
INSERT INTO `tm_poin` VALUES ('3', '3', '51', '75', 'admin');
INSERT INTO `tm_poin` VALUES ('4', '4', '76', '99', 'admin');

-- ----------------------------
-- Table structure for tm_user
-- ----------------------------
DROP TABLE IF EXISTS `tm_user`;
CREATE TABLE `tm_user` (
  `id_user` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(30) NOT NULL,
  `level` varchar(30) NOT NULL,
  `status` varchar(1) NOT NULL,
  `foto` text,
  `password` varchar(100) NOT NULL,
  `provinsi` varchar(50) NOT NULL,
  `kota` varchar(50) NOT NULL,
  `id_ongkir` int(11) NOT NULL,
  `aktif_user` tinyint(1) NOT NULL,
  `nama_rek_user` varchar(255) NOT NULL,
  `no_rek_user` varchar(255) NOT NULL,
  `bank_rek_user` varchar(255) NOT NULL,
  `view_password` varchar(100) DEFAULT NULL,
  `admin_level` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tm_user
-- ----------------------------
INSERT INTO `tm_user` VALUES ('admin', 'admin', 'admin@admin.com', 'admin', '1', null, 'e00cf25ad42683b3df678c61f42c6bda', '', '', '0', '0', '', '', '', 'admin1', '1');
INSERT INTO `tm_user` VALUES ('afif', 'wildan afif', 'wildan@gmail.com', 'member', '', null, 'e353cc5a41c4b79ffa728ce8b80d1b62', 'Jawa Timur', 'Malang', '256', '1', 'wildan afif', '3244325423', 'Mandiri', null, null);
INSERT INTO `tm_user` VALUES ('AMurleloli', 'AMurleloli', 'quonahquee@bestmailonline.com', 'member', '', null, '68856c90365e35e3e8df64cc4e9c3345', '', '', '0', '0', 'AMurleloli', 'AMurleloli', 'La Strada  hardcore', null, null);
INSERT INTO `tm_user` VALUES ('Briandab', 'Briandab', 'xrumm88965@gmail.com', 'member', '', null, '63710fe5626abcda4dc059508f4a7fd1', '', '', '0', '0', 'Briandab', 'Briandab', 'Pearl Jam', null, null);
INSERT INTO `tm_user` VALUES ('danaukerinciraya', 'Admin', 'admin@admin.com', 'admin', '1', '', '827ccb0eea8a706c4c34a16891f84e7b', '0', '0', '0', '0', '', '', '', null, null);
INSERT INTO `tm_user` VALUES ('david', 'David', 'davis', 'member', '', null, '172522ec1028ab781d9dfd17eaca4427', 'Jawa Timur', 'Malang', '255', '1', 'David', '23402840', 'Mandiri', null, null);
INSERT INTO `tm_user` VALUES ('edoedo', 'edoedoeo', 'edo_guitarman.4171@yahoo.com', 'member', '', null, '827ccb0eea8a706c4c34a16891f84e7b', 'Jawa Barat', 'Bandung', '22', '1', '', '', '', null, null);
INSERT INTO `tm_user` VALUES ('eka', 'eka ramdani', 'admin@admin.com', 'member', '', null, '827ccb0eea8a706c4c34a16891f84e7b', 'DKI Jakarta', 'Jakarta Pusat', '152', '1', 'Eka Ramdani', '12324', 'BRI', null, null);
INSERT INTO `tm_user` VALUES ('Emma', 'Fathimatuz Zahra', 'emma_zahra@gmail.com', 'member', '', null, '972a9f0dc30d2d552063983763dab7d8', 'Jawa Timur', 'Malang', '256', '1', '', '', '', null, null);
INSERT INTO `tm_user` VALUES ('Ontownnageenano', 'Ontownnageenano', 'cxufmhorrespesia@maldonadomail', 'member', '', null, '6d1324945fb216a6c6348a04abc2058f', '', '', '0', '0', 'Ontownnageenano', 'Ontownnageenano', '', null, null);
INSERT INTO `tm_user` VALUES ('pisang', 'Pisang', 'pisang@gmail.com', 'member', '', null, '4dc2a159b17b4725943816b8ba6d7ff5', 'Jawa Timur', 'Malang', '255', '1', 'Pisang', '23432532523', 'Mandiri', null, null);
INSERT INTO `tm_user` VALUES ('rhetech', 'Rheza Arief', 'rhetech@yahoo.co.uk', 'member', '', null, '827ccb0eea8a706c4c34a16891f84e7b', 'Jawa Timur', 'Sidoarjo', '409', '1', 'rheza arief ', '712537512375133', 'BNI', null, null);
INSERT INTO `tm_user` VALUES ('ridhorobby', 'RIdho', 'ridho.robby50@yahoo.com', 'member', '1', null, '827ccb0eea8a706c4c34a16891f84e7b', 'Jawa Timur', 'Bondowoso', '86', '1', '', '', '', null, null);
INSERT INTO `tm_user` VALUES ('robby1', 'Robby', 'roby@robyyahoo.com', 'member', '', null, '827ccb0eea8a706c4c34a16891f84e7b', '0', '0', '0', '0', '', '', '', null, null);
INSERT INTO `tm_user` VALUES ('robby12', 'Robby Bibi', 'roby@robyyahoo.com', 'member', '', null, '827ccb0eea8a706c4c34a16891f84e7b', 'Jawa Timur', 'Malang', '256', '1', '', '', '', null, null);
INSERT INTO `tm_user` VALUES ('saya', 'coba', 'coba@gmail.com', 'member', '', null, '81dc9bdb52d04dc20036dbd8313ed055', 'Jawa Timur', 'Jember', '160', '1', 'COba', '11111111', 'BCA', null, null);
INSERT INTO `tm_user` VALUES ('test', 'test', 'ic_troumax@yahoo.com', 'admin', '1', '', '22476aa1575b6ef6032e4c2c2e3a7b25', '0', '0', '0', '0', '', '', '', null, null);
INSERT INTO `tm_user` VALUES ('TOKONEO', 'TOKONEO', 'yayanraw@gmail.com', 'member', '', null, 'd8578edf8458ce06fbc5bb76a58c5ca4', 'Jawa Timur', 'Malang', '255', '1', 'Yayan Rahmat Wijaya', '021223224244', 'BRI', null, null);
INSERT INTO `tm_user` VALUES ('Usernam', 'Admin', 'admin@admin.com', 'member', '', null, 'e10adc3949ba59abbe56e057f20f883e', 'DI Yogyakarta', 'Bantul', '39', '0', 'nama pem', 'no 12312', 'Mandiri', null, null);
INSERT INTO `tm_user` VALUES ('wildan', 'wildan afif', 'wildanafif00@gmail.com', 'member', '', null, '827ccb0eea8a706c4c34a16891f84e7b', 'Jawa Timur', 'Malang', '255', '1', 'Wildan Afif', '34234233', 'BRI', null, null);
INSERT INTO `tm_user` VALUES ('wildanafif', 'wildan afif a', 'wildanafif.id@gmail.com', 'member', '', null, '827ccb0eea8a706c4c34a16891f84e7b', 'Jawa Timur', 'Blitar', '74', '1', 'wildan', '43534534', 'Mandiri', null, null);
INSERT INTO `tm_user` VALUES ('wld', 'wildan afif', 'wildanafif.id@gmail.com', 'member', '', null, 'e353cc5a41c4b79ffa728ce8b80d1b62', 'Jawa Timur', 'Lumajang', '243', '0', 'erda', '324232', 'Mandiri', null, null);

-- ----------------------------
-- Table structure for tm_voucher
-- ----------------------------
DROP TABLE IF EXISTS `tm_voucher`;
CREATE TABLE `tm_voucher` (
  `fc_id_voucher` int(11) NOT NULL AUTO_INCREMENT,
  `fc_kdbarang` char(15) DEFAULT NULL,
  `id_user` char(25) DEFAULT NULL,
  `fm_nominal` double(11,0) DEFAULT NULL,
  `fd_tgl_exp_voucher` datetime DEFAULT NULL,
  `f_kode_voucher` char(30) DEFAULT NULL,
  `fd_tgl_terbit_voucher` datetime DEFAULT NULL,
  `fc_status` enum('0','1','2') DEFAULT '0' COMMENT '0 : status belum di pakai,1: status kalau sudah cek voucher , 2 : status kalau sudah check out',
  PRIMARY KEY (`fc_id_voucher`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tm_voucher
-- ----------------------------
INSERT INTO `tm_voucher` VALUES ('1', 'BI00001', 'admin', '100000', '2018-10-17 17:08:00', 'Promo1', '2014-01-09 11:23:00', '1');
INSERT INTO `tm_voucher` VALUES ('2', 'BI00001', 'admin', '100000', '2018-10-17 17:08:00', 'Promo2', '2014-01-09 11:23:02', '1');
INSERT INTO `tm_voucher` VALUES ('3', 'BI00001', 'admin', '100000', '2018-10-17 17:08:00', 'Promo3', '2014-01-09 11:23:03', '1');
INSERT INTO `tm_voucher` VALUES ('4', 'BI00001', 'admin', '100000', '2018-10-17 17:08:00', 'Promo3', '2014-01-09 11:23:04', '1');
INSERT INTO `tm_voucher` VALUES ('5', 'BI00001', 'admin', '100000', '2018-10-17 17:08:00', 'Promo3', '2014-01-09 11:23:05', '1');

-- ----------------------------
-- Table structure for t_barang_pindah
-- ----------------------------
DROP TABLE IF EXISTS `t_barang_pindah`;
CREATE TABLE `t_barang_pindah` (
  `fc_kdbarang_pindah` int(11) NOT NULL AUTO_INCREMENT,
  `fd_tgl_barang_pindah` date DEFAULT NULL,
  `fc_kdgudang_asal` int(11) DEFAULT NULL,
  `fc_kdgudang_tujuan` int(11) DEFAULT NULL,
  `fc_kdbarang` char(20) DEFAULT NULL,
  `f_jumlah_barang` int(20) DEFAULT NULL,
  `id_user` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`fc_kdbarang_pindah`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_barang_pindah
-- ----------------------------

-- ----------------------------
-- Table structure for t_bpbdtl
-- ----------------------------
DROP TABLE IF EXISTS `t_bpbdtl`;
CREATE TABLE `t_bpbdtl` (
  `fc_id` int(11) NOT NULL AUTO_INCREMENT,
  `fc_nobpb` char(15) DEFAULT NULL,
  `fc_kdbarang` char(15) DEFAULT NULL,
  `fn_qtyterima` int(11) DEFAULT NULL,
  `fm_harsat` double(10,0) DEFAULT NULL,
  `fm_subtot` double(10,0) DEFAULT NULL,
  `id_user` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`fc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_bpbdtl
-- ----------------------------

-- ----------------------------
-- Table structure for t_bpbmst
-- ----------------------------
DROP TABLE IF EXISTS `t_bpbmst`;
CREATE TABLE `t_bpbmst` (
  `fc_id` int(11) NOT NULL AUTO_INCREMENT,
  `fc_nobpb` char(15) DEFAULT NULL,
  `fd_tglbpb` date DEFAULT NULL,
  `fv_nama_supplier` varchar(30) DEFAULT NULL,
  `fd_tglinput` datetime DEFAULT NULL,
  `fc_userinput` char(15) DEFAULT NULL,
  `fn_qtytot` int(11) DEFAULT NULL,
  `id_user` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`fc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_bpbmst
-- ----------------------------

-- ----------------------------
-- Table structure for t_nomor
-- ----------------------------
DROP TABLE IF EXISTS `t_nomor`;
CREATE TABLE `t_nomor` (
  `kode` char(10) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `awalan` char(15) COLLATE latin1_general_ci DEFAULT NULL,
  `akhiran` char(15) COLLATE latin1_general_ci DEFAULT NULL,
  `panjang` int(4) unsigned DEFAULT '0',
  `nomor` int(4) unsigned DEFAULT '0',
  PRIMARY KEY (`kode`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci ROW_FORMAT=FIXED;

-- ----------------------------
-- Records of t_nomor
-- ----------------------------
INSERT INTO `t_nomor` VALUES ('INV', 'INV-', null, '5', '1');
INSERT INTO `t_nomor` VALUES ('PST', 'PST/', null, '5', '1');

-- ----------------------------
-- Table structure for t_setup
-- ----------------------------
DROP TABLE IF EXISTS `t_setup`;
CREATE TABLE `t_setup` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `fc_param` char(20) DEFAULT NULL,
  `fc_kode` char(1) DEFAULT NULL,
  `fc_isi` char(200) DEFAULT NULL,
  `id_user` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_setup
-- ----------------------------

-- ----------------------------
-- Table structure for t_status
-- ----------------------------
DROP TABLE IF EXISTS `t_status`;
CREATE TABLE `t_status` (
  `fc_param` char(10) NOT NULL,
  `fc_kode` char(2) NOT NULL,
  `fv_value` char(50) DEFAULT NULL,
  PRIMARY KEY (`fc_param`,`fc_kode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_status
-- ----------------------------

-- ----------------------------
-- Table structure for t_temp_order
-- ----------------------------
DROP TABLE IF EXISTS `t_temp_order`;
CREATE TABLE `t_temp_order` (
  `fc_id` int(11) NOT NULL AUTO_INCREMENT,
  `fc_kdbarang` char(15) DEFAULT NULL,
  `fn_quantity` int(10) DEFAULT NULL,
  `fc_kdgudang` int(11) DEFAULT NULL,
  `fc_kdkeranjang_belanja` char(20) DEFAULT NULL,
  PRIMARY KEY (`fc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_temp_order
-- ----------------------------
INSERT INTO `t_temp_order` VALUES ('3', 'BI00001', '1', '1', '::1');
INSERT INTO `t_temp_order` VALUES ('4', 'BI00002', '1', '1', '::1');

-- ----------------------------
-- Event structure for reset
-- ----------------------------
DROP EVENT IF EXISTS `reset`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` EVENT `reset` ON SCHEDULE EVERY 5 MINUTE STARTS '2018-10-18 16:25:52' ON COMPLETION NOT PRESERVE ENABLE DO update tm_voucher 
set fc_status='1' 
where fd_tgl_terbit_voucher < date_sub(now(),interval 24 HOUR) 
  and (fc_status='0')
;;
DELIMITER ;
